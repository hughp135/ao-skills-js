# Generating items/stats data

Data is taken from the source folder, and by running the `compile.js` file inside the source folder,
it will compile all this source data into smaller files usable by the client (output goes into `front-end`).

Do not modify files in `front-end` manually, as these are the output of running `compile.js`

Source files required (to update items database to new version): Note: these files are `.gitignore`d by this repo to reduce repository file size (it is around ~100MB in total).

- `aoitems.db`. SQLite dump of the RDB as created by AOIA+. (Located by default in User/AppData/Local/Hallucina Software/AOIAPlus). Download AOIA from [https://forums.funcom.com/t/anarchy-online-item-assistant-plus/4250](AO forums AOIA) (also in my google drive).
- `database.db`. SQLite dump created by [https://github.com/Budabot/Tyrbot](Tyrbot). After running Tyrbot for the first time, you must copy the file generated at `/data/database.db` into the source folder of this project.

# JSON data sources from Tyrbot

Note: These JSON exports of the Tyrbot database are commited to the `/data/source` folder as they are unexpected to change in future patches of AO. If these need to be updated, you can export new copies of the tables into JSON format and replace the source files:

'Cluster', 'ClusterImplantMap', 'EffectTypeMatrix', 'implantHelper'

# Extra data sources

The files `data/source/benefits.json` and `data/source/perks.json` are unexpected to change, and were generated using the scripts in the `unused` folder. These also come primarily from Tyrbot, and other information sources found on the AO wiki etc.

Should perks be updated in the future, check out `/data/unused/parse-perks.js` and other similar scripts.

# Unused data

This data is in the 'unused' folder because the files aren't used by any code. They were original references for data that has been copied into constants in the code.
