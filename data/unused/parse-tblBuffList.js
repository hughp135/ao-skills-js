const fs = require("fs");
const file = fs.readFileSync("./tblAO.json");
const db = JSON.parse(file.toString());

const buffList = JSON.parse(fs.readFileSync("./tblBuffList.json").toString());

// Some random exceptions that are annoyingly labeled as general buffs
const SOLDIER = [
  "empowered",
  "fight",
  "resonance", "pre-nullity",
];
const FIXER = [
  "grid",
  "shadow",
  "compressor", "retool ncu", "jury-rig", "deck recoder", "recompiling mem", "quarkstor", "active viral com",
  "sentient viral",
];
const TRADER = ["umbral", "sacrifice", "Ineptitude Transfer"];
const KEEPER = ["of the", "insight", "imminence of", "symbiotic bypass", "righteous smite", "virtuous"];
const CRAT = ["overrule", "leadership", "the director", "governance"];
const MA = ["cohort", "horde"];
const AGENT = ["meld with", "cloak of night", "blanket of sh", "shadow filter"];
const ENG = ["sympathetic", "assault force relief"];
const DOC = ["improved life chan"];

const result = buffList.map(buff => {
  const item = db.find(i => i.aoid === buff.aoid);
  buff.icon = item.icon;

  if (buff.prof === -1) {
    // Change prof of some incorrectly labeled buffs
    if (wordMatch(buff.name, SOLDIER)) {
      buff.prof = 1;
    } else if (wordMatch(buff.name, FIXER)) {
      buff.prof = 4;
    } else if (wordMatch(buff.name, TRADER)) {
      buff.prof = 7;
    } else if (wordMatch(buff.name, KEEPER)) {
      buff.prof = 14;
    } else if (wordMatch(buff.name, CRAT)) {
      buff.prof = 8;
    } else if (wordMatch(buff.name, MA)) {
      buff.prof = 2;
    } else if (wordMatch(buff.name, AGENT)) {
      buff.prof = 5;
    } else if (wordMatch(buff.name, ENG)) {
      buff.prof = 3;
    } else if (wordMatch(buff.name, DOC)) {
      buff.prof = 10;
    }
  }

  // change -2 (misc) to -1 (general)
  if (buff.prof === -2) {
    buff.prof = -1;
  }

  return buff;
});

// Add buffs that aren't included (nano cans)
result.push({
  aoid: 303383,
  fpable: 0,
  name: "Nano Can Buff: Composite Attribute Improvement",
  prof: -1,
  target: 1,
  icon: 297495,
});

result.push({
  aoid: 303387,
  fpable: 0,
  name: "Nano Can Buff: Treatment Mastery",
  prof: -1,
  target: 1,
  icon: 16342,
});

result.push({
  aoid: 303386,
  fpable: 0,
  name: "Nano Can Buff: Computer Literacy Mastery",
  prof: -1,
  target: 1,
  icon: 16213,
});

result.push({
  "aoid": 287559,
  "fpable": 0,
  "name": "Zazen Stance",
  "prof": 2,
  "target": 1,
  "icon": 301439
});

result.push({
  "aoid": 269470,
  "fpable": 0,
  "name": "Fists of the Winter Flame",
  "prof": 2,
  "target": 1,
  "icon": 301567
});

function wordMatch(name, matchWords) {
  return matchWords.some(word => name.toLowerCase().includes(word.toLowerCase()));
}

fs.writeFileSync("./buffList.json", JSON.stringify(result));
