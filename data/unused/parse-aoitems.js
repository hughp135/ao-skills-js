const cheerio = require('cheerio');
const got = require('got');
const fs = require('fs');

// Run this, then parse-tblItemEffects.js and parse-tblAO.js, before generating fe data

const EXISTING_ITEMEFFECTS = JSON.parse(fs.readFileSync('./extra-itemeffects.json').toString());
const EXISTING_SLOTS = JSON.parse(fs.readFileSync('./extra-itemslots.json').toString());

const armor = [
  // Loren's XX of Azure Reveries
  304614, 304613, 304610, 304611, 304612, 304609,
  // Serpentine Armor
  304457, 304456, 304455, 304454, 304445, 304444,
  304447, 304446, 304449, 304448, 304453, 304452, 304451, 304450,
  // Elite Desert Nomad Armor
  303555, 303554, 303561, 303560, 303563, 303562, 303557, 303556,
  303549, 303548, 303559, 303558, 303551, 303550, 303553, 303552,
  // Putrescent Ring
  304622,
  // The Reaper's
  304130, 304129, 304128,
  // Rusted Carbonum
  303597, 303596, 303595, 303594, 303601, 303600, 303593, 303592,
  303603, 303602, 303599, 303598,
  304621, // Symbiotic gloves of Erudition
  304820, // Modified santa bag
];

const weapons = [
  304223, // Inert Reaper
  303322, // Freedom Arms 3927 Chameleon,
  304822, // Skroog's list

];

const nanos = [
  287559, 269470,
];

async function start() {
  let skippedCount = 0;
  async function addId(id, type) {
    if (EXISTING_ITEMEFFECTS[id]) {
      skippedCount++;
      return;
    }
    const html = await getHtml(id);
    const skillEffects = await getItemEffectsFromAoItems(html, id, type === 'Nano');
    if (skillEffects.length) {
      EXISTING_ITEMEFFECTS[id] = skillEffects;
    } else {
      console.log(`No effects found for id ${id}`);
    }
    if (type !== 'Nano') {
      const slots = getItemSlotFromAoItems(html, id);
      EXISTING_SLOTS[type][id] = slots;
    }
  }
  for (const id of armor) {
    await addId(id, 'Armor');
  }
  for (const id of weapons) {
    await addId(id, 'Weapon');
  }
  for (const id of nanos) {
    await addId(id, 'Nano');
  }

  fs.writeFileSync('./extra-itemeffects.json', JSON.stringify(EXISTING_ITEMEFFECTS));
  fs.writeFileSync('./extra-itemslots.json', JSON.stringify(EXISTING_SLOTS));
  console.log(`skipped ${skippedCount} IDs because they are already added`);
}

function getItemSlotFromAoItems(html, ID) {
  const $ = cheerio.load(html);
  const panels = $('.panel.panel-default');
  const attribPanel = panels.toArray().find(panel => {
    const $panel = cheerio.load(panel);
    const heading = $panel('.panel-heading');
    return heading.text() === 'Attributes';
  });
  if (!attribPanel) {
    console.log('No attrib panel for id ' + ID)
    return;
  }
  const rows = cheerio.load(attribPanel)('.table.table-condensed tbody tr');
  const slotRow = rows.toArray().find(row => {
    const $row = cheerio.load(row);
    const td = $row('td:first-child');
    return td.text().startsWith('Slot298');
  });
  const $slotRow = cheerio.load(slotRow);
  const val = parseInt($slotRow('td.Value span:nth-child(2)').text(), 10);
  return val;
}

function getItemEffectsFromAoItems(html, ID, useEffects) {
  const $ = cheerio.load(html);
  const panels = $('.panel.panel-default');
  const effectsPanel = panels.toArray().find(panel => {
    const $panel = cheerio.load(panel);
    const heading = $panel('.panel-heading');
    if (!useEffects) {
      const debug = heading.find('.Debug');
      return debug.text() === '14';
    }
    const label = heading.find('.label.label-default.pull-right');
    return label.text().includes('Effect') && heading.text().includes('Use');
  });
  if (!effectsPanel) {
    return console.log('No effects panel found for item ' + ID);
  }
  const rows = cheerio.load(effectsPanel)('.table.table-condensed tbody tr.EventSpell:not(.HiddenElement)');
  return rows.toArray().map(row => {
    const $row = cheerio.load(row);
    // const hook = 14;
    // const type = 53;
    const cell = $row('td:nth-child(2)');
    const isModifier = cell.text().includes('Modify');
    if (!isModifier) {
      return;
    }
    const isLevelScaling = cell.html().includes('At level 200: Modify');
    const skill = parseInt($row('td:nth-child(2) .SpellArgument .Debug:nth-child(2)').text(), 10);
    const value = parseInt($row('td:nth-child(2) span:nth-child(3)').text(), 10);
    if (isNaN(skill) || isNaN(value)) {
      console.error('Received NAN for cell', cell.html());
      return;
    }
    const effects = [
      skill, value,
    ];
    if (isLevelScaling) {
      effects.push(1);
    }
    return effects;
  }).filter(v => !!v);
}

start().catch(e => {
  console.error(e);
});

async function getHtml(aoid) {
  const {
    body
  } = await got.get(`https://aoitems.com/item/${aoid}`);
  return body;
}