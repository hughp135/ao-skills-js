const fs = require("fs");

const implantHelper = JSON.parse(fs.readFileSync("./tblImplantHelper.json").toString());
const countBefore = implantHelper.length;
let count = 0;
const result = implantHelper.reduce((acc, curr) => {
  const shiny = curr.shiny_cluster === "Empty" ? "" : curr.shiny_cluster;
  const bright = curr.bright_cluster === "Empty" ? "" : curr.bright_cluster;
  const faded = curr.faded_cluster === "Empty" ? "" : curr.faded_cluster;
  const key = `${shiny}@${bright}@${faded}`;
  ++count;
  acc[key] = [curr.aoid_ql001, curr.aoid_ql200, curr.aoid_ql201, curr.aoid_ql300];
  return acc;
}, {});

if (count !== countBefore) {
  throw new Error("Count is different " + countBefore + " " + count);
}

fs.writeFileSync("./implantHelper.json", JSON.stringify(result));
