const fs = require("fs");
const lineReader = require("readline").createInterface({
  input: fs.createReadStream(__dirname + "/categories-skills.txt"),
});

const categories = {};
let current = null;

lineReader
  .on("line", line => {
    if (line.startsWith("-")) {
      if (current) {
        // add previous to categories
        categories[current.name] = current.skills;
      }
      current = { name: line.substr(1), skills: [] };
    } else {
      // const cat = categories[categories.length - 1];
      current.skills.push(line);
    }
    // console.log('line', line)
  })
  .on("close", () => {
    console.log(categories);
    fs.writeFileSync(__dirname + "/categories-skills.json", JSON.stringify(categories));
  });
