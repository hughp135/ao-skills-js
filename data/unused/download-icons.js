const got = require('got');
const stream = require('stream');
const fs = require('fs');

const aodb = require('./aodb.json');

(async function start() {
  const itemsIcons = aodb.reduce((acc, curr) => {
    if (!acc.some(a => a.icon === curr.icon)) {
      acc.push(curr);
    }
    return acc;
  }, []);
  const notDownloaded = itemsIcons.filter(item => {
    const fileToWrite = `./icons/${item.icon}.png`;
    return !fs.existsSync(fileToWrite);
  }).sort((a, b) => a.icon < b.icon ? -1 : a.icon > b.icon ? 1 : 0);
  while (notDownloaded.length) {
    const portion = notDownloaded.splice(0, 5);
    await Promise.all(portion.map(async item => {
      try {
        await downloadFile(item.icon);
        console.log('downloaded icon', item.icon);
      } catch (e) {
        if (typeof e !== 'string' || !e.startsWith('file already downloaded')) {
          console.error(`icon ${item.icon} failed to download`);
        }
        console.error(e);
      }
    }));
    await new Promise(res => setTimeout(res, 100));
  }
})();

async function downloadFile(iconId) {
  return new Promise((res, rej) => {
    const fileToWrite = `./icons/${iconId}.png`;
    if (!fs.existsSync(fileToWrite)) {
      const imgStream = got.stream(`https://static.aoitems.com/icon/${iconId}`);

      imgStream.on('error', rej);

      const writeStream = fs.createWriteStream(fileToWrite);

      imgStream.pipe(writeStream);

      writeStream.on('close', res);
      writeStream.on('error', rej);
    } else {
      rej('file already downloaded ' + iconId);
    }
  })
}

