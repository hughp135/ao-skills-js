const csv = require('csv-parser')
const fs = require('fs')
const results = [];
 
fs.createReadStream(__dirname + '/skill-costs-profession.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    parseSkillCosts(results);
    
  });

function parseSkillCosts(results) {
  console.log(JSON.stringify(results))
  fs.writeFileSync(__dirname + '/output.json', JSON.stringify(results))
}