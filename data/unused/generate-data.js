const fs = require('fs');

// Note: this file contains data from table with the same name in the AOS4 database
// It is not commited due to a large file size

const files = [
  'aodb',
  'Cluster',
  'ClusterImplantMap',
  'ClusterType',
  'EffectTypeMatrix',
  'Symbiant',
  'SymbiantClusterMatrix',
  'itemEffects',
  'implantHelper',
  'perks',
  'buffList',
  'benefits',
];

files.forEach((fileName) => {
  const file = fs.readFileSync(`./${fileName}.json`);
  const db = JSON.parse(file.toString());
  const result = mapToArray(db);

  fs.writeFileSync(`../front-end/${fileName}.json`, JSON.stringify(result));
});

function mapToArray(db) {
  if (Array.isArray(db)) {
    const columnNames = Object.keys(db[0]);
    return db.reduce(
      (acc, curr) => {
        acc.values.push(Object.values(curr));
        return acc;
      },
      {
        columns: columnNames,
        values: [],
      }
    );
  }
  return db;
}
