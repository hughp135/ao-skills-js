const fs = require('fs');
const { NANO_CAN_BUFFS } = require('./extra-buffs');
const items = JSON.parse(fs.readFileSync('./aodb.json').toString());
const perks = JSON.parse(fs.readFileSync('./perks.json').toString());
// Note: tblItemEffects.json contains data from the tblItemEffects table in the AOS4 database
// It is not commited to the repository due to a large file size. This must be extracted manually from the AOS4 source
const itemEffects = JSON.parse(fs.readFileSync('./tblItemEffects.json').toString());
const buffList = JSON.parse(fs.readFileSync('./buffList.json').toString());
const benefits = JSON.parse(fs.readFileSync('./benefits.json').toString());
const extraItemEffects = JSON.parse(fs.readFileSync('./extra-itemeffects.json').toString());
const perkIds = perks.reduce((acc, curr) => {
  acc[curr.aoid] = true;
  return acc;
}, {});
const buffIds = buffList.reduce((acc, curr) => {
  acc[curr.aoid] = true;
  return acc;
}, {});
const benefitIds = Object.values(benefits).reduce((acc, curr) => {
  curr.forEach((benefit) => {
    benefit.aoids.forEach(({ aoid }) => {
      acc[aoid] = true;
    });
  });
  return acc;
}, []);
const itemIds = items.reduce((acc, curr) => {
  acc[curr.highid] = true;
  acc[curr.lowid] = true;
  return acc;
}, {});

const map = itemEffects
  .filter((item) => {
    const isItem = itemIds[item.aoid] && item.hook === 14 && item.type === 53;
    const isBuff = buffIds[item.aoid] && item.hook === 0 && [20, 22, 53, 183].includes(item.type);
    const isBenefit = benefitIds[item.aoid] && item.hook !== 25 && item.hook !== 24 && ((item.hook === 0 && item.type === 183) || item.type === 53);
    const isPerk = perkIds[item.aoid] && item.hook === 14 && item.type === 53;

    // @todo item effects that scale by level seem to be ignored? see item 25994 for example

    return isItem || isBuff || isBenefit || isPerk; // 53: on wear effect
  })
  .reduce((acc, curr) => {
    if (!acc[curr.aoid]) {
      acc[curr.aoid] = [];
    }
    if (curr.value1 !== 0) {
      // ignore 0 values
      const vals = [curr.value1, curr.value2];
      if (curr.type === 183) {
        vals.push(1); // set 3rd value to 1 if type is effect on level 200
      }
      // Don't add nano resist twice (fix for HHAB items adding NR twice)
      if (curr.value1 === 168 && acc[curr.aoid].some((val) => val[0] === 168) && curr.value2 === 200) {
        console.log(`Not adding second NR effect to item ${curr.aoid}`);
      } else {
        acc[curr.aoid].push(vals); // value1 is abilityID, value2 is amount
      }
    }
    return acc;
  }, {});

Object.keys(NANO_CAN_BUFFS).forEach((aoid) => {
  if (!map[aoid]) {
    map[aoid] = NANO_CAN_BUFFS[aoid];
  } else {
    console.warn(`aoid ${aoid} already has item effects`);
  }
});

Object.entries(extraItemEffects).forEach(([id, val]) => {
  if (!map[id]) {
    map[id] = val;
  } else {
    console.log(`extra item effect with id ${id} already has item effects.`);
  }
});

fs.writeFileSync('./itemEffects.json', JSON.stringify(map));

// Old way to detect buffs:
// const isBuff = item.hook === 0 && [20, 22, 53].includes(item.type)
//   && ([1, 181, 216, 217, 219, 221, 233, 234, 311, 343, 364, 379, 482, 483, 536].includes(item.value1)
//     || (item.value1 >= 16 && item.value1 <= 21)
//     || (item.value1 >= 90 && item.value1 <= 97)
//     || (item.value1 >= 100 && item.value1 <= 168)
//     || (item.value1 >= 205 && item.value1 <= 208)
//     || (item.value1 >= 225 && item.value1 <= 231)
//     || (item.value1 >= 276 && item.value1 <= 282)
//     || (item.value1 >= 316 && item.value1 <= 319)
//     || (item.value1 >= 380 && item.value1 <= 383)
//     || (item.value1 >= 475 && item.value1 <= 480)
//   );
