const fs = require('fs');

// Note: this file contains data from table with the same name in the AOS4 database
// It is not commited due to a large file size
const file = fs.readFileSync('./tblAO.json');
const db = JSON.parse(file.toString());

// Run parse-tblBuffList.js first
const buffList = JSON.parse(fs.readFileSync('./buffList.json').toString());
const buffIds = buffList.map(b => b.aoid);

const extraItemSlots = JSON.parse(fs.readFileSync('./extra-itemslots.json').toString());

// AOS4 db query for benefit qls
/*
 "SELECT 'CITY' AS type, name, MIN(ql) AS min_ql, MAX(ql) AS max_ql " & _
  "FROM tblAO " & _
  "WHERE type = 'Misc' AND flags = 1 AND properties = 1 AND icon = 0 AND islot = 0 AND aoid NOT IN (254407,254410,254411) " & _
  "GROUP BY type, name " & _
  "UNION ALL " & _
  "SELECT 'TOWER' AS type, name, MIN(ql) AS min_ql, MAX(ql) AS max_ql " & _
  "FROM tblAO " & _
  "WHERE type = 'Misc' AND flags = -2143288831 AND properties = 9 AND islot = 0 AND name NOT LIKE '%TEST Item' AND name NOT LIKE 'Control Tower%' AND name NOT LIKE '% Spirit %' " & _
  "GROUP BY type, name " & _
  "UNION ALL " & _
  "SELECT 'ADVANTAGE' AS type, name, MIN(ql) AS min_ql, MAX(ql) AS max_ql " & _
  "FROM tblAO " & _
  "WHERE type = 'Misc' AND flags = 1 AND properties = 1 AND islot = 0 AND name LIKE 'Universal Advantage%' " & _
  "GROUP BY type, name " & _
  "ORDER BY 1,3,4"
*/
const benefitQls = JSON.parse(fs.readFileSync('./benefit-qls.json').toString());

const specialSymbs = [
  'Exterminator Ocular Enhancement',
  `Nematet's Inner Eye`,
  'Ethereal Spirit of Berserker Rage',
  'Ethereal Implant of Berserker Rage',
  `Cortex of the Executioner`,
  `Internal Anti-Matter Powerplant`,
  `Sinew of Tarasque`,
  `Heart of Tarasque`,
  `Nematet's Inner Eye`,
  `Skull of the Ancient`,
  `Notum Splice`,
  `Keeper's Vitality`,
  `Touch of the Gripper`,
  `The Primeval Skull`,
  'Cyborg Head Implant',
  'Cyborg Arm Implant',
  'Cyborg Chest Implant',
  'Cyborg Arm Implant',
  'Cyborg Wrist Implant',
  'Cyborg Waist Implant',
  'Cyborg Wrist Implant',
  'Cyborg Leg Implant',
  'Fist of the Dominator',
  'Rebuilt NCU Wrist Implant'
];

function isXanSymb(name) {
  return name.includes('Xan') && name.includes('Symbiant');
}

function isAdoSymb(name) {
  return name.includes('Syndicate Brain Symbiant');
}

const itemSlots = db
  .filter(item => {
    return (
      item.type === 'Weapon' ||
      item.type === 'Armor' ||
      item.type === 'Spirit' ||
      specialSymbs.includes(item.name) ||
      buffIds.includes(item.aoid) ||
      isXanSymb(item.name) ||
      isAdoSymb(item.name)
    );
  })
  .reduce(
    (acc, item) => {
      if (!['Weapon', 'Armor', 'Spirit'].includes(item.type)) {
        item.type = 'Extra';
      }
      if (item.islot < 0) {
        // Some of the DB items have negative values :/
        item.islot = getIntValue(item.islot);
      }
      acc[item.type][item.aoid] = item.islot;
      return acc;
    }, {
    Weapon: {},
    Armor: {},
    Extra: {},
    Spirit: {},
  }
  );

const orgBenefits = db.reduce(
  (acc, item) => {
    let type = null;
    if (
      item.type === 'Misc' &&
      item.flags === 1 &&
      item.properties === 1 &&
      item.icon === 0 &&
      item.islot === 0 &&
      ![254407, 254410, 254411].includes(item.aoid)
    ) {
      type = 'city';
    } else if (
      item.type === 'Misc' &&
      item.flags === -2143288831 &&
      item.properties === 9 &&
      item.islot === 0 &&
      !['TEST Item', 'Control Tower', 'Spirit'].some(word => item.name.includes(word))
    ) {
      type = 'tower';
    } else if (item.type === 'Misc' && item.flags === 1 && item.properties === 1 && item.islot === 0 && item.name.includes('Universal Advantage')) {
      type = 'advantage';
    }

    if (type) {
      const withSameName = acc[type].find(i => i.name === item.name);
      if (!withSameName) {
        acc[type].push(mapBenefit(item));
      } else {
        withSameName.aoids.push({
          ql: item.ql,
          aoid: item.aoid,
        });
      }
    }

    return acc;
  }, {
  city: [],
  tower: [],
  advantage: []
}
);

// Ensure AOIDs are sorted in ascending order
Object.values(orgBenefits).forEach(benefits => {
  benefits.forEach(benefit => {
    benefit.aoids = benefit.aoids.sort((a, b) => a.ql - b.ql);
  });
});

function mapBenefit(item) {
  const qlData = benefitQls.find(i => i.name === item.name && item.type === item.type);
  if (!qlData) {
    throw new Error(`Could not find ql Data for item ${item.name}`);
  }
  return {
    icon: item.icon,
    aoids: [{
      ql: item.ql,
      aoid: item.aoid
    }],
    name: item.name,
    min_ql: qlData ? qlData.min_ql : undefined,
    max_ql: qlData ? qlData.max_ql : undefined,
  };
}

function getIntValue(val) {
  return val + 2147483647;
}

Object.entries(extraItemSlots.Armor).forEach(([id, slot]) => {
  if (!itemSlots.Armor[id]) {
    itemSlots.Armor[id] = slot;
  } else {
    console.log(`Item ${id} already has a slot`);
  }
})

Object.entries(extraItemSlots.Weapon).forEach(([id, slot]) => {
  if (!itemSlots.Weapon[id]) {
    itemSlots.Weapon[id] = slot;
  } else {
    console.log(`Item ${id} already has a slot`);
  }
})

fs.writeFileSync('./benefits.json', JSON.stringify(orgBenefits));
fs.writeFileSync('./front-end/itemSlots.json', JSON.stringify(itemSlots));