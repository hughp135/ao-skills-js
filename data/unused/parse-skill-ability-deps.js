const csv = require("csv-parser");
const fs = require("fs");
const results = [];

const ABILITIES = [
  "Strength",
  "Agility",
  "Stamina",
  "Intelligence",
  "Sense",
  "Psychic",
];

fs.createReadStream(__dirname + "/skill-ability-deps.csv")
  .pipe(csv())
  .on("data", data => results.push(data))
  .on("end", () => {
    parseSkillCosts(results);
  });

function parseSkillCosts(results) {
  console.log(JSON.stringify(results));
  const parsed = {};
  results.forEach(skill => {
    const skillName = skill.skill;
    delete skill.skill;
    parsed[skillName] = {
      Strength: parseFloat(skill.Strength || 0),
      Agility: parseFloat(skill.Agility || 0),
      Stamina: parseFloat(skill.Stamina || 0),
      Intelligence: parseFloat(skill.Intelligence || 0),
      Sense: parseFloat(skill.Sense || 0),
      Psychic: parseFloat(skill.Psychic || 0),
    };
  });

  console.log("parsed", parsed);
  fs.writeFileSync(__dirname + "/skill-ability-deps.json", JSON.stringify(parsed));
}
