const fs = require("fs");

// Note: this file contains data from table with the same name in the AOS4 database
// It is not commited due to a large file size
const file = fs.readFileSync("./item_types.json");
const db = JSON.parse(file.toString());

const result = db.reduce((acc, curr) => {
  if (acc[curr.item_id]) {
    acc[curr.item_id].push(curr.item_type);
  } else {
    acc[curr.item_id] = [curr.item_type];
  }
  return acc;
}, {});

fs.writeFileSync("./front-end/itemTypes.json", JSON.stringify(result));
