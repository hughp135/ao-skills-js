const fs = require('fs');

// query for all-perks.json: AOSkilsl4 tblAO:
/*
SELECT aoid, name FROM tblAO WHERE ql = 1
AND type = 'Armor' AND flags = 9
AND (properties = 268435461 OR properties = 5)
AND icon = 0 AND islot = 0
UNION ALL
SELECT aoid, name FROM tblAO WHERE aoid IN (247748,247749,256082)
ORDER BY name, aoid
*/

// Note: this file contains data from table with the same name in the AOS4 database
// It is not commited due to a large file size
const allPerks = JSON.parse(fs.readFileSync('./all-perks.json').toString()); // source: AOSkills4 tblAO :see query
const itemReqs = JSON.parse(fs.readFileSync('./tblItemReqs.json').toString()); // source: AOSkills4

const perksToIgnore = [
  // from AOS4 Source
  211036,
  262591,
  262592,
  262593,
  262594,
  262595,
  262596,
  262598,
  262599,
  262600,
  262601,
  262602,
  262603,
  212162,
  212163,
  301880,
  301881,
  301882,
  301883,
  301884,
  239812,
  239813,
  239814,
  239815,
  239816,
  239817,
  239818,
  239819,
  239820,
  239821,
  211803,
  211804,
  211805,
  211806,
  211807,
  211808,
  211833,
  211834,
  211835,
  211836,
  211837,
  211838,
  211839,
  211840,
  211841,
  211842,
  260020,
  260021,
  260022,
  263035,
  263036,
  263037,
  263038,
  263039,
  263040,
  263028,
  263029,
  263030,
  263031,
  263032,
  263033,
  211123,
  260017,
  260018,
  260019,
  260023,
  260024,
  260025,
  262431,
  262432,
  262433,
  262434,
  262435,
  262436,
  211957,
  211958,
  211959,
  211960,
  211961,
  211962,
  211963,
  211964,
  211965,
  211966,
  213405,
  213406,
  213407,
  260014,
  260015,
  260016,
  211134,
  211075,
  // Added by me
  212141,
  212142,
  212143, // Adventurer Outdoorsman (removed swimming)
  211140,
  211057,
  211065, // removed perks
];

const BREEDS = {
  1: 'Solitus',
  2: 'Opifex',
  3: 'Nanomage',
  4: 'Atrox',
};
const PROFS = {
  1: 'Soldier',
  2: 'Martial Artist',
  3: 'Engineer',
  4: 'Fixer',
  5: 'Agent',
  6: 'Adventurer',
  7: 'Trader',
  8: 'Bureaucrat',
  9: 'Enforcer',
  10: 'Doctor',
  11: 'Nano-Technician',
  12: 'Meta Physicist',
  14: 'Keeper',
  15: 'Shade',
};

const itemReqsMap = itemReqs.reduce((acc, curr) => {
  if (!acc[curr.aoid]) {
    acc[curr.aoid] = [];
  }
  acc[curr.aoid].push(curr);
  return acc;
}, {});

let number;
let last_name = '';
let last_level = 0;

const perks = allPerks.reduce((acc, perk) => {
  const { aoid, name } = perk;
  // Note: logic ported from AOSkills4 source
  if (perksToIgnore.includes(aoid)) {
    return acc;
  }
  if (name !== last_name) {
    number = 0;
    last_level = 0;
  }
  number++;
  perk.counter = number;
  if ((aoid >= 210830 && aoid <= 239821) || (aoid >= 281534 && aoid <= 283682)) {
    perk.type = 'SL';
  } else if ((aoid >= 247748 && aoid <= 253147) || aoid === 256082 || [254415, 254416, 254417, 254418].includes(aoid)) {
    perk.type = 'AI';
  } else if ((aoid >= 260870 && aoid <= 262158) || (aoid >= 303268 && aoid <= 303277)) {
    perk.type = 'LE';
  } else {
    throw new Error(`Unknown perk ${aoid} ${perk.name}`);
  }
  last_name = name;

  const reqs = itemReqsMap[aoid];
  if (!reqs) {
    throw new Error(`Requirement not found for aoid ${aoid}`);
  }
  perk.professions = [];
  perk.breeds = [];
  perk.level = null;
  perk.aiTitle = null;
  if (perk.name === 'Ranger') {
    // console.log(reqs);
  }
  reqs.forEach(req => {
    if (req.attribute === 54) {
      // level
      perk.level = req.value + 1;
      last_level = perk.level;
    } else if (req.attribute === 4) {
      // breed
      perk.breeds.push(BREEDS[req.value]);
    } else if (req.attribute === 60) {
      // prof
      if (req.value !== 0) {
        // ignore 0 values
        if (!PROFS[req.value]) {
          throw new Error(`Invalid prof value ${req.value}`);
        }
        perk.professions.push(PROFS[req.value]);
      }
    } else if (req.attribute === 169) {
      // ai level
      perk.aiTitle = req.value + 1;
    }
  });

  acc.push(perk);

  return acc;
}, []);

fs.writeFileSync('./perks.json', JSON.stringify(perks));
