const NANO_CAN_BUFFS = {
  // Nano Can Buff: Composite Attribute Improvement
  303383: [
    [16, 20],
    [18, 20],
    [17, 20],
    [19, 20],
    [21, 20],
    [20, 20],
  ],
  // Nano Can Buff: Treatment Mastery
  303387: [
    [124, 35],
  ],
  // Nano Can Buff: Computer Literacy Mastery#
  303386: [
    [161, 35],
  ],
};

module.exports = {
  NANO_CAN_BUFFS,
};