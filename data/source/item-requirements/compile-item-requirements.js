const fs = require('fs');

// From https://github.com/TheTinkeringIdiot/TinkerParse/blob/main/AODb.Data/enums.cs#L464
const SKILLS_TO_INCLUDE = [
  /** Skills */
  16, // Strength
  17, // Agility
  18, // Stamina
  19, // Intelligence
  20, // Sense
  21, // Psychic
  100, // MartialArts
  101, // MultiMelee
  102, // _1hBlunt
  103, // _1hEdged
  104, // MeleeEnergy
  105, // _2hEdged
  106, // Piercing
  107, // _2hBlunt
  108, // SharpObjects
  109, // Grenade
  110, // HeavyWeapons
  111, // Bow
  112, // Pistol
  113, // Rifle
  114, // MG_SMG
  115, // Shotgun
  116, // AssaultRifle
  117, // VehicleWater
  118, // MeleeInit
  119, // RangedInit
  120, // PhysicalInit
  121, // BowSpecialAttack
  122, // SensoryImprovement
  123, // FirstAid
  124, // Treatment
  125, // MechanicalEngineering
  126, // ElectricalEngineering
  127, // MaterialMetamorphose
  128, // BiologicalMetamorphose
  129, // PsychologicalModification
  130, // MaterialCreation
  131, // SpaceTime
  132, // NanoPool
  133, // RangedEnergy
  134, // MultiRanged
  135, // TrapDisarm
  136, // Perception
  137, // Adventuring
  // 138, // Swimming
  139, // VehicleAir
  // 140, // MapNavigation
  141, // Tutoring
  142, // Brawl
  143, // Riposte
  144, // Dimach
  145, // Parry
  146, // SneakAttack
  147, // FastAttack
  148, // Burst
  149, // NanoInit
  150, // FlingShot
  151, // AimedShot
  152, // BodyDevelopment
  153, // DuckExplosions
  154, // DodgeRanged
  155, // EvadeClose
  156, // RunSpeed
  157, // QuantumFT
  158, // WeaponSmithing
  159, // Pharmaceuticals
  160, // NanoProgramming
  161, // ComputerLiteracy
  162, // Psychology
  163, // Chemistry
  164, // Concealment
  165, // BreakingEntry
  166, // VehicleGround
  167, // FullAuto
  168, // NanoResist
  /** Misc */
  389, // Expansion set required e.g. : 2 (sl) 32 (LE) 8 (AI)
];

const getItemReqs = async (aoiaDb, filteredItems) => {
  // const expansionReqs = await aoiaDb.all('SELECT * from tblAOItemRequirements WHERE attribute = 389');
  const query = `SELECT aoid, type, attribute, value, operator from tblAOItemRequirements WHERE
  attribute = 389
  OR (attribute IN (${SKILLS_TO_INCLUDE.join(', ')}) AND operator = 2 AND type IN (8, 6));
`; // type 8, 6 = on equip requirements

  const validIds = filteredItems.reduce((acc, curr) => {
    acc[curr.aoid] = true;

    return acc;
  }, {});

  const dbResult = await aoiaDb.all(query);

  const itemReqs = dbResult
    .filter(itemReq => validIds[itemReq.aoid])
    .reduce((acc, curr) => {
      if (!acc[curr.aoid]) {
        acc[curr.aoid] = [];
      }

      acc[curr.aoid].push(Object.values(curr).slice(1)); // remove first column (aoid) from values

      return acc;
    }, {});

  return itemReqs;
};

module.exports = {
  getItemReqs,
};
