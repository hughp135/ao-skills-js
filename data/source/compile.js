const sqlite3 = require('sqlite3');
const { open } = require('sqlite');
const fs = require('fs');

const benefitsPerksEffects = require('./benefits-perk-effects.json');
const { getItemReqs } = require('./item-requirements/compile-item-requirements');

function getIntValue(val) {
  return val + 2147483647;
}

/**
 * @TODO: https://github.com/TheTinkeringIdiot/TinkerParse/blob/main/AODb.Data/enums.cs
 * Map item requirements to skill names and make a json of item requirements
 */

const profs = {
  Adventurer: 6,
  Agent: 5,
  Bureaucrat: 8,
  Doctor: 10,
  Enforcer: 9,
  Engineer: 3,
  Fixer: 4,
  Keeper: 14,
  'Martial Artist': 2,
  'Meta Physicist': 12,
  'Nano-Technician': 11,
  Shade: 15,
  Soldier: 1,
  Trader: 7,
  General: -1,
};

const profsById = Object.entries(profs).reduce((acc, [key, value]) => {
  acc[value] = key;
  return acc;
}, {});

const extraBuffs = ['Coalescing Energy Cascade'];

const customNanos = [
  { aoid: 252028, fpable: 0, name: 'Extruder Nutrition Bar', prof: -1, icon: 130517 },
  { aoid: 254431, fpable: 0, name: 'Quantum Physics Laboratory', prof: -1, icon: 159122 },
];

// this is a top-level await
(async () => {
  // open the database. The DB file is from AOIA
  const aoiaDb = await open({
    filename: './aoitems.db',
    driver: sqlite3.Database,
  });

  const tyrbotDb = await open({
    filename: './database.db',
    driver: sqlite3.Database,
  });

  const tyrbotTables = await tyrbotDb.all("select name from sqlite_master where type='table'");
  const aodb = await tyrbotDb.all('SELECT * from aodb');
  const requiredTyrbotTables = ['aodb'];
  requiredTyrbotTables.forEach(tblName => {
    if (!tyrbotTables.some(tbl => tbl.name === tblName)) {
      throw new Error('Tyrbot Database does not have a required table: ' + tblName);
    }
  });

  const AOITables = await aoiaDb.all("select name from sqlite_master where type='table'");
  const requiredAOIATables = ['tblAOItems', 'tblAONanos', 'tblAONanoCrystals', 'tblAOItemEffects', 'tblAOItemRequirements'];

  requiredAOIATables.forEach(tblName => {
    if (!AOITables.some(tbl => tbl.name === tblName)) {
      throw new Error('AOIA Database does not have a required table: ' + tblName);
    }
  });

  const nanoCrystals = await aoiaDb.all('SELECT * from tblAONanoCrystals WHERE type LIKE "Crystal"');
  const extraNanos = await aoiaDb.all(
    `SELECT * from tblAONanos WHERE crystal_aoid = 0 OR name IN (${extraBuffs.map(buff => `'${buff}'`).join(', ')})`
  );
  const effects = await aoiaDb.all('SELECT * from tblAOItemEffects');
  const profRequirements = await aoiaDb.all('SELECT * from tblAOItemRequirements WHERE attribute = 60 OR attribute = 368');

  const buffList = nanoCrystals.map(nano => {
    return {
      aoid: nano.nano_id,
      fpable: nano.fp,
      name: nano.nano_name,
      prof: profs[nano.profession],
      icon: nano.nano_icon,
    };
  });

  // All nano effects that don't have nano crystals
  extraNanos.forEach(nano => {
    buffList.push({
      aoid: nano.aoid,
      fpable: 0,
      name: nano.name,
      prof: -1,
      icon: nano.icon,
    });
  });

  buffList.push(...customNanos);

  const buffIds = buffList.reduce((acc, curr) => {
    acc[curr.aoid] = true;
    return acc;
  }, {});

  const items = await aoiaDb.all('SELECT * from tblAOItems');
  const itemIds = {};
  const filteredItems = items.filter(item => {
    return (
      item.type === 'Weapon' ||
      item.type === 'Armor' ||
      item.type === 'Spirit' ||
      (buffIds[item.aoid] && item.type !== 'Misc') ||
      (item.type === 'Implant' &&
        // Filtering to only get symbiants / special imps
        !item.name.includes('Implant:') &&
        !item.name.startsWith('Basic') &&
        !item.name.startsWith('A Crashed') &&
        !item.name.startsWith('TestItem') &&
        item.name !== 'Uber imp!')
    );
  });
  const itemSlots = filteredItems.reduce(
    (acc, item) => {
      const type = item.type === 'Implant' ? 'Symbiant' : item.type;
      if (item.islot < 0) {
        // Some of the DB items have negative values :/
        item.islot = getIntValue(item.islot);
      }
      itemIds[item.aoid] = true;
      acc[type][item.aoid] = item.islot;
      return acc;
    },
    {
      Weapon: {},
      Armor: {},
      Symbiant: {},
      Spirit: {},
      Misc: {},
    }
  );

  const itemReqs = await getItemReqs(aoiaDb, filteredItems);

  const itemEffectsMap = effects
    .filter(item => {
      const isItem = itemIds[item.aoid] && item.hook === 14 && item.type === 53;
      const isBuff = buffIds[item.aoid] && item.hook === 0 && [20, 22, 53, 183].includes(item.type);

      return isItem || isBuff; // 53: on wear effect
    })
    .reduce((acc, curr) => {
      if (!acc[curr.aoid]) {
        acc[curr.aoid] = [];
      }
      if (curr.value1 !== 0) {
        // ignore 0 values
        const vals = [curr.value1, curr.value2];
        if (curr.type === 183) {
          vals.push(1); // set 3rd value to 1 if type is effect on level 200
        }
        // Don't add nano resist twice (fix for HHAB items adding NR twice)
        if (curr.value1 === 168 && acc[curr.aoid].some(val => val[0] === 168) && curr.value2 === 200) {
          // console.log(`Not adding second NR effect to item ${curr.aoid}`);
        } else {
          acc[curr.aoid].push(vals); // value1 is abilityID, value2 is amount
        }
      }
      return acc;
    }, {});

  Object.entries(benefitsPerksEffects).forEach(([key, value]) => {
    if (itemEffectsMap[key]) {
      console.log('Already has item', key);
    } else {
      itemEffectsMap[key] = value;
    }
  });

  const filteredBuffList = buffList.filter(buff => {
    return itemEffectsMap[buff.aoid] && itemEffectsMap[buff.aoid].length;
  });

  const itemProfRequirements = profRequirements.reduce((acc, curr) => {
    if (!itemIds[curr.aoid]) {
      return acc;
    }

    if (curr.attribute !== 60 && curr.attribute !== 368) {
      throw new Error('Invalid attribute for prof reqs', curr.attribute);
    }

    const profString = profsById[curr.value];

    if (!profString) {
      throw new Error('Invalid profession', curr.value);
    }

    if (!acc[curr.aoid]) {
      acc[curr.aoid] = {
        fp: false,
        profs: [],
      };
    }

    if (!acc[curr.aoid].profs.includes(profString)) {
      acc[curr.aoid].profs.push(profString);
    }

    if (curr.attribute === 368) {
      acc[curr.aoid].fp = true;
    }

    return acc;
  }, {});

  // Write source data into front-end folder (compressed structure)

  fs.writeFileSync('../front-end/buffList.json', JSON.stringify(mapToArray(filteredBuffList)));
  fs.writeFileSync('../front-end/itemEffects.json', JSON.stringify(mapToArray(itemEffectsMap)));
  fs.writeFileSync('../front-end/itemSlots.json', JSON.stringify(itemSlots));
  fs.writeFileSync('../front-end/aodb.json', JSON.stringify(mapToArray(aodb)));
  fs.writeFileSync('../front-end/itemProfs.json', JSON.stringify(itemProfRequirements));
  fs.writeFileSync('../front-end/itemReqs.json', JSON.stringify(itemReqs));

  // List of extra source data files used in the frontend
  const files = ['Cluster', 'ClusterImplantMap', 'EffectTypeMatrix', 'implantHelper', 'perks', 'benefits'];

  files.forEach(fileName => {
    const file = fs.readFileSync(`./${fileName}.json`);
    const db = JSON.parse(file.toString());
    const result = mapToArray(db);

    fs.writeFileSync(`../front-end/${fileName}.json`, JSON.stringify(result));
  });
})().catch(e => console.error(e));

function mapToArray(db) {
  if (Array.isArray(db)) {
    const columnNames = Object.keys(db[0]);
    return db.reduce(
      (acc, curr) => {
        acc.values.push(Object.values(curr));
        return acc;
      },
      {
        columns: columnNames,
        values: [],
      }
    );
  }
  return db;
}
