import expect from 'expect';
import { AODBService } from './ao-db-service';
import { SLOT_NUMBERS } from '../constants/items/item-slots';
import { getStore } from '../test/get-store';
import { DBBuff, Buff, Perk, DBItem } from '../constants/db/types';

describe('AODBService', () => {
  let service: AODBService;

  before(async () => {
    service = new AODBService(getStore());
  });
  it('creates a new instance', () => {
    expect(service).toBeDefined();
  });
  it('gets an item', () => {
    const item = service.getItem(302730);
    expect(item).toEqual({
      lowid: 302730,
      highid: 302730,
      lowql: 300,
      highql: 300,
      name: 'Awakened Burden of Competence',
      icon: 302952,
    });
  });
  describe('item buffs', () => {
    it('gets item with buffs', () => {
      const result = service.getItemBuffs({ lowid: 302730, highid: 302730, lowql: 300, highql: 300, selectedQl: 300 });
      expect(result).toHaveLength(23);
    });
    it('gets buffs for item by highid', () => {
      const item = { lowid: 123950, highid: 123951, lowql: 44, highql: 76, name: 'Galahad Inc T70 Beyer', icon: 13317, selectedQl: 76 };
      const result = service.getItemBuffs(item);
      expect(result).toHaveLength(2);
      const skills = ['Quantum FT', 'Comp. Liter'];
      result.forEach(b => {
        expect(skills).toContain(b.skillName);
        expect(b.amount).toEqual(20);
      });
    });
    it('interpolates item buffs correctly', () => {
      const item = { lowid: 238914, highid: 238915, lowql: 100, highql: 300, selectedQl: 179 };
      const result = service.getItemBuffs(item);
      expect(result).toEqual([
        { skillName: 'Sense', amount: 24 },
        { skillName: 'Agility', amount: 14 },
      ]);
    });
    it('interpolates item buffs correctly', () => {
      const item = { lowid: 238914, highid: 238915, lowql: 100, highql: 300, selectedQl: 100 };
      const result = service.getItemBuffs(item);
      expect(result).toEqual([
        { skillName: 'Sense', amount: 20 },
        { skillName: 'Agility', amount: 10 },
      ]);
    });
    it('interpolates item buffs correctly', () => {
      const item = { lowid: 238914, highid: 238915, lowql: 100, highql: 300, selectedQl: 300 };
      const result = service.getItemBuffs(item);
      expect(result).toEqual([
        { skillName: 'Sense', amount: 30 },
        { skillName: 'Agility', amount: 20 },
      ]);
    });
    it('returns correct item buffs for belt', () => {
      const item = <DBItem & { selectedQl?: number }>service.getItemByIdAndQl(36785, 60);
      expect(item.name).toEqual('Belt Component Platform 4IX');
      expect(item.lowid).toEqual(36777);
      item.selectedQl = 60;
      const result = service.getItemBuffs(item, 1);
      expect(result).toEqual([
        {
          amount: 3, // @TODO: Fix this shit deck slot shud be 4
          isLevelScaling: undefined,
          skillName: 'Free deck slot',
        },
      ]);
    });
    it('returns item buffs correctly for scope', () => {
      const item = <DBItem & { selectedQl?: number }>service.getItemByIdAndQl(160633, 205);
      item.selectedQl = 205;
      const result = service.getItemBuffs(item, 1);
      expect(result).toEqual([
        { amount: -280, isLevelScaling: undefined, skillName: 'Melee. Init.' },
        { amount: -280, isLevelScaling: undefined, skillName: 'Ranged. Init.' },
        { amount: -280, isLevelScaling: undefined, skillName: 'Physic. Init' },
        { amount: -34, isLevelScaling: undefined, skillName: 'NanoC. Init.' },
        { amount: 210, isLevelScaling: undefined, skillName: 'Aimed Shot' },
        { amount: 6, isLevelScaling: undefined, skillName: 'CriticalIncrease' },
        { amount: 28, isLevelScaling: undefined, skillName: 'RangeInc. Weapon' },
      ]);
    });
    it('gets Anun Membrane Gloves item effects, ignoring Cold AC (attrib 95)', () => {
      const result = service.getItemBuffs({ lowid: 248927, highid: 248928, lowql: 1, highql: 200, selectedQl: 150 });

      expect(result).toEqual([
        { amount: 188, isLevelScaling: undefined, skillName: 'Melee/ma AC' },
        { amount: 226, isLevelScaling: undefined, skillName: 'Imp/Proj AC' },
        { amount: 75, isLevelScaling: undefined, skillName: 'Energy AC' },
        { amount: 170, isLevelScaling: undefined, skillName: 'Fire AC' },
        { amount: 226, isLevelScaling: undefined, skillName: 'Radiation AC' },
        { amount: 170, isLevelScaling: undefined, skillName: 'Chemical AC' },
        { amount: 170, isLevelScaling: undefined, skillName: 'Disease AC' },
        { amount: 38, isLevelScaling: undefined, skillName: 'Max Health' },
        { amount: 15, isLevelScaling: undefined, skillName: 'Pistol' },
        { amount: 4, isLevelScaling: undefined, skillName: 'Add All Def.' },
      ]);
    });
  });
  describe('search item by slot', () => {
    it('returns all items for a weapon slot', () => {
      const result = service.searchSlotItems(2048, 'Weapon');
      expect(result.length).toEqual(764); // might fail if aodb gets updated
    });
    it('returns correct weapons for search term', () => {
      const result = service.searchSlotItems(2, 'Weapon', 'Keeper Nanodeck');
      result.forEach(i => {
        expect(i.name.toLowerCase()).toContain('Keeper Nanodeck'.toLowerCase());
      });
      expect(result.length).toEqual(3); // might fail if aodb gets updated
    });
    it('returns correct head armor for search term', () => {
      const result = service.searchSlotItems(4, 'Armor', "Miy's");
      expect(result.length).toEqual(5); // might fail if aodb gets updated
      result.forEach(i => {
        expect(i.name.toLowerCase()).toContain("Miy's".toLowerCase());
      });
    });
    it('returns correct finger armor for search term', () => {
      const result = service.searchSlotItems(32768, 'Armor', 'pinky jar');
      expect(result.length).toEqual(1);
    });
  });
  describe('get all weaps/huds/utils', () => {
    it('returns items for each slot', () => {
      const results = service.getWeaponHudUtils();
      expect(Object.keys(results)).toHaveLength(SLOT_NUMBERS.length);
      expect(results.HUD2).toBeDefined();
      Object.values(results).forEach((items: any) => {
        expect(items.length).toBeGreaterThan(1);
      });
    });
  });
  describe('get all clothing', () => {
    it('returns clothing items', () => {
      const results = service.getAllClothingItems();
      expect(Object.keys(results)).toHaveLength(15);
      expect(results.FINGER_L).toBeDefined();
      Object.values(results).forEach((items: any) => {
        expect(items.length).toBeGreaterThan(1);
      });
      const boards = results.NECK.filter(i => i.name.includes('Clan Merits'));
      expect(boards).toHaveLength(14);
    });
  });
  describe('Implants / Symbiants', () => {
    it('gets all imps with clusters and symbs', () => {
      const results = service.getAllImpsSymbs();
      expect(results).toHaveLength(13);
    });
  });
  describe('Perks', () => {
    it('returns all perks for profession and breed', () => {
      const result = service.getPerks('Solitus', 'Engineer');
      expect(result.general).toHaveLength(174);
      expect(result.profession).toHaveLength(40);
      expect(result.group).toHaveLength(67);
      expect(result.research).toHaveLength(80);
    });
    it('returns perk effects', () => {
      const perk: Perk = {
        aiTitle: null,
        aoid: 248136,
        breeds: [],
        level: 105,
        name: 'Embrace',
        professions: ['Doctor'],
        type: 'AI',
        counter: 0,
      };
      const result = service.getPerkEffects(perk);
      expect(result).toEqual([
        { skillName: 'Matt. Metam', amount: 5 },
        { skillName: 'Max Nano', amount: 90 },
        { skillName: '% Add. Nano Cost', amount: -1 },
      ]);
    });
    describe('power up perk returns correct amount of perks for group perk', () => {
      it('when prof is soldier', () => {
        const perks = service.getPerks('Solitus', 'Soldier');
        const powerUp = perks.group.filter(p => p.name === 'Power Up');
        expect(powerUp).toHaveLength(10);
      });
      it('when prof is engi', () => {
        const perks = service.getPerks('Solitus', 'Engineer');
        const powerUp = perks.group.filter(p => p.name === 'Power Up');
        expect(powerUp).toHaveLength(8);
      });
    });
  });

  describe('Nanos', () => {
    it('returns a list of buffs', () => {
      const result = service.getBuffList();
      // Length may change when db gets updated.
      expect(result).toHaveLength(3843);
      const wrangle = result.find(i => i.aoid === 121332);
      expect(wrangle).toBeDefined();
      (<Buff>wrangle).effects.forEach(w => {
        expect(w.amount).toEqual(85);
        expect(w.skillName).toBeDefined();
        expect(w.skillName.length).toBeGreaterThan(2); // kinda arbitrary
      });
    });
    it('returns nano effects for every nano', () => {
      const nanos = service.getBuffList();
      nanos.forEach(nano => {
        const effects = service.getBuffEffects(nano);
        expect(effects).toBeDefined();
      });
      const someWrangle = nanos.find(i => i.aoid === 121332);
      expect(someWrangle).toBeDefined();
      const wrangleBuffs = service.getBuffEffects(<DBBuff>someWrangle);
      expect(wrangleBuffs).toHaveLength(23);
      wrangleBuffs.forEach(w => {
        expect(w.amount).toEqual(85);
        expect(w.skillName).toBeDefined();
        expect(w.skillName.length).toBeGreaterThan(2); // kinda arbitrary
      });
    });
    it('interpolates level scaling buff correctly', () => {
      const item = {
        aoid: 293685,
        name: 'Coalescing Energy Cascade',
        target: <DBBuff['target']>0,
        fpable: <DBBuff['fpable']>1,
        prof: <DBBuff['prof']>-1,
        icon: <DBBuff['icon']>0,
      };
      const result = service.getBuffEffects(item, 100);
      expect(result).toEqual([
        { skillName: 'Max Nano', amount: 500, isLevelScaling: true },
        { skillName: 'Max Health', amount: 500, isLevelScaling: true },
        { skillName: 'First Aid', amount: 20 },
        { skillName: 'Strength', amount: 10 },
        { skillName: 'Agility', amount: 10 },
        { skillName: 'Stamina', amount: 10 },
        { skillName: 'Intelligence', amount: 10 },
        { skillName: 'Sense', amount: 10 },
        { skillName: 'Psychic', amount: 10 },
        { skillName: '% Add. Xp', amount: 2 },
      ]);
    });
    it('level scaling buff ceils if amount is negative', () => {
      const item = {
        aoid: 254391,
        name: 'Notum Silo Nano Efficiency',
        target: <DBBuff['target']>0,
        fpable: <DBBuff['fpable']>1,
        prof: <DBBuff['prof']>-1,
        icon: <DBBuff['icon']>0,
      };
      const result = service.getBuffEffects(item, 30);
      expect(result).toEqual([
        { skillName: 'NanoDelta', amount: 5, isLevelScaling: true },
        {
          skillName: '% Add. Nano Cost',
          amount: -0,
          isLevelScaling: true,
        },
      ]);
    });
  });

  describe('Benefits', () => {
    it('returns the list of benefits', () => {
      const result = service.getAllBenefits();
      expect(result).toHaveProperty('city');
      expect(result).toHaveProperty('tower');
      expect(result).toHaveProperty('advantage');
      expect(result.city).toHaveLength(8);
      expect(result.tower).toHaveLength(29);
      expect(result.advantage).toHaveLength(35);
    });
  });

  describe('getImplantByAOID', () => {
    it('returns the correct clusters for an implant', () => {
      const result = service.getImplantByAOID(102624, 200, 'EYE');
      expect(result).toEqual({
        type: 'implant',
        slot: 'EYE',
        ql: 200,
        clusters: {
          Shiny: { ClusterID: 8 },
          Bright: { ClusterID: 84 },
          Faded: { ClusterID: 12 },
        },
      });
    });
    it('returns the correct symbiant', () => {
      const result = service.getImplantByAOID(236302, 25, 'HEAD');
      expect(result).toEqual({
        type: 'symbiant',
        ql: 25,
        slot: 'HEAD',
        symbiant: { highid: 236302, selectedQl: 25 },
      });
    });
    it('returns special implants', () => {
      const result = service.getImplantByAOID(164825, 1, 'HEAD');
      expect(result).toEqual({
        type: 'symbiant',
        slot: 'HEAD',
        ql: 1,
        symbiant: { highid: 164826, selectedQl: 1 },
      });
    });
    it('returns xan symbiants', () => {
      const result = service.getImplantByAOID(278997, 300, 'EYE');
      expect(result).toEqual({
        type: 'symbiant',
        ql: 300,
        slot: 'EYE',
        symbiant: { highid: 278997, selectedQl: 300 },
      });
    });
  });

  describe.only('item requirements', () => {
    it('should return list of reqs', () => {
      const result = service.getItemSkillRequirements();
      expect(result).toBeDefined();
    });
    it('should return correct item requirements at different QLs', () => {
      const combinedMerc = service.getItem(246642, true) as DBItem & { lowql: number };
      const ql244 = service.getItemRequirements({ ...combinedMerc, selectedQl: 244 });
      expect(ql244).toEqual([
        { stat: 'Strength', value: 815 },
        { stat: 'Stamina', value: 815 },
      ]);
      const ql300 = service.getItemRequirements({ ...combinedMerc, selectedQl: 300 });
      expect(ql300).toEqual([
        { stat: 'Strength', value: 1000 },
        { stat: 'Stamina', value: 1000 },
      ]);
      const ql75 = service.getItemRequirements({ ...combinedMerc, selectedQl: 75 });
      expect(ql75).toEqual([
        { stat: 'Strength', value: 255 },
        { stat: 'Stamina', value: 255 },
      ]);
      const ql1 = service.getItemRequirements({ ...combinedMerc, selectedQl: 1 });
      expect(ql1).toEqual([
        { stat: 'Strength', value: 10 },
        { stat: 'Stamina', value: 10 },
      ]);
    });
  });
});
