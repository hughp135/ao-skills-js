import expect from 'expect';
import { ImplantService, Implant, Cluster, ImplantConfig } from './implant-service';
import { AODBService } from './ao-db-service';
import { CLUSTER_NAME_TO_STAT } from '../constants/implants/cluster-name-to-stat';
import { getStore } from '../test/get-store';
import { ImplantConfigJSON } from '../constants/json/json-types';

describe('ImplantService', () => {
  let service: ImplantService;
  let dbService: AODBService;

  before(async () => {
    dbService = new AODBService(getStore());
    service = new ImplantService(dbService);
  });
  it('creates a new instance', () => {
    expect(service).toBeDefined();
  });
  describe('Implant Effects', () => {
    describe('Implant with single Shiny Cluster Effects', () => {
      describe('EffectTypeID 1 (skill)', () => {
        // went very thorough this because first time testing interpolation, feel free to delete some when confident about interpolation
        it('returns correct amount for ql109 cluster', () => {
          const imp = {
            ql: 109,
            clusters: {
              Shiny: getTestCluster(1),
            },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(60);
        });
        it('returns correct amount for ql108 cluster', () => {
          const imp = {
            ql: 108,
            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(59);
        });
        it('returns correct amount for ql107 cluster', () => {
          const imp = {
            ql: 107,
            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(59);
        });
        it('returns correct amount for ql106 cluster', () => {
          const imp = {
            ql: 106,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(58);
        });
        it('returns correct amount for ql1 cluster', () => {
          const imp = {
            ql: 1,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(6);
        });
        it('returns correct amount for ql200 cluster', () => {
          const imp = {
            ql: 200,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(105);
        });
        it('returns correct amount for ql201 cluster', () => {
          const imp = {
            ql: 201,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(106);
        });
        it('returns correct amount for ql202 cluster', () => {
          const imp = {
            ql: 202,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(106);
        });
        it('returns correct amount for ql203 cluster', () => {
          const imp = {
            ql: 203,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(107);
        });
        it('returns correct amount for ql250 cluster', () => {
          const imp = {
            ql: 250,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(123);
        });
        it('returns correct amount for ql251 cluster', () => {
          const imp = {
            ql: 251,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(124);
        });
        it('returns correct amount for ql300 cluster', () => {
          const imp = {
            ql: 300,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(141);
        });
        it('returns correct amount for ql299 cluster', () => {
          const imp = {
            ql: 299,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(141);
        });
        it('returns correct amount for ql298 cluster', () => {
          const imp = {
            ql: 298,

            clusters: { Shiny: getTestCluster(1) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(140);
        });
      });
      describe('when EffectType is 14 (range)', () => {
        it('returns correct amount for 1 cluster', () => {
          const imp = {
            ql: 1,

            clusters: { Shiny: getTestCluster(14) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(5);
        });
        it('returns correct amount for ql300 cluster', () => {
          const imp = {
            ql: 300,

            clusters: { Shiny: getTestCluster(14) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(19);
        });
      });
      describe('when EffectType is 1 (skill lock)', () => {
        it('returns correct amount for 1 cluster', () => {
          const imp = {
            ql: 1,

            clusters: { Shiny: getTestCluster(11) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(5);
        });
        it('returns correct amount for ql200 cluster', () => {
          const imp = {
            ql: 200,

            clusters: { Shiny: getTestCluster(11) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(-5);
        });
        it('returns correct amount for ql201 cluster', () => {
          const imp = {
            ql: 201,

            clusters: { Shiny: getTestCluster(11) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(-5);
        });
        it('returns correct amount for ql300 cluster', () => {
          const imp = {
            ql: 300,

            clusters: { Shiny: getTestCluster(11) },
          };
          const result = service.getImplantEffects(imp);
          expect(result[0].amount).toEqual(-9);
        });
      });
    });
    describe('Implant with all clusters', () => {
      it('ql1', () => {
        const imp = {
          ql: 1,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 6, skillName: 'Treatment' },
          { amount: 3, skillName: 'Treatment' },
          { amount: 2, skillName: 'Treatment' },
        ]);
      });
      it('ql85', () => {
        const imp = {
          ql: 85,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 48, skillName: 'Treatment' },
          { amount: 28, skillName: 'Treatment' },
          { amount: 19, skillName: 'Treatment' },
        ]);
      });
      it('ql200', () => {
        const imp = {
          ql: 200,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 105, skillName: 'Treatment' },
          { amount: 63, skillName: 'Treatment' },
          { amount: 42, skillName: 'Treatment' },
        ]);
      });
      it('ql201', () => {
        const imp = {
          ql: 201,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 106, skillName: 'Treatment' },
          { amount: 63, skillName: 'Treatment' },
          { amount: 42, skillName: 'Treatment' },
        ]);
      });
      it('ql259', () => {
        const imp = {
          ql: 259,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 127, skillName: 'Treatment' },
          { amount: 76, skillName: 'Treatment' },
          { amount: 51, skillName: 'Treatment' },
        ]);
      });
      it('ql300', () => {
        const imp = {
          ql: 300,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 141, skillName: 'Treatment' },
          { amount: 85, skillName: 'Treatment' },
          { amount: 57, skillName: 'Treatment' },
        ]);
      });
      it('ql250', () => {
        const imp = {
          ql: 250,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(1),
            Faded: getTestCluster(1),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: 123, skillName: 'Treatment' },
          { amount: 74, skillName: 'Treatment' },
          { amount: 49, skillName: 'Treatment' },
        ]);
      });
      it('nano cost ql250', () => {
        const imp = {
          ql: 250,
          clusters: {
            Shiny: getNanoCostCluster(),
            Bright: getNanoCostCluster(),
            Faded: getNanoCostCluster(),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: -4, skillName: '% Add. Nano Cost' },
          { amount: -2, skillName: '% Add. Nano Cost' },
          { amount: -1, skillName: '% Add. Nano Cost' },
        ]);
      });
      it('nano cost ql201', () => {
        const imp = {
          ql: 201,
          clusters: {
            Shiny: getNanoCostCluster(),
            Bright: getNanoCostCluster(),
            Faded: getNanoCostCluster(),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: -3, skillName: '% Add. Nano Cost' },
          { amount: -2, skillName: '% Add. Nano Cost' },
          { amount: -1, skillName: '% Add. Nano Cost' },
        ]);
      });
      it('nano cost ql300', () => {
        const imp = {
          ql: 300,
          clusters: {
            Shiny: getNanoCostCluster(),
            Bright: getNanoCostCluster(),
            Faded: getNanoCostCluster(),
          },
        };
        const result = service.getImplantEffects(imp);
        expect(result).toEqual([
          { amount: -5, skillName: '% Add. Nano Cost' },
          { amount: -3, skillName: '% Add. Nano Cost' },
          { amount: -2, skillName: '% Add. Nano Cost' },
        ]);
      });
    });
  });
  describe('Symbiant Effects', () => {
    it('gets xan symb effects', () => {
      const result = service.getSymbiantEffects({
        QL: 1,
        highid: 278997,
        lowid: 278997,
        icon: 0,
        Name: 'Xan Ocular Symbiant, Support Unit Alpha',
        SlotID: 2,
        selectedQl: 300,
        lowql: 300,
        highql: 300,
      });
      expect(result).toEqual([
        { skillName: 'Bow', amount: 58, isLevelScaling: undefined },
        { skillName: 'Chemistry', amount: 87, isLevelScaling: undefined },
        {
          skillName: 'Comp. Liter',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Concealment',
          amount: 58,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Intelligence',
          amount: 43,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Matter Crea',
          amount: 58,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Multi Ranged',
          amount: 58,
          isLevelScaling: undefined,
        },
        {
          skillName: 'NanoC. Init.',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Perception',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Pharma Tech',
          amount: 87,
          isLevelScaling: undefined,
        },
        { skillName: 'Pistol', amount: 58, isLevelScaling: undefined },
        {
          skillName: 'Psycho Modi',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Sensory Impr',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Time&Space',
          amount: 58,
          isLevelScaling: undefined,
        },
        { skillName: 'Treatment', amount: 87, isLevelScaling: undefined },
        { skillName: 'Tutoring', amount: 144, isLevelScaling: undefined },
        {
          skillName: 'Vehicle Air',
          amount: 144,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Vehicle Ground',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'Vehicle Water',
          amount: 87,
          isLevelScaling: undefined,
        },
        {
          skillName: 'RangeInc. Weapon',
          amount: 16,
          isLevelScaling: undefined,
        },
      ]);
    });
    it('gets special symb efects for eoe ql300', () => {
      const eoe = {
        TreatmentReq: 0,
        LevelReq: 0,
        QL: 300,
        ID: 252987,
        highid: 252987,
        icon: 230980,
        Name: 'Exterminator Ocular Enhancement',
        SlotID: 1,
        selectedQl: 300,
        lowid: 252986,
        lowql: 1,
        highql: 300,
      };
      const result = service.getSymbiantEffects(eoe);
      expect(result).toEqual([
        { skillName: 'RangeInc. Weapon', amount: 12 },
        { skillName: 'Rifle', amount: 100 },
        { skillName: 'Aimed Shot', amount: 100 },
        { skillName: 'Grenade', amount: 90 },
        { skillName: 'Ranged Ener', amount: 100 },
        { skillName: 'Heavy Weapons', amount: 80 },
        { skillName: 'Sharp Obj', amount: 70 },
        { skillName: 'Assault Rif', amount: 80 },
        { skillName: 'Bow', amount: 80 },
        { skillName: 'Multi Ranged', amount: 80 },
        { skillName: 'Pistol', amount: 80 },
        { skillName: 'MG / SMG', amount: 70 },
        { skillName: 'Burst', amount: 70 },
        { skillName: 'Full Auto', amount: 70 },
        { skillName: 'Shotgun', amount: 70 },
        { skillName: 'CriticalIncrease', amount: 3 },
        { skillName: 'Fling Shot', amount: 70 },
      ]);
    });
    it('gets special symb efects for eoe 150', () => {
      const eoe = {
        TreatmentReq: 0,
        LevelReq: 0,
        QL: 300,
        ID: 252987,
        highid: 252987,
        icon: 230980,
        Name: 'Exterminator Ocular Enhancement',
        SlotID: 1,
        selectedQl: 150,
        lowid: 252986,
        lowql: 1,
        highql: 300,
      };
      const result = service.getSymbiantEffects(eoe);
      expect(result).toEqual([
        { skillName: 'RangeInc. Weapon', amount: 7 },
        { skillName: 'Rifle', amount: 51 },
        { skillName: 'Aimed Shot', amount: 51 },
        { skillName: 'Grenade', amount: 46 },
        { skillName: 'Ranged Ener', amount: 51 },
        { skillName: 'Heavy Weapons', amount: 41 },
        { skillName: 'Sharp Obj', amount: 36 },
        { skillName: 'Assault Rif', amount: 41 },
        { skillName: 'Bow', amount: 41 },
        { skillName: 'Multi Ranged', amount: 41 },
        { skillName: 'Pistol', amount: 41 },
        { skillName: 'MG / SMG', amount: 36 },
        { skillName: 'Burst', amount: 36 },
        { skillName: 'Full Auto', amount: 36 },
        { skillName: 'Shotgun', amount: 36 },
        { skillName: 'CriticalIncrease', amount: 2 },
        { skillName: 'Fling Shot', amount: 36 },
      ]);
    });
  });
  describe('Cluster stat mapping', () => {
    it('all clusters in DB (except obsolete ones) can be mapped to a stat', () => {
      const clusters = dbService.getAllClusters();
      clusters.forEach(clust => {
        if (!CLUSTER_NAME_TO_STAT[clust.LongName] && clust.LongName && clust.LongName !== 'Parry') {
          throw new Error(`Stat name not found for cluster: ${clust.LongName}`);
        }
      });
    });
  });
  describe('ao ids/icons for implants', () => {
    it('gets correct id < ql200', () => {
      const imp: Implant = {
        ql: 149,
        clusters: {
          Faded: {
            ClusterID: 1,
            EffectTypeID: 1,
            LongName: 'Radiation Damage Shield*',
            NPReq: 1,
            AltName: '',
          },
        },
      };
      const id = service.getImplantID(imp);
      const icon = ImplantService.getImplantIcon(2048);
      expect(id).toEqual(191497);
      expect(icon).toEqual(12689);
    });
    it('gets correct id > ql200', () => {
      const imp: Implant = {
        ql: 201,
        clusters: {
          Faded: {
            ClusterID: 1,
            EffectTypeID: 1,
            LongName: 'Radiation Damage Shield*',
            NPReq: 1,
            AltName: '',
          },
        },
      };

      const id = service.getImplantID(imp);
      const icon = ImplantService.getImplantIcon(2048);
      expect(id).toEqual(191499);
      expect(icon).toEqual(12689);
    });
  });

  describe('toJSON', () => {
    it('outputs correct JSON', () => {
      const config: ImplantConfig = {
        eye: {
          type: 'symbiant',
          clusters: {},
          ql: 200,
          symbiant: {
            Name: 'Active Ocular Symbiant, Artillery Unit Aban',
            QL: 170,
            SlotID: 1,
            highid: 219135,
            lowid: 219135,
            icon: 230979,
            selectedQl: 170,
          },
        },
        head: {
          type: 'symbiant',
          clusters: {},
          ql: 200,
          symbiant: {
            QL: 200,
            highid: 164823,
            lowid: 164824,
            icon: 12698,
            Name: 'Cyborg Head Implant',
            SlotID: 2,
            selectedQl: 200,
            lowql: 1,
            highql: 200,
          },
        },
        ear: {
          type: 'implant',
          clusters: {
            Shiny: {
              AltName: '',
              ClusterID: 56,
              EffectTypeID: 1,
              LongName: 'Perception',
              NPReq: 800,
            },
          },
          ql: 200,
        },
        chest: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        waist: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        legs: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        feet: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        larm: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        lwrist: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        lhand: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        rarm: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        rwrist: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
        rhand: {
          type: 'implant',
          clusters: {},
          ql: 200,
        },
      };
      const result = service.toJSON(config);
      expect(JSON.parse(JSON.stringify(result))).toEqual([
        {
          slot: 'eye',
          type: 'symbiant',
          ql: 200,
          symbiant: {
            highid: 219135,
            selectedQl: 170,
          },
        },
        {
          slot: 'head',
          type: 'symbiant',
          ql: 200,
          symbiant: {
            highid: 164823,
            selectedQl: 200,
          },
        },
        {
          slot: 'ear',
          type: 'implant',
          ql: 200,
          clusters: {
            Shiny: {
              ClusterID: 56,
            },
          },
        },
      ]);
    });
  });
  describe('fromJSON', () => {
    it('loads correctly from JSON', () => {
      const savedConfig: ImplantConfigJSON = [
        {
          slot: 'eye',
          type: 'symbiant',
          ql: 200,
          symbiant: {
            ID: 18,
            highid: 219135,
            selectedQl: 170,
          },
        },
        {
          slot: 'head',
          type: 'symbiant',
          ql: 200,
          symbiant: {
            ID: 164823,
            highid: 164823,
            selectedQl: 200,
          },
        },
        {
          slot: 'ear',
          type: 'implant',
          ql: 200,
          clusters: {
            Shiny: {
              ClusterID: 56,
            },
          },
        },
      ];
      const loaded = service.fromJSON(savedConfig);
      expect(loaded.eye).toEqual({
        ql: 200,
        type: 'symbiant',
        symbiant: {
          Name: 'Active Ocular Symbiant, Artillery Unit Aban',
          SlotID: 1,
          highid: 219135,
          lowid: 219135,
          QL: 170,
          icon: 230979,
          selectedQl: 170,
          highql: 170,
          lowql: 170,
        },
      });
      expect(loaded.head).toEqual({
        ql: 200,
        type: 'symbiant',
        symbiant: {
          Name: 'Cyborg Head Implant',
          SlotID: 2,
          highid: 164823,
          lowid: 164824,
          QL: 1,
          icon: 12698,
          selectedQl: 200,
          highql: 200,
          lowql: 1,
        },
      });
      expect(loaded.ear).toEqual({
        ql: 200,
        type: 'implant',
        clusters: {
          Shiny: {
            AltName: '',
            ClusterID: 56,
            EffectTypeID: 1,
            LongName: 'Perception',
            NPReq: 800,
          },
          Bright: undefined,
          Faded: undefined,
        },
      });
      Object.entries(loaded).forEach(([key, val]) => {
        if (!['eye', 'head', 'ear'].includes(key)) {
          expect(val).toEqual({ type: 'implant', ql: 200, clusters: {} });
        }
      });
    });
  });
});

function getTestCluster(EffectTypeID: number): Cluster {
  return {
    EffectTypeID,
    ClusterID: 1,
    LongName: 'Treatment',
    NPReq: 1,
    AltName: 'Treatment',
  };
}

function getNanoCostCluster(): Cluster {
  return {
    AltName: 'Nano cost modifier',
    ClusterID: 110,
    EffectTypeID: 13,
    LongName: 'Nano Cost (%)*',
    NPReq: 1,
  };
}
