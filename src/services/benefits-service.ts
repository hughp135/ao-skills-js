import { AODBService } from './ao-db-service';
import { SelectedBenefit } from '../constants/db/types';
import { SkillEffect } from '../classes/equipment/Equipment';

export class BenefitsService {
  constructor(private dbService: AODBService) { }

  public getBenefitEffects(benefit: SelectedBenefit, characterLevel?: number): SkillEffect[] {
    const sortedAOIDs = [...benefit.aoids].sort((a, b) => a.ql - b.ql);
    let nextAoidQlObj: { ql: number; aoid: number; } | undefined;
    const aoidQlObj = sortedAOIDs.find((current, idx) => {
      const next = benefit.aoids[idx + 1];
      const nextQl = next ? next.ql : Infinity;
      if (benefit.selectedQl >= current.ql && benefit.selectedQl < nextQl) {
        if (next) {
          nextAoidQlObj = next;
        }
        return true;
      }
      return false;
    });
    if (!aoidQlObj) {
      throw new Error(`Could not find aoid for benefit ${benefit.name} at ql ${benefit.selectedQl}`);
    }

    return this.dbService.getItemBuffs({
      lowid: aoidQlObj.aoid,
      highid: nextAoidQlObj ? nextAoidQlObj.aoid : aoidQlObj.aoid,
      lowql: aoidQlObj.ql,
      highql: nextAoidQlObj ? nextAoidQlObj.ql : aoidQlObj.ql,
      selectedQl: benefit.selectedQl,
    }, characterLevel, true);
  }
}
