import { SPECIAL_IMP_NAMES } from './../constants/implants/special-imps';
import { AODBService } from './ao-db-service';
import { interpolate } from '../helpers/items/interpolate';
import { CLUSTER_NAME_TO_STAT } from '../constants/implants/cluster-name-to-stat';
import { SlotNumber } from '../constants/items/item-slots';
import { IMPLANT_ICONS } from '../constants/implants/implant-icons';
import { SkillEffect } from '../classes/equipment/Equipment';
import { ImplantConfigJSON } from '../constants/json/json-types';
import { IMPLANT_TYPES } from '../constants/implants/implant-types';

export interface Implant {
  ql: number;
  clusters: {
    Shiny?: Cluster;
    Bright?: Cluster;
    Faded?: Cluster;
  };
}
export interface Cluster {
  ClusterID: number;
  EffectTypeID: number;
  LongName: string;
  NPReq: number;
  AltName: string;
}

export interface SymbiantWithID {
  Name: string;
  QL: number;
  SlotID: number;
  highid: number;
  lowid: number;
  icon: number;
  selectedQl: number;
  highql?: number;
  lowql?: number;
}

interface ImplantEffect {
  skillName: string;
  amount: number;
}

export interface ImplantConfigItem {
  type: 'implant' | 'symbiant';
  ql: number;
  clusters?: {
    Shiny?: Cluster;
    Bright?: Cluster;
    Faded?: Cluster;
  };
  symbiant?: SymbiantWithID;
}
export interface ImplantConfig {
  [type: string]: ImplantConfigItem; // todo: type should be slot name
}

export class ImplantService {
  constructor(private readonly aoDBService: AODBService) {}

  public toJSON(implantConfig: ImplantConfig): ImplantConfigJSON {
    return Object.keys(implantConfig)
      .filter(slot => {
        // Ignore empty slots
        const implant = implantConfig[slot];
        if (implant.type === 'implant' && (!implant.clusters || !Object.values(implant.clusters).length)) {
          return false;
        }
        if (implant.type === 'symbiant' && !implant.symbiant) {
          return false;
        }
        return true;
      })
      .map(slot => {
        const implant = implantConfig[slot];
        return {
          slot,
          type: implant.type,
          ql: implant.ql,
          clusters:
            implant.type === 'implant' && implant.clusters
              ? {
                  Shiny: implant.clusters.Shiny ? { ClusterID: implant.clusters.Shiny.ClusterID } : undefined,
                  Bright: implant.clusters.Bright ? { ClusterID: implant.clusters.Bright.ClusterID } : undefined,
                  Faded: implant.clusters.Faded ? { ClusterID: implant.clusters.Faded.ClusterID } : undefined,
                }
              : undefined,
          symbiant:
            implant.type === 'symbiant' && implant.symbiant
              ? {
                  highid: implant.symbiant.highid,
                  selectedQl: implant.symbiant.selectedQl,
                }
              : undefined,
        };
      });
  }

  /**
   * Maps a saved implant config containing basic config information into a full implant config.
   * @param savedConfig A minimal representation of implant config
   */
  public fromJSON(savedConfig: ImplantConfigJSON): ImplantConfig {
    const clusters = this.aoDBService.getAllClusters();

    const implants: ImplantConfig = Object.values(IMPLANT_TYPES).reduce((acc, curr) => {
      const configItem = savedConfig.find(i => i.slot === curr.ShortName);
      if (!configItem) {
        // Create empty implant slots
        acc[curr.ShortName] = {
          type: 'implant',
          ql: 200,
          clusters: {},
        };
        return acc;
      }
      const item: ImplantConfigItem = {
        ql: configItem.ql,
        type: configItem.type,
      };
      if (configItem.type === 'symbiant') {
        if (!configItem.symbiant) {
          // Ignore empty symbiant slots
          return acc;
        }
        const dbItem = this.aoDBService.getItem(configItem.symbiant.highid, true);
        if (!dbItem) {
          throw new Error(`Could not find DB item with highid ${configItem.symbiant.highid}`);
        }
        const slot = Object.entries(IMPLANT_TYPES).find(([_, type]) => type.ShortName === configItem.slot);
        if (!slot) {
          throw new Error(`Could not find slot ID for slot ${configItem.slot}`);
        }

        item.symbiant = {
          Name: dbItem.name,
          SlotID: parseInt(slot[0], 10),
          highid: dbItem.highid,
          lowid: dbItem.lowid,
          QL: dbItem.lowql,
          icon: dbItem.icon,
          selectedQl: configItem.symbiant.selectedQl,
          highql: dbItem.highql,
          lowql: dbItem.lowql,
        };
      } else if (configItem.type === 'implant') {
        if (!configItem.clusters) {
          configItem.clusters = {};
        }
        const { Shiny, Bright, Faded } = configItem.clusters;
        item.clusters = {
          Shiny: Shiny ? clusters.find(c => c.ClusterID === Shiny.ClusterID) : undefined,
          Bright: Bright ? clusters.find(c => c.ClusterID === Bright.ClusterID) : undefined,
          Faded: Faded ? clusters.find(c => c.ClusterID === Faded.ClusterID) : undefined,
        };
      } else {
        throw new Error(`Invalid ImplantConfigItem - type ${configItem.type}`);
      }
      acc[configItem.slot] = item;
      return acc;
    }, {} as ImplantConfig);

    return implants;
  }

  public getImplantID(implant: Implant): number {
    return this.aoDBService.getImplantID(implant);
  }

  public static getImplantIcon(slot: SlotNumber): number {
    return IMPLANT_ICONS[slot];
  }

  public getImplantEffects(implant: Implant): ImplantEffect[] {
    if (!implant.clusters) {
      return [];
    }

    const { Shiny, Bright, Faded } = implant.clusters;

    const effects: ImplantEffect[] = [];

    const shinyEffects = Shiny && this.getClusterEffects(Shiny, 'Shiny', implant.ql);
    const brightEffects = Bright && this.getClusterEffects(Bright, 'Bright', implant.ql);
    const fadedEffects = Faded && this.getClusterEffects(Faded, 'Faded', implant.ql);

    if (shinyEffects) {
      effects.push(shinyEffects);
    }
    if (brightEffects) {
      effects.push(brightEffects);
    }
    if (fadedEffects) {
      effects.push(fadedEffects);
    }

    return effects;
  }

  private getClusterEffects(cluster: Cluster, type: 'Shiny' | 'Bright' | 'Faded', ql: number): SkillEffect {
    const lowQl = ql <= 200 ? 1 : 201;
    const highQl = ql <= 200 ? 200 : 300;
    const matrix = this.aoDBService.getEffectTypeMatrix(cluster);
    const clusterFactor = type === 'Shiny' ? 1 : type === 'Bright' ? 0.6 : 0.4; // Factor for bright/shiny/faded
    const { MinValLow, MaxValLow, MinValHigh, MaxValHigh } = matrix;
    const baseLowVal = ql <= 200 ? MinValLow : MinValHigh;
    const baseHighVal = ql <= 200 ? MaxValLow : MaxValHigh;
    const lowVal = baseLowVal < 0 ? Math.round(clusterFactor * baseLowVal) : Math.floor(clusterFactor * baseLowVal);
    const skillName = CLUSTER_NAME_TO_STAT[cluster.LongName];
    const highVal = Math.ceil(clusterFactor * baseHighVal);
    const amount = interpolate(lowQl, lowVal, highQl, highVal, ql);

    if (!skillName) {
      throw new Error(`Could not find skill name for cluster ${cluster.LongName}`);
    }

    return {
      skillName,
      amount,
    };
  }

  public getSymbiantEffects(symbiant: SymbiantWithID): ImplantEffect[] {
    return this.aoDBService.getItemBuffs({
      lowid: symbiant.lowid,
      highid: symbiant.highid,
      selectedQl: symbiant.selectedQl,
      lowql: <number>symbiant.lowql,
      highql: <number>symbiant.highql,
    });
  }
}
