import expect from 'expect';
import { AODBService } from './ao-db-service';
import { getStore } from '../test/get-store';
import { BenefitsService } from './benefits-service';
import { SelectedBenefit } from '../constants/db/types';

describe('BenefitsService', () => {
  let service: BenefitsService;
  let dbService: AODBService;

  before(async () => {
    dbService = new AODBService(getStore());
    service = new BenefitsService(dbService);
  });
  it('creates a new instance', () => {
    expect(service).toBeDefined();
  });
  describe('tower benefit effects', () => {
    it('throws an error if selectedQl is out of range', () => {
      expect(() => service.getBenefitEffects(getExampleTower(1))).toThrow();
    });
    it('returns effect for a benefit at ql10', () => {
      const result = service.getBenefitEffects(getExampleTower(10));
      expect(result).toEqual([
        { skillName: 'Max Health', amount: 50 },
      ]);
    });
    it('returns effect for a benefit at ql199', () => {
      const result = service.getBenefitEffects(getExampleTower(199));
      expect(result).toEqual([
        { skillName: 'Max Health', amount: 746 },
      ]);
    });
    it('returns effect for a benefit at ql200', () => {
      const result = service.getBenefitEffects(getExampleTower(200));
      expect(result).toEqual([
        { skillName: 'Max Health', amount: 750 },
      ]);
    });
    it('returns effect for a benefit at ql300', () => {
      const result = service.getBenefitEffects(getExampleTower(300));
      expect(result).toEqual([
        { skillName: 'Max Health', amount: 1500 },
      ]);
    });
  });
  describe('advantage benefit effects', () => {
    it('returns correct effects at ql100', () => {
      const result = service.getBenefitEffects(getExampleAdvantage(100));
      expect(result).toEqual([{ skillName: 'First Aid', amount: 12, isLevelScaling: true },
      { skillName: 'Treatment', amount: 12, isLevelScaling: true }]);
    });
    it('returns correct effects at ql100 and level 1', () => {
      const result = service.getBenefitEffects(getExampleAdvantage(100), 1);
      expect(result).toEqual([{ skillName: 'First Aid', amount: 0, isLevelScaling: true },
      { skillName: 'Treatment', amount: 0, isLevelScaling: true }]);
    });
    it('returns correct effects at ql100 and level 100', () => {
      const result = service.getBenefitEffects(getExampleAdvantage(100), 100);
      expect(result).toEqual([{ skillName: 'First Aid', amount: 6, isLevelScaling: true },
      { skillName: 'Treatment', amount: 6, isLevelScaling: true }]);
    });
    it('scales correctly at level 220', () => {
      const result = service.getBenefitEffects(getExampleAdvantage(250), 220);
      expect(result).toEqual([{ skillName: 'First Aid', amount: 33, isLevelScaling: true },
      { skillName: 'Treatment', amount: 33, isLevelScaling: true }]);
    });
    it('scales correctly at level 207', () => {
      const result = service.getBenefitEffects(getExampleAdvantage(250), 207);
      expect(result).toEqual([{ skillName: 'First Aid', amount: 31, isLevelScaling: true },
      { skillName: 'Treatment', amount: 31, isLevelScaling: true }]);
    });
  });
});

function getExampleTower(selectedQl: number): SelectedBenefit {
  return {
    selectedQl,
    icon: 202154,
    type: 'tower',
    aoids: [{
      ql: 10,
      aoid: 202944,
    }, {
      ql: 200,
      aoid: 202945,
    }, {
      ql: 201,
      aoid: 202946,
    }, {
      ql: 300,
      aoid: 202947,
    },
    ],
    name: 'Guardian Conductor of Life',
    min_ql: 10,
    max_ql: 300,
  };
}

function getExampleAdvantage(selectedQl: number): SelectedBenefit {
  return {
    selectedQl,
    icon: 81782,
    type: 'advantage',
    aoids: [{
      ql: 1,
      aoid: 201809,
    }, {
      ql: 100,
      aoid: 204690,
    }, {
      ql: 200,
      aoid: 201810,
    }, {
      ql: 300,
      aoid: 201811,
    },
    ],
    name: 'Universal Advantage - Medical Expertise',
    min_ql: 1,
    max_ql: 300,
  };
}
