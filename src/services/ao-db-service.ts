import { SKILL_IDS_NAMES, SkillName } from './../constants/skills/skills';
import { CLUSTER_TYPES } from '../constants/implants/cluster-types';
import { IMPLANT_TYPES } from '../constants/implants/implant-types';
import { Cluster, Implant, SymbiantWithID } from './implant-service';
import { SlotNumber, getSlots, SLOT_NAMES_CLOTHES, SLOT_NAMES_WEAPONS, SLOT_NUMBERS, WeaponSlot, ClothingSlot } from '../constants/items/item-slots';
import { interpolate } from '../helpers/items/interpolate';
import { getIntValue } from '../helpers/misc/integer-negative';
import {
  EffectTypeMatrix,
  ItemEffects,
  DBItem,
  ClusterImplantMap,
  ImplantHelper,
  SlotItems,
  Perk,
  PerkGroups,
  BuffList,
  DBBuff,
  Buff,
  Benefits,
  ItemReqStore,
  ItemRequirementList,
  ItemRequirement,
} from '../constants/db/types';
import { Breed } from '../constants/breeds';
import { Profession, AOS4_PROF_ID_TO_NAME } from '../constants/professions';
import { SkillEffect } from '../classes/equipment/Equipment';
import { ImplantJSON } from '../constants/json/json-types';
import { EXCLUDED_LOWIDS_FROM_BUFFS } from '../constants/db/db-item-exceptions';
import { canSkillBeOE } from '../helpers/skills/can-skill-be-oe';
import { roundAo } from '../helpers/misc/round-ao';

// The data is all loaded into memory in the 'Store' object
export interface Store {
  Cluster: Cluster[];
  EffectTypeMatrix: EffectTypeMatrix[];
  itemEffects: ItemEffects;
  aodb: DBItem[];
  itemSlots: {
    Weapon: { [aoid: number]: number };
    Armor: { [aoid: number]: number };
    Symbiant: { [aoid: number]: number };
    Spirit: { [aoid: number]: number };
  };
  ClusterImplantMap: ClusterImplantMap[];
  itemTypes: { [aoid: number]: string[] };
  implantHelper: ImplantHelper;
  allImplantsClusters?: any;
  perks: Perk[];
  buffList: BuffList;
  benefits: Benefits;
  itemProfs: {
    [aoid: number]: {
      fp: boolean;
      profs: string[];
    };
  };
  itemReqs: ItemReqStore;
}

export type AllImplantsSymbiants = {
  slot: string;
  clusterTypes: {
    Bright: Cluster;
    Faded: Cluster;
    Shiny: Cluster;
  }[];
  symbiants: SymbiantWithID[];
}[];

export class AODBService {
  private allImplantsClusters: any;
  private allBuffs: Buff[];
  private implantHelperById: { [aoid: number]: { Shiny: string; Bright: string; Faded: string } };
  private _aodbByHighId: { [highid: number]: DBItem };
  private _aodbByLowId: { [lowid: number]: DBItem };
  private itemRequirements: ItemRequirementList;
  constructor(private readonly store: Store) {}

  // Source data is passed in to the class in the form of JSON files.
  // This is to reduce the size of the package and allow the consuemr to obtain data in whichever way appropriate.
  // Note: the source JSON files that need to be passed in are located in /data/front-end.
  public static createStoreFromSourceData(dataSources: { storeName: string; data: any }[]): Store {
    const store: Store = <Store>{};
    dataSources.forEach(({ storeName, data }) => {
      const { columns, values }: any = data;
      if (columns && values) {
        store[storeName] = Object.freeze(
          values.map(itemValues => {
            return columns.reduce((acc, curr, idx) => {
              acc[curr] = Object.freeze(itemValues[idx]);
              return acc;
            }, {});
          })
        );
      } else {
        store[storeName] = Object.freeze(data);
      }
    });

    return store;
  }

  // This adds inividual JSON files to the store. This is so parts of the data can be lazy loaded.
  public addSourceDataToStore({ data, storeName }: { storeName: string; data: any }): void {
    const { columns, values }: any = data;
    if (columns && values) {
      this.store[storeName] = Object.freeze(
        values.map(itemValues => {
          return columns.reduce((acc, curr, idx) => {
            acc[curr] = Object.freeze(itemValues[idx]);
            return acc;
          }, {});
        })
      );
    } else {
      this.store[storeName] = Object.freeze(data);
    }
  }

  public get aodbByHighId() {
    if (!this._aodbByHighId) {
      this.createAODBItemMaps();
    }
    return this._aodbByHighId;
  }

  public get aodbByLowId() {
    if (!this._aodbByLowId) {
      this.createAODBItemMaps();
    }
    return this._aodbByLowId;
  }

  /**
   * Creates an map of AODB items by their low/highids (for performance reasons)
   * This is only called once when the getters for aodbByHighId/aodbByLowId are accessed for the first time
   */
  private createAODBItemMaps() {
    const { byHighId, byLowId } = this.store.aodb.reduce(
      (acc, curr) => {
        acc.byHighId[curr.highid] = curr;
        acc.byLowId[curr.lowid] = curr;
        return acc;
      },
      { byHighId: {}, byLowId: {} }
    );
    this._aodbByHighId = byHighId;
    this._aodbByLowId = byLowId;
  }

  public getItem(id: number, highId?: boolean): DBItem | undefined {
    if (highId) {
      return this.aodbByHighId[id];
    } else {
      return this.aodbByLowId[id];
    }
  }

  public getItemByHighIdAndQl(highId: number, selectedQl: number): DBItem | undefined {
    const matches = this.store.aodb.filter(i => i.highid === highId);
    if (!matches.length) {
      return this.aodbByLowId[highId]; // Fallback incase low id is given
    }
    if (matches.length === 1) {
      return matches[0];
    }
    return matches.find(i => i.lowql <= selectedQl && i.highql >= selectedQl);
  }

  /**
   * A lose search to find an item at a specific ql from an ID that may not be the correct ID for the ql
   * This is used to find some items that have many ql-ranges (e.g. belts/scopes), and for auno items
   * @param aoid either lowid or highid or item or any item with the same name
   * @param selectedQl ql of item
   */
  public getItemByIdAndQl(aoid: number, selectedQl: number): DBItem | undefined {
    let name: string;
    const found = this.store.aodb.find(i => {
      const matchesId = i.lowid === aoid || i.highid === aoid;
      const matchesName = name && i.name === name;
      const matchesQl = i.lowql <= selectedQl && i.highql >= selectedQl;
      if (matchesId) {
        name = i.name;
      }
      return !!((matchesId || matchesName) && matchesQl);
    });
    if (found) {
      return found;
    }
    // Do a 2nd pass to check if we missed one with the same name
    return this.store.aodb.find(i => {
      const matchesId = i.lowid === aoid || i.highid === aoid;
      const matchesName = name && i.name === name;
      const matchesQl = i.lowql <= selectedQl && i.highql >= selectedQl;
      return !!((matchesId || matchesName) && matchesQl);
    }) as DBItem | undefined;
  }

  public getItemBuffs(
    item: {
      lowid: number;
      highid: number;
      lowql: number;
      highql: number;
      selectedQl?: number;
      oeFactor?: number;
      slotName?: WeaponSlot | ClothingSlot | string;
    },
    characterLevel?: number,
    scalePast200?: boolean
  ): SkillEffect[] {
    // High QL isn't always right for interpolation, check if the next item up has a different QL
    let highQl = item.highql;
    const nextItem = this.aodbByLowId[item.highid];
    if (nextItem && nextItem.lowql !== item.highql) {
      // console.info(`Using ql ${nextItem.lowql} instead of ${item.highql} to calculate item buffs`);
      highQl = nextItem.lowql;
    }

    const highEffects = this.store.itemEffects[item.highid];
    const lowEffects = EXCLUDED_LOWIDS_FROM_BUFFS.includes(item.lowid) ? highEffects : this.store.itemEffects[item.lowid];

    if (!highEffects) {
      return [];
    }
    if (!lowEffects) {
      throw new Error(`loweffects ${item.lowid}`);
    }

    const buffs = highEffects
      .filter(effect => {
        const attributeId = effect[0];
        if (!SKILL_IDS_NAMES[attributeId] && ![140, 26, 27].includes(attributeId)) {
          // ignore logging map navi, random gm item skills
          console.warn(`Could not find skill with attributeId ${attributeId}. Item highid: ${item.highid}`);
        }
        const hasLowEffect = lowEffects.some(e => e[0] === attributeId);

        if (!hasLowEffect) {
          console.warn(`no lowEffect found for highEffect with attrib id ${attributeId},
          and item low/high id: ${item.lowid}/${item.highid}. This skill buff will be ignored`);
        }

        return attributeId && SKILL_IDS_NAMES[attributeId] && hasLowEffect;
      })
      .map(effect => {
        const attributeId = effect[0];
        const highValRaw = effect[1];
        const skillName = SKILL_IDS_NAMES[attributeId];
        const lowEffect = lowEffects.find(e => e[0] === attributeId);

        if (!lowEffect) {
          throw new Error('Failed to filter out buffs with no corresponding lowEffect');
        }

        // Gotta convert values since they're stored as unsigned 32bit ints (negative numbers turn into extremely high nums)
        const lowVal = getIntValue(lowEffect[1]);
        const highVal = getIntValue(highValRaw);
        const amountPreScaling = interpolate(item.lowql, lowVal, highQl, highVal, item.selectedQl || highQl);
        const isLevelScaling = effect[2] ? true : undefined;
        const amountWithoutOE = isLevelScaling ? this.scaleAmountByLevel(amountPreScaling, characterLevel, scalePast200) : amountPreScaling;

        const isOE = !!(
          !(item.slotName && ['WEAPON_R', 'WEAPON_L'].includes(item.slotName)) &&
          typeof item.oeFactor !== 'undefined' &&
          canSkillBeOE(skillName as SkillName)
        );
        const amount = isOE ? roundAo(amountWithoutOE * (item.oeFactor as number)) : amountWithoutOE;

        return {
          skillName,
          amount,
          amountWithoutOE,
          isLevelScaling,
          isOE,
        };
      });

    return buffs;
  }

  public getItemRequirements(item: DBItem & { selectedQl?: number }): ItemRequirement[] {
    const allReqs = this.getItemSkillRequirements();
    const lowReqs = allReqs[item.lowid];
    const highReqs = allReqs[item.highid];

    if (!highReqs) {
      return [];
    }

    return highReqs.map(highReq => {
      const lowReq = lowReqs.find(r => r.stat === highReq.stat);
      if (!lowReq) {
        throw new Error('Failed to filter out reqs with no corresponding lowEffect');
      }

      const lowVal = getIntValue(lowReq.value);
      const highVal = getIntValue(highReq.value);

      const value = interpolate(item.lowql, lowVal, item.highql, highVal, item.selectedQl || item.highql) + 1; // +1 for >=

      return { stat: highReq.stat, value };
    });
  }

  private scaleAmountByLevel(amount, characterLevel?: number, scalePast200?: boolean): number {
    if (!characterLevel) {
      return amount;
    }

    const levelToScaleBy = scalePast200 ? characterLevel : Math.min(characterLevel, 200);
    const rawAmount = (levelToScaleBy / 200) * amount;
    return characterLevel
      ? rawAmount < 0
        ? Math.ceil(rawAmount)
        : Math.floor(rawAmount) // Negative values should be ceil'd.
      : amount;
  }

  public getBuffEffects(nano: DBBuff, characterLevel?: number): SkillEffect[] {
    const itemEffects = this.store.itemEffects[nano.aoid];
    if (!itemEffects) {
      return [];
    }

    return <SkillEffect[]>itemEffects
      .map(effect => {
        const attributeId = effect[0];
        const value = effect[1];
        const name = SKILL_IDS_NAMES[attributeId];
        if (!name) {
          return null;
        }

        // Gotta convert values since they're stored as unsigned 32bit ints (negative numbers turn into extremely high nums)
        const parsedVal = getIntValue(value);
        const isLevelScaling = !!effect[2];
        const amount = isLevelScaling ? this.scaleAmountByLevel(parsedVal, characterLevel, true) : parsedVal;

        const buff: SkillEffect = {
          skillName: name,
          amount,
        };

        if (isLevelScaling) {
          buff.isLevelScaling = isLevelScaling;
        }
        return buff;
      })
      .filter(e => !!e);
  }

  public getAllPerksByID(): { [aoid: number]: Perk } {
    return this.store.perks.reduce((acc, curr) => {
      acc[curr.aoid] = curr;
      return acc;
    }, {});
  }

  public getAllPerks(): { name: string; aoid: number; level: number }[] {
    return this.store.perks.map(perk => {
      return {
        name: perk.name,
        aoid: perk.aoid,
        level: perk.level,
      };
    });
  }

  public getPerks(breed: Breed, profession: Profession): PerkGroups {
    return this.store.perks
      .filter(perk => {
        if (perk.breeds.length && !perk.breeds.includes(breed)) {
          return false;
        }
        if (perk.professions.length && !perk.professions.includes(profession)) {
          return false;
        }
        return true;
      })
      .reduce(
        (acc, perk, _, perks) => {
          if (perk.type === 'LE') {
            acc.research.push(perk);
          } else if (perk.professions.length === 0) {
            acc.general.push(perk);
          } else if (perk.professions.length === 1 && !perks.some(p => p.name === perk.name && p.professions.length > 1)) {
            acc.profession.push(perk);
          } else {
            acc.group.push(perk);
          }

          return acc;
        },
        {
          group: <Perk[]>[],
          profession: <Perk[]>[],
          general: <Perk[]>[],
          research: <Perk[]>[],
        }
      );
  }

  public getPerkEffects(perk: Perk): { skillName: string; amount: number }[] {
    const itemEffects = this.store.itemEffects[perk.aoid];
    if (!itemEffects) {
      return [];
    }

    return itemEffects.map(effect => {
      const [attributeId, amount] = effect;
      if (attributeId === 138) {
        console.warn(`Perk effect included a legacy skill: ${attributeId}, ${perk.name} ${perk.aoid}`);
      }
      const skillName = SKILL_IDS_NAMES[attributeId];
      if (!skillName) {
        throw new Error(`could not find skill with id ${attributeId} found on perk ${perk.name} ${perk.aoid}`);
      }
      return { skillName, amount: getIntValue(amount) };
    });
  }

  public searchSlotItems(slot: SlotNumber, type: 'Armor' | 'Weapon', search?: string) {
    return this.store.aodb.filter(item => {
      const itemSlotsValue = this.store.itemSlots[type][item.lowid];
      if (!itemSlotsValue) {
        return false;
      }
      const itemSlots = getSlots(itemSlotsValue);

      return itemSlots.includes(slot) && (search ? item.name.toLowerCase().includes(search.toLowerCase()) : true);
    });
  }

  public getWeaponHudUtils(): SlotItems {
    const items = this.store.aodb.filter(item => {
      return !!this.store.itemSlots.Weapon[item.lowid];
    });

    return SLOT_NUMBERS.reduce((acc, slot, idx) => {
      acc[SLOT_NAMES_WEAPONS[idx]] = items.filter(item => {
        const slots = getSlots(this.store.itemSlots.Weapon[item.lowid]);
        return slots.includes(slot);
      });
      return acc;
    }, <SlotItems>{});
  }

  getAllClothingItems(): SlotItems {
    const items = this.store.aodb.filter(item => {
      return !!this.store.itemSlots.Armor[item.lowid];
    });

    // note SLOT_NUMBERS is sorted differently for clothes than weapons
    const sorted = [...SLOT_NUMBERS].sort((a, b) => a - b);
    const result = {};

    for (const [idx, slot] of sorted.entries()) {
      result[SLOT_NAMES_CLOTHES[idx]] = items.filter(item => {
        const slots = getSlots(this.store.itemSlots.Armor[item.lowid]);
        return slots.includes(slot);
      });
    }

    return <SlotItems>result;
  }

  public getAllImpsSymbs(): AllImplantsSymbiants {
    if (this.allImplantsClusters) {
      // Return already cached data
      return this.allImplantsClusters;
    }

    const symbs: SymbiantWithID[] = [];
    const namesAdded: { [name: string]: boolean } = {};

    Object.entries({ ...this.store.itemSlots.Symbiant, ...this.store.itemSlots.Spirit }).forEach(([aoid, slotRaw]) => {
      const symbiant = this.getItem(parseInt(aoid, 10)) || this.getItem(parseInt(aoid, 10), true);
      if (!symbiant) {
        console.warn(`Symbiant/Spirit not found with aoid ${aoid}`);
        return;
      }

      const slotVals = getSlots(slotRaw);

      slotVals.forEach(slotVal => {
        const slot = Object.entries(IMPLANT_TYPES).find(([s, t]) => t.slot === slotVal);
        if (!slot) {
          throw new Error(`no type found for special symb with lowid ${aoid} and slot ${slotVal}`);
        }
        if (!namesAdded[symbiant.name]) {
          symbs.push({
            QL: symbiant.highql,
            highid: symbiant.highid,
            lowid: symbiant.lowid,
            icon: symbiant.icon,
            Name: symbiant.name,
            SlotID: parseInt(slot[0], 10),
            selectedQl: symbiant.highql,
            lowql: symbiant.lowql,
            highql: symbiant.highql,
          });
        }
        namesAdded[symbiant.name] = true;
      });
    });

    const clusterImplantMap = this.store.ClusterImplantMap.reduce((acc, item) => {
      const cluster = this.store.Cluster.find(c => c.ClusterID === item.ClusterID);

      if (!acc[item.ImplantTypeID]) {
        acc[item.ImplantTypeID] = {};
      }

      if (!acc[item.ImplantTypeID][CLUSTER_TYPES[item.ClusterTypeID]]) {
        acc[item.ImplantTypeID][CLUSTER_TYPES[item.ClusterTypeID]] = [];
      }

      acc[item.ImplantTypeID][CLUSTER_TYPES[item.ClusterTypeID]].push(cluster);
      return acc;
    }, {});

    const allImplantsClusters = Object.keys(clusterImplantMap).reduce((acc: any, curr) => {
      acc.push({
        slot: IMPLANT_TYPES[curr].ShortName,
        clusterTypes: [
          { type: 'Faded', clusters: clusterImplantMap[curr].Faded },
          { type: 'Bright', clusters: clusterImplantMap[curr].Bright },
          { type: 'Shiny', clusters: clusterImplantMap[curr].Shiny },
        ],
        symbiants: symbs.filter(s => !!s && s.SlotID.toString() === curr),
      });

      return acc;
    }, []);

    this.allImplantsClusters = allImplantsClusters;

    return allImplantsClusters;
  }

  public getEffectTypeMatrix({ EffectTypeID }: Cluster): EffectTypeMatrix {
    const result = this.store.EffectTypeMatrix.find(e => e.ID === EffectTypeID);
    if (!result) {
      throw new Error('Failed to find effect type matrix with effectTypeID ' + EffectTypeID);
    }
    return result;
  }

  public getAllClusters(): Cluster[] {
    return this.store.Cluster;
  }

  public getImplantID(implant: Implant): number {
    const { Shiny, Bright, Faded } = implant.clusters;
    const shinySearch = Shiny ? Shiny.LongName : '';
    const brightSearch = Bright ? Bright.LongName : '';
    const fadedSearch = Faded ? Faded.LongName : '';
    const key = `${shinySearch}@${brightSearch}@${fadedSearch}`;
    const found = this.store.implantHelper ? this.store.implantHelper[key] : null;

    const highid = found ? (implant.ql <= 200 ? found[1] : found[3]) : 0;

    return highid;
  }

  public getBuffList(): Buff[] {
    if (!this.allBuffs) {
      this.allBuffs = Object.values(this.store.buffList).map(buff => {
        return {
          ...buff,
          profession: AOS4_PROF_ID_TO_NAME[buff.prof],
          effects: this.getBuffEffects(buff),
        };
      });
    }

    return this.allBuffs;
  }

  public getAllBenefits(): Benefits {
    return this.store.benefits;
  }

  /**
   * Returns an implant, symbiant, or special symb by AOID. Useful for importing from external sources
   * @param aoid the AOID of the item
   * @param ql the QL of the item
   * @param slot the slot name
   */
  public getImplantByAOID(aoid: number, ql: number, slot: string): ImplantJSON | undefined {
    if (!this.implantHelperById) {
      // Create Map of implant AOIDs to cluster names
      this.implantHelperById = Object.entries(this.store.implantHelper).reduce((acc, val) => {
        const names = val[0];
        const aoids = val[1];
        const [Shiny, Bright, Faded] = names.split('@');
        acc[aoids[0]] = { Shiny, Bright, Faded };
        acc[aoids[1]] = { Shiny, Bright, Faded };
        acc[aoids[2]] = { Shiny, Bright, Faded };
        acc[aoids[3]] = { Shiny, Bright, Faded };
        return acc;
      }, {});
    }

    const clusterNames = this.implantHelperById[aoid];
    if (clusterNames) {
      const { Bright, Shiny, Faded } = clusterNames;
      let bright;
      let shiny;
      let faded;
      this.store.Cluster.forEach(cluster => {
        if (Bright && Bright === cluster.LongName) {
          bright = cluster.ClusterID;
        } else if (Shiny && Shiny === cluster.LongName) {
          shiny = cluster.ClusterID;
        } else if (Faded && Faded === cluster.LongName) {
          faded = cluster.ClusterID;
        }
      });
      return {
        type: 'implant',
        slot,
        ql,
        clusters: {
          Shiny: shiny ? { ClusterID: shiny } : undefined,
          Bright: bright ? { ClusterID: bright } : undefined,
          Faded: faded ? { ClusterID: faded } : undefined,
        },
      };
    } else {
      const item = this.aodbByHighId[aoid] || this.aodbByLowId[aoid];
      if (!item) {
        console.warn(`Could not find item with aoid ${aoid}`);
        return undefined;
      }

      return {
        type: 'symbiant',
        ql,
        slot,
        symbiant: {
          highid: item.highid,
          selectedQl: ql,
        },
      };
    }
  }

  public getItemProfRequirements() {
    return this.store.itemProfs;
  }

  public getNonFroobItemIDs(): { [aoid: string]: true } {
    return Object.entries(this.store.itemReqs).reduce((acc, [aoid, rawStats]) => {
      if (rawStats.some(stat => stat[1] === 389)) {
        // attribute 389 = expansion
        acc[aoid] = true;
      }
      return acc;
    }, {});
  }

  public getItemSkillRequirements(): ItemRequirementList {
    if (!this.itemRequirements) {
      // Convert minimal store data in map by aoid
      this.itemRequirements = Object.entries(this.store.itemReqs).reduce((acc: ItemRequirementList, [aoid, rawStats]) => {
        acc[aoid] = rawStats
          // Ignore profession requirements, only get skill requirements
          .filter(([_, attribute]) => attribute !== 389)
          .map(([type, attribute, value, operator]): ItemRequirement => {
            if (!SKILL_IDS_NAMES[attribute]) {
              console.warn(`Unmapped item req attribute name: ${attribute}`);
            }
            return {
              stat: SKILL_IDS_NAMES[attribute],
              value,
            };
          });

        return acc;
      }, {});
    }

    return this.itemRequirements;
  }
}
