import { Profession } from './../professions';

// Amount of base HP (starting) for each breed
export const BREED_BASE_HP: { [breed: string]: number } = {
  Solitus: 10,
  Opifex: 15,
  Nanomage: 10,
  Atrox: 25,
};

// Amount of base Nano (starting) for each breed
export const BREED_BASE_NP: { [breed: string]: number } = {
  Solitus: 10,
  Opifex: 10,
  Nanomage: 15,
  Atrox: 8,
};

/**
 * Amount of hit points added per point of body dev for each breed
 */
export const BREED_BODY_DEV_FACTOR: { [breed: string]: number } = {
  Solitus: 3,
  Opifex: 3,
  Nanomage: 2,
  Atrox: 4,
};

/**
 * Amount of nano points added per point of nano pool for each breed
 */
export const BREED_NANO_POOL_FACTOR: { [breed: string]: number } = {
  Solitus: 3,
  Opifex: 3,
  Nanomage: 4,
  Atrox: 2,
};

/**
 * Multiplier for amount of HP gained per level, per breed
 */
export const BREED_HP_LEVEL_FACTOR: { [breed: string]: number } = {
  Solitus: 0,
  Opifex: -1,
  Nanomage: -1,
  Atrox: 0,
};

/**
 * Multiplier for amount of Nano gained per level, per breed
 */
export const BREED_NANO_LEVEL_FACTOR: { [breed: string]: number } = {
  Solitus: 0,
  Opifex: -1,
  Nanomage: 1,
  Atrox: -2,
};

/**
 * Amount of HP gained per level per profession and title level
 * Values are in format: [TL1, TL2, TL3, TL4, TL5, TL6]
 * NOTE: Keeper and Shade not complete (guessed), no TL7 specific values
 */
export const PROF_HP_PER_TL_LVL: { [profession in Profession]: readonly number[] } = Object.freeze({
  Adventurer: Object.freeze([6, 7, 8, 8, 9, 9]),
  Agent: Object.freeze([6, 7, 7, 8, 8, 9]),
  Bureaucrat: Object.freeze([6, 7, 7, 7, 8, 9]),
  Doctor: Object.freeze([6, 6, 6, 6, 6, 6]),
  Enforcer: Object.freeze([7, 8, 9, 10, 11, 12]),
  Engineer: Object.freeze([6, 6, 6, 6, 6, 6]),
  Fixer: Object.freeze([6, 7, 7, 8, 8, 10]),
  Keeper: Object.freeze([6, 7, 8, 9, 10, 11]),
  'Martial Artist': Object.freeze([6, 7, 7, 8, 9, 12]),
  'Meta Physicist': Object.freeze([6, 6, 6, 6, 6, 6]),
  'Nano-Technician': Object.freeze([6, 6, 6, 6, 6, 6]),
  Shade: Object.freeze([6, 7, 8, 9, 9, 10]),
  Soldier: Object.freeze([6, 7, 8, 9, 10, 11]),
  Trader: Object.freeze([6, 6, 7, 7, 8, 9]),
});

/**
 * Amount of Nano gained per level per profession and title level
 * Values are in format: [TL1, TL2, TL3, TL4, TL5, TL6]
 * NOTE: Keeper and Shade not complete (guessed), no TL7 specific values
 */
export const PROF_NANO_PER_TL_LVL: { [profession in Profession]: readonly number[] } = Object.freeze({
  Adventurer: Object.freeze([4, 5, 5, 6, 6, 7]),
  Agent: Object.freeze([5, 5, 6, 6, 7, 7]),
  Bureaucrat: Object.freeze([4, 5, 5, 5, 6, 7]),
  Doctor: Object.freeze([4, 5, 6, 7, 8, 10]),
  Enforcer: Object.freeze([4, 4, 4, 4, 4, 4]),
  Engineer: Object.freeze([4, 5, 6, 7, 8, 9]),
  Fixer: Object.freeze([4, 4, 4, 4, 4, 4]),
  Keeper: Object.freeze([4, 4, 4, 4, 4, 4]),
  'Martial Artist': Object.freeze([4, 4, 4, 4, 4, 4]),
  'Meta Physicist': Object.freeze([4, 5, 6, 7, 8, 10]),
  'Nano-Technician': Object.freeze([4, 5, 6, 7, 8, 10]),
  Shade: Object.freeze([4, 4, 4, 6, 6, 6]),
  Soldier: Object.freeze([4, 4, 4, 4, 4, 4]),
  Trader: Object.freeze([4, 5, 5, 5, 6, 7]),
});
