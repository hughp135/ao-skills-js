export type StatName = 'Max Health' |
  'Strength' |
  'Agility' |
  'Stamina' |
  'Intelligence' |
  'Sense' |
  'Psychic' |
  'Attack rating' |
  'Damage to Pet' |
  'Damage To Pet Damage Multiplier' |
  'Free deck slot' |
  'Aggdef' |
  'Imp/Proj AC' |
  'Melee/ma AC' |
  'Energy AC' |
  'Chemical AC' |
  'Radiation AC' |
  'Cold AC' |
  'Disease AC' |
  'Fire AC' |
  'Martial Arts' |
  'Mult. Melee' |
  '1h Blunt' |
  '1h Edged' |
  'Melee Ener.' |
  '2h Edged' |
  'Piercing' |
  '2h Blunt' |
  'Sharp Obj' |
  'Grenade' |
  'Heavy Weapons' |
  'Bow' |
  'Pistol' |
  'Rifle' |
  'MG / SMG' |
  'Shotgun' |
  'Assault Rif' |
  'Vehicle Water' |
  'Melee. Init.' |
  'Ranged. Init.' |
  'Physic. Init' |
  'Bow Spc Att' |
  'Sensory Impr' |
  'First Aid' |
  'Treatment' |
  'Mech. Engi' |
  'Elec. Engi' |
  'Matt. Metam' |
  'Bio Metamor' |
  'Psycho Modi' |
  'Matter Crea' |
  'Time&Space' |
  'Nano Pool' |
  'Ranged Ener' |
  'Multi Ranged' |
  'Trap Disarm.' |
  'Perception' |
  'Adventuring' |
  'Swimming' |
  'Vehicle Air' |
  'Tutoring' |
  'Brawling' |
  'Riposte' |
  'Dimach' |
  'Deflect' |
  'Sneak Atck' |
  'Fast Attack' |
  'Burst' |
  'NanoC. Init.' |
  'Fling Shot' |
  'Aimed Shot' |
  'Body Dev.' |
  'Duck-Exp' |
  'Dodge-Rng' |
  'Evade-ClsC' |
  'Run Speed' |
  'Quantum FT' |
  'Weapon Smt' |
  'Pharma Tech' |
  'Nano Progra' |
  'Comp. Liter' |
  'Psychology' |
  'Chemistry' |
  'Concealment' |
  'Break&Entry' |
  'Vehicle Ground' |
  'Full Auto' |
  'Nano Resist' |
  'Used NCU' |
  'Max NCU' |
  'Aggressiveness' |
  'ReflectProjectileAC' |
  'ReflectMeleeAC' |
  'ReflectEnergyAC' |
  'ReflectChemicalAC' |
  'ReflectRadiationAC' |
  'ReflectColdAC' |
  'ReflectNanoAC' |
  'ReflectFireAC' |
  'Max Nano' |
  'ReflectPoisonAC' |
  'ShieldProjectileAC' |
  'ShieldMeleeAC' |
  'ShieldEnergyAC' |
  'ShieldChemicalAC' |
  'ShieldRadiationAC' |
  'ShieldColdAC' |
  'ShieldNanoAC' |
  'ShieldFireAC' |
  'ShieldPoisonAC' |
  'Add All Off.' |
  'Add All Def.' |
  'Add. Proj. Dam.' |
  'Add. Melee Dam.' |
  'Add. Energy Dam.' |
  'Add. Chem. Dam.' |
  'Add. Rad. Dam.' |
  'Add. Cold Dam.' |
  'Add. Nano Dam.' |
  'Add. Fire Dam.' |
  'Add. Poison Dam.' |
  '% Add. Nano Cost' |
  '% Add. Xp' |
  'HealDelta' |
  'Scale' |
  'NanoDelta' |
  'CriticalIncrease' |
  'RangeInc. Weapon' |
  'RangeInc. NF' |
  'SkillLockModifier' |
  'Decreased Nano-Interrupt Modifier %' |
  'Critical Decrease' |
  'MaxReflectedProjectileDmg' |
  'MaxReflectedMeleeDmg' |
  'MaxReflectedEnergyDmg' |
  'MaxReflectedChemicalDmg' |
  'MaxReflectedRadiationDmg' |
  'MaxReflectedColdDmg' |
  'MaxReflectedNanoDmg' |
  'MaxReflectedFireDmg' |
  'MaxReflectedPoisonDmg' |
  'Healing Efficiency' |
  'Direct Nano Damage Efficiency' |
  'Regain XP Percentage' |
  'Map Navig.' |
  'Heal Reactivity' |
  'Parry';

export const ALL_STATS: StatName[] = [
  'Max Health',
  'Strength',
  'Agility',
  'Stamina',
  'Intelligence',
  'Sense',
  'Psychic',
  'Attack rating',
  'Damage to Pet',
  'Damage To Pet Damage Multiplier',
  'Free deck slot',
  'Aggdef',
  'Imp/Proj AC',
  'Melee/ma AC',
  'Energy AC',
  'Chemical AC',
  'Radiation AC',
  'Cold AC',
  'Disease AC',
  'Fire AC',
  'Martial Arts',
  'Mult. Melee',
  '1h Blunt',
  '1h Edged',
  'Melee Ener.',
  '2h Edged',
  'Piercing',
  '2h Blunt',
  'Sharp Obj',
  'Grenade',
  'Heavy Weapons',
  'Bow',
  'Pistol',
  'Rifle',
  'MG / SMG',
  'Shotgun',
  'Assault Rif',
  'Vehicle Water',
  'Melee. Init.',
  'Ranged. Init.',
  'Physic. Init',
  'Bow Spc Att',
  'Sensory Impr',
  'First Aid',
  'Treatment',
  'Mech. Engi',
  'Elec. Engi',
  'Matt. Metam',
  'Bio Metamor',
  'Psycho Modi',
  'Matter Crea',
  'Time&Space',
  'Nano Pool',
  'Ranged Ener',
  'Multi Ranged',
  'Trap Disarm.',
  'Perception',
  'Adventuring',
  'Swimming',
  'Vehicle Air',
  'Tutoring',
  'Brawling',
  'Riposte',
  'Dimach',
  'Deflect',
  'Sneak Atck',
  'Fast Attack',
  'Burst',
  'NanoC. Init.',
  'Fling Shot',
  'Aimed Shot',
  'Body Dev.',
  'Duck-Exp',
  'Dodge-Rng',
  'Evade-ClsC',
  'Run Speed',
  'Quantum FT',
  'Weapon Smt',
  'Pharma Tech',
  'Nano Progra',
  'Comp. Liter',
  'Psychology',
  'Chemistry',
  'Concealment',
  'Break&Entry',
  'Vehicle Ground',
  'Full Auto',
  'Nano Resist',
  'Used NCU',
  'Max NCU',
  'Aggressiveness',
  'ReflectProjectileAC',
  'ReflectMeleeAC',
  'ReflectEnergyAC',
  'ReflectChemicalAC',
  'ReflectRadiationAC',
  'ReflectColdAC',
  'ReflectNanoAC',
  'ReflectFireAC',
  'Max Nano',
  'ReflectPoisonAC',
  'ShieldProjectileAC',
  'ShieldMeleeAC',
  'ShieldEnergyAC',
  'ShieldChemicalAC',
  'ShieldRadiationAC',
  'ShieldColdAC',
  'ShieldNanoAC',
  'ShieldFireAC',
  'ShieldPoisonAC',
  'Add All Off.',
  'Add All Def.',
  'Add. Proj. Dam.',
  'Add. Melee Dam.',
  'Add. Energy Dam.',
  'Add. Chem. Dam.',
  'Add. Rad. Dam.',
  'Add. Cold Dam.',
  'Add. Nano Dam.',
  'Add. Fire Dam.',
  'Add. Poison Dam.',
  '% Add. Nano Cost',
  '% Add. Xp',
  'HealDelta',
  'Scale',
  'NanoDelta',
  'CriticalIncrease',
  'RangeInc. Weapon',
  'RangeInc. NF',
  'SkillLockModifier',
  'Decreased Nano-Interrupt Modifier %',
  'Critical Decrease',
  'MaxReflectedProjectileDmg',
  'MaxReflectedMeleeDmg',
  'MaxReflectedEnergyDmg',
  'MaxReflectedChemicalDmg',
  'MaxReflectedRadiationDmg',
  'MaxReflectedColdDmg',
  'MaxReflectedNanoDmg',
  'MaxReflectedFireDmg',
  'MaxReflectedPoisonDmg',
  'Healing Efficiency',
  'Direct Nano Damage Efficiency',
  'Regain XP Percentage',
  'Heal Reactivity',
];
