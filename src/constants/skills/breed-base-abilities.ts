import { AbilityName } from './skills';
import { Breed } from './../breeds';

export const BREED_BASE_ABILITIES: {
  [b in Breed]: { [s in AbilityName]: number }
} = {
  Solitus: {
    Strength: 6,
    Agility: 6,
    Stamina: 6,
    Intelligence: 6,
    Sense: 6,
    Psychic: 6,
  },
  Opifex: {
    Strength: 3,
    Agility: 15,
    Stamina: 6,
    Intelligence: 6,
    Sense: 10,
    Psychic: 3,
  },
  Nanomage: {
    Strength: 3,
    Agility: 3,
    Stamina: 3,
    Intelligence: 15,
    Sense: 6,
    Psychic: 10,
  },
  Atrox: {
    Strength: 15,
    Agility: 6,
    Stamina: 10,
    Intelligence: 3,
    Sense: 3,
    Psychic: 3,
  },
};
