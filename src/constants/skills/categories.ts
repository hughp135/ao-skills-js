import { SkillName } from './skills';
import { StatName } from './stats';

export type CategoryName =
  | 'Abilities'
  | 'Body & Defense'
  | 'Melee Weapons'
  | 'Melee Specials'
  | 'Ranged Weapons'
  | 'Ranged Specials'
  | 'Nanos & Casting'
  | 'Exploring'
  | 'Combat & Healing'
  | 'Trade & Repair'
  | 'Disabled / Legacy';

export type StatCategories = CategoryName | 'ACs';

export const IP_CATEGORY_SKILLS: { [k in CategoryName]: SkillName[] } = Object.freeze({
  Abilities: [
    'Strength',
    'Agility',
    'Stamina',
    'Intelligence',
    'Sense',
    'Psychic',
  ],
  'Body & Defense': [
    'Body Dev.',
    'Nano Pool',
    'Evade-ClsC',
    'Dodge-Rng',
    'Duck-Exp',
    'Nano Resist',
    'Deflect',
  ],
  'Melee Weapons': [
    '1h Blunt',
    '1h Edged',
    'Piercing',
    '2h Blunt',
    '2h Edged',
    'Melee Ener.',
    'Martial Arts',
    'Mult. Melee',
    'Melee. Init.',
    'Physic. Init',
  ],
  'Melee Specials': [
    'Sneak Atck',
    'Brawling',
    'Fast Attack',
    'Dimach',
    'Riposte',
  ],
  'Ranged Weapons': [
    'Pistol',
    'Bow',
    'MG / SMG',
    'Assault Rif',
    'Shotgun',
    'Rifle',
    'Ranged Ener',
    'Grenade',
    'Heavy Weapons',
    'Multi Ranged',
    'Ranged. Init.',
  ],
  'Ranged Specials': [
    'Fling Shot',
    'Aimed Shot',
    'Burst',
    'Full Auto',
    'Bow Spc Att',
    'Sharp Obj',
  ],
  'Nanos & Casting': [
    'Matt. Metam',
    'Bio Metamor',
    'Psycho Modi',
    'Sensory Impr',
    'Time&Space',
    'Matter Crea',
    'NanoC. Init.',
  ],
  Exploring: [
    'Vehicle Air',
    'Vehicle Ground',
    'Vehicle Water',
    'Run Speed',
    'Adventuring',
  ],
  'Combat & Healing': [
    'Perception',
    'Concealment',
    'Psychology',
    'Trap Disarm.',
    'First Aid',
    'Treatment',
  ],
  'Trade & Repair': [
    'Mech. Engi',
    'Elec. Engi',
    'Quantum FT',
    'Chemistry',
    'Weapon Smt',
    'Nano Progra',
    'Tutoring',
    'Break&Entry',
    'Comp. Liter',
    'Pharma Tech',
  ],
  'Disabled / Legacy': ['Swimming', 'Map Navig.'],
});

export const ALL_CATEGORIES_SKILLS: { [k in CategoryName]: SkillName[] } = Object.freeze({
  ...IP_CATEGORY_SKILLS,
});

export const MISC_STATS_CATEGORIES: { [k: string]: StatName[] } = Object.freeze({
  'Body & Defense': [
    'Max Health',
    'Max Nano',
  ],
  ACs: [
    'Cold AC', 'Fire AC', 'Melee/ma AC', 'Energy AC', 'Imp/Proj AC', 'Chemical AC',
    'Radiation AC', 'Disease AC',
  ],
});

export const DISPLAY_CATEGORIES: { [k in CategoryName]: StatName[] } = Object.freeze({
  ...IP_CATEGORY_SKILLS,
  ...MISC_STATS_CATEGORIES,
});
