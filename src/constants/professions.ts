export type ProfessionShort = 'Adv' | 'Age' | 'Bur' | 'Doc' | 'Enf' | 'Eng' | 'Fix' | 'Kee' | 'MA' | 'MP' | 'NT' | 'Sha' | 'Sol' | 'Tra';

export type Profession =
  | 'Adventurer'
  | 'Agent'
  | 'Bureaucrat'
  | 'Doctor'
  | 'Enforcer'
  | 'Engineer'
  | 'Fixer'
  | 'Keeper'
  | 'Martial Artist'
  | 'Meta Physicist'
  | 'Nano-Technician'
  | 'Shade'
  | 'Soldier'
  | 'Trader';

export const PROFESSIONS: Profession[] = <Profession[]>[
  'Adventurer', // 6
  'Agent', // 5
  'Bureaucrat', // 8
  'Doctor', // 10
  'Enforcer', // 9
  'Engineer', // 3
  'Fixer', // 4
  'Keeper', // 14
  'Martial Artist', // 2
  'Meta Physicist', // 12
  'Nano-Technician', // 11
  'Shade', // 15
  'Soldier', // 1
  'Trader', // 7
].sort((a, b) => (a === b ? 0 : a < b ? -1 : 1)); // sort alphabetically

export const AOS4_PROF_ID_TO_NAME: { [id: number]: Profession } = Object.freeze(
  [6, 5, 8, 10, 9, 3, 4, 14, 2, 12, 11, 15, 1, 7, -1, -2].reduce((acc, curr, idx) => {
    if (curr === -1) {
      acc[-1] = 'General';
    } else if (curr === -2) {
      acc[-2] = 'General';
    } else {
      acc[curr] = PROFESSIONS[idx];
    }
    return acc;
  }, {})
);

export const PROFESSIONS_SHORT_MAP: { [key in ProfessionShort]: Profession } = {
  Adv: 'Adventurer',
  Age: 'Agent',
  Bur: 'Bureaucrat',
  Doc: 'Doctor',
  Enf: 'Enforcer',
  Eng: 'Engineer',
  Fix: 'Fixer',
  Kee: 'Keeper',
  MA: 'Martial Artist',
  MP: 'Meta Physicist',
  NT: 'Nano-Technician',
  Sha: 'Shade',
  Sol: 'Soldier',
  Tra: 'Trader',
};
