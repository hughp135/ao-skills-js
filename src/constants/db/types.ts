import { WeaponSlot, ClothingSlot } from '../items/item-slots';
import { Profession } from '../professions';
import { Breed } from '../breeds';
import { SkillEffect } from '../../classes/equipment/Equipment';

export interface ImplantHelper {
  /* key: a string containing the clusters names in the format 'Shiny@Bright@Faded' */
  [key: string]: [
    number, // AOID at ql1
    number, // AOID at ql200
    number, // AOID at ql201
    number // AOID at ql300
  ];
}
export interface Symbiant {
  ID: number;
  Name: string;
  QL: number;
  SlotID: number;
  TreatmentReq: number;
  LevelReq: number;
}
export interface ClusterImplantMap {
  ImplantTypeID: number;
  ClusterID: number;
  ClusterTypeID: number;
}
export interface SymbiantClusterMatrix {
  Amount: number;
  ClusterID: number;
  SymbiantID: number;
}
export type SlotItems = { [k in WeaponSlot | ClothingSlot]: DBItem[] };
export interface SkillNameMap {
  [skillId: string]: string;
}
export interface EffectTypeMatrix {
  ID: number;
  MaxValHigh: number;
  MaxValLow: number;
  MinValLow: number;
  MinValHigh: number;
  Name: string;
}
export interface ItemEffects {
  [aoid: number]: [number, number, 1 | undefined][]; // [attribId, value, isLevelScaling]
}

export interface DBItem {
  lowid: number;
  highid: number;
  lowql: number;
  highql: number;
  name: string;
  icon: number;
}

export interface DBItemWithBuffs extends DBItem {
  buffs: DBBuff[];
}

export interface Perk {
  aoid: number;
  level: number;
  name: string;
  type: 'SL' | 'AI' | 'LE';
  professions: Profession[];
  breeds: Breed[];
  aiTitle: number | null;
  counter: number;
}

export interface PerkGroups {
  group: Perk[];
  profession: Perk[];
  general: Perk[];
  research: Perk[];
}

export interface DBBuff {
  aoid: number;
  name: string;
  target: 0 | 1;
  fpable: 0 | 1;
  prof: number;
  icon: number;
}

export interface Buff extends DBBuff {
  profession: Profession | 'General';
  effects: SkillEffect[];
}

export interface BuffList {
  [aoid: string]: DBBuff;
}

export interface Benefit {
  icon: number;
  name: string;
  min_ql: number;
  max_ql: number;
  aoids: {
    ql: number;
    aoid: number;
  }[];
}

export interface SelectedBenefit extends Benefit {
  selectedQl: number;
  type: BenefitType;
}

export interface Benefits {
  city: Benefit[];
  tower: Benefit[];
  advantage: Benefit[];
}

export type BenefitType = 'city' | 'tower' | 'advantage';

export type ItemReqRaw = [
  /** type */
  number,
  /** attribute */
  number,
  /** value */
  number,
  /** operator */
  number
];
export type ItemReqStore = { [aoid: number]: ItemReqRaw[] };
export type ItemRequirement = { stat: string; value: number };
export type ItemRequirementList = { [aoid: number]: ItemRequirement[] };
