import { IMPLANT_TYPES } from './implant-types';

export const DEFAULT_IMP_CONFIG = Object.keys(IMPLANT_TYPES).reduce((acc, curr) => {
  const slot = IMPLANT_TYPES[curr].ShortName;
  acc[slot] = {
    type: 'implant',
    clusters: {},
    ql: 200,
  };
  return acc;
}, {});
