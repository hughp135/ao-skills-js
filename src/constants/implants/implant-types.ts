import { SlotNumber } from '../items/item-slots';

export const IMPLANT_TYPES: {
  [implantTypeId: number]: {
    ImplantTypeID: number;
    Name: string;
    ShortName: string;
    slot: SlotNumber;
  },
} = {
  1: { ImplantTypeID: 1, Name: 'Eye', ShortName: 'eye', slot: 2 },
  2: { ImplantTypeID: 2, Name: 'Head', ShortName: 'head', slot: 4 },
  3: { ImplantTypeID: 3, Name: 'Ear', ShortName: 'ear', slot: 8 },
  4: { ImplantTypeID: 4, Name: 'Chest', ShortName: 'chest', slot: 32 },
  5: { ImplantTypeID: 5, Name: 'Waist', ShortName: 'waist', slot: 256 },
  6: { ImplantTypeID: 6, Name: 'Leg', ShortName: 'legs', slot: 2048 },
  7: { ImplantTypeID: 7, Name: 'Feet', ShortName: 'feet', slot: 8192 },
  8: { ImplantTypeID: 8, Name: 'Left Arm', ShortName: 'larm', slot: 64 },
  9: { ImplantTypeID: 9, Name: 'Left Wrist', ShortName: 'lwrist', slot: 512 },
  10: { ImplantTypeID: 10, Name: 'Left Hand', ShortName: 'lhand', slot: 4096 },
  11: { ImplantTypeID: 11, Name: 'Right Arm', ShortName: 'rarm', slot: 16 },
  12: { ImplantTypeID: 12, Name: 'Right Wrist', ShortName: 'rwrist', slot: 128 },
  13: { ImplantTypeID: 13, Name: 'Right Hand', ShortName: 'rhand', slot: 1024 },
};
