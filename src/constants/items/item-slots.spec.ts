import { getSlots } from './item-slots';
import expect from 'expect';

describe('weapon slots', () => {
  it('returns correct slots for value 32774', () => {
    const result = getSlots(32774);
    expect(result).toEqual([32768, 4, 2]);
  });
  it('returns correct slots for value 32768', () => {
    const result = getSlots(32768);
    expect(result).toEqual([32768]);
  });
  it('returns correct slots for value 32770', () => {
    const result = getSlots(32770);
    expect(result).toEqual([32768, 2]);
  });
  it('returns correct slots for value 63366 (e.g. the item Anything)', () => {
    const result = getSlots(63366);
    expect(result).toEqual([32768, 16384, 8192, 4096, 1024, 512, 256, 128, 4, 2]);
  });
});
