export type SlotNumber = 2 | 32768 | 4 | 8 | 16 | 32 | 64 | 128 | 256 | 512 | 1024 | 2048 | 4096 | 8192 | 16384;

export const SLOT_NUMBERS: SlotNumber[] = [2, 32768, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384];

export type WeaponSlot =
  | 'HUD1'
  | 'HUD2'
  | 'HUD3'
  | 'UTIL1'
  | 'UTIL2'
  | 'UTIL3'
  | 'WEAPON_R'
  | 'BELT'
  | 'WEAPON_L'
  | 'NCU1'
  | 'NCU2'
  | 'NCU3'
  | 'NCU4'
  | 'NCU5'
  | 'NCU6';

export const SLOT_NAMES_WEAPONS: WeaponSlot[] = [
  'HUD1',
  'HUD2',
  'HUD3',
  'UTIL1',
  'UTIL2',
  'UTIL3',
  'WEAPON_R',
  'BELT',
  'WEAPON_L',
  'NCU1',
  'NCU2',
  'NCU3',
  'NCU4',
  'NCU5',
  'NCU6',
];

export type ClothingSlot =
  | 'NECK'
  | 'FINGER_L'
  | 'BACK'
  | 'SHOULDER_R'
  | 'BODY'
  | 'SHOULDER_L'
  | 'ARM_R'
  | 'HANDS'
  | 'ARM_L'
  | 'WRIST_R'
  | 'LEGS'
  | 'WRIST_L'
  | 'FINGER_R'
  | 'FEET'
  | 'HEAD';

export const SLOT_NAMES_CLOTHES: ClothingSlot[] = [
  'NECK',
  'HEAD',
  'BACK',
  'SHOULDER_R',
  'BODY',
  'SHOULDER_L',
  'ARM_R',
  'HANDS',
  'ARM_L',
  'WRIST_R',
  'LEGS',
  'WRIST_L',
  'FINGER_R',
  'FEET',
  'FINGER_L',
];

export const SLOT_NAMES_IMPS: (string | undefined)[] = [
  'eye',
  'head',
  'ear',
  'rarm',
  'chest',
  'larm',
  'rwrist',
  'waist',
  'lwrist',
  'rhand',
  'legs',
  'lhand',
  undefined,
  'feet',
  undefined,
];

export type ImplantSlot = 'eye' | 'head' | 'ear' | 'rarm' | 'chest' | 'larm' | 'rwrist' | 'waist' | 'lwrist' | 'rhand' | 'legs' | 'lhand';

// Items can sometimes be in multiple slots. So to find which slots they go in,
// we must subtract each slot value from the item's slot value to create an array of slots
export function getSlots(value: number): SlotNumber[] {
  const slotsSorted = [...SLOT_NUMBERS].sort((a, b) => b - a);
  const result: SlotNumber[] = [];
  while (value >= 2) {
    slotsSorted.forEach(slot => {
      if (value >= slot) {
        result.push(slot);
        value -= slot;
      }
    });
  }

  return result;
}
