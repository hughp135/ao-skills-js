export type Breed = 'Solitus' | 'Opifex' | 'Atrox' | 'Nanomage';

export const BREEDS: Breed[] = ['Solitus', 'Opifex', 'Atrox', 'Nanomage'];
