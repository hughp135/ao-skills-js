import { CharacterJSON } from '../../classes/character/Character';

export interface ItemJSON {
  slot: string;
  highid: number;
  selectedQl: number;
}

export interface EquipConfigJSON {
  implants: ImplantConfigJSON;
  weapons: ItemJSON[];
  clothes: ItemJSON[];
  perks: { aoid: number }[];
  buffs: { aoid: number }[];
  benefits: { selectedQl: number; name: string }[];
  character?: CharacterJSON;
}

export interface ImplantJSON {
  slot: string;
  type: 'implant' | 'symbiant';
  ql: number;
  clusters?: {
    Shiny?: { ClusterID: number };
    Bright?: { ClusterID: number };
    Faded?: { ClusterID: number };
  };
  symbiant?: {
    ID?: number; // deprecated
    highid: number;
    selectedQl: number;
  };
}

export type ImplantConfigJSON = ImplantJSON[];
