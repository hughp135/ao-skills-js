export const ALIEN_LEVELS = [
  {
    alien_level: 1,
    axp: 1500,
    defender_rank: 'Fledgling',
    min_level: 5,
  },
  {
    alien_level: 2,
    axp: 9000,
    defender_rank: 'Amateur',
    min_level: 15,
  },
  {
    alien_level: 3,
    axp: 22500,
    defender_rank: 'Beginner',
    min_level: 25,
  },
  {
    alien_level: 4,
    axp: 42000,
    defender_rank: 'Starter',
    min_level: 35,
  },
  {
    alien_level: 5,
    axp: 67500,
    defender_rank: 'Newcomer',
    min_level: 45,
  },
  {
    alien_level: 6,
    axp: 99000,
    defender_rank: 'Student',
    min_level: 55,
  },
  {
    alien_level: 7,
    axp: 136500,
    defender_rank: 'Common',
    min_level: 65,
  },
  {
    alien_level: 8,
    axp: 180000,
    defender_rank: 'Intermediate',
    min_level: 75,
  },
  {
    alien_level: 9,
    axp: 229500,
    defender_rank: 'Mediocre',
    min_level: 85,
  },
  {
    alien_level: 10,
    axp: 285000,
    defender_rank: 'Fair',
    min_level: 95,
  },
  {
    alien_level: 11,
    axp: 346500,
    defender_rank: 'Able',
    min_level: 105,
  },
  {
    alien_level: 12,
    axp: 414000,
    defender_rank: 'Accomplished',
    min_level: 110,
  },
  {
    alien_level: 13,
    axp: 487500,
    defender_rank: 'Adept',
    min_level: 115,
  },
  {
    alien_level: 14,
    axp: 567000,
    defender_rank: 'Qualified',
    min_level: 120,
  },
  {
    alien_level: 15,
    axp: 697410,
    defender_rank: 'Competent',
    min_level: 125,
  },
  {
    alien_level: 16,
    axp: 857814,
    defender_rank: 'Suited',
    min_level: 130,
  },
  {
    alien_level: 17,
    axp: 1055112,
    defender_rank: 'Talented',
    min_level: 135,
  },
  {
    alien_level: 18,
    axp: 1297787,
    defender_rank: 'Trustworthy',
    min_level: 140,
  },
  {
    alien_level: 19,
    axp: 1596278,
    defender_rank: 'Supporter',
    min_level: 145,
  },
  {
    alien_level: 20,
    axp: 1931497,
    defender_rank: 'Backer',
    min_level: 150,
  },
  {
    alien_level: 21,
    axp: 2298481,
    defender_rank: 'Defender',
    min_level: 155,
  },
  {
    alien_level: 22,
    axp: 2689223,
    defender_rank: 'Challenger',
    min_level: 160,
  },
  {
    alien_level: 23,
    axp: 3092606,
    defender_rank: 'Patron',
    min_level: 165,
  },
  {
    alien_level: 24,
    axp: 3494645,
    defender_rank: 'Protector',
    min_level: 170,
  },
  {
    alien_level: 25,
    axp: 3879056,
    defender_rank: 'Medalist',
    min_level: 175,
  },
  {
    alien_level: 26,
    axp: 4228171,
    defender_rank: 'Champ',
    min_level: 180,
  },
  {
    alien_level: 27,
    axp: 4608707,
    defender_rank: 'Hero',
    min_level: 185,
  },
  {
    alien_level: 28,
    axp: 5023490,
    defender_rank: 'Guardian',
    min_level: 190,
  },
  {
    alien_level: 29,
    axp: 5475604,
    defender_rank: 'Vanquisher',
    min_level: 195,
  },
  {
    alien_level: 30,
    axp: 5968409,
    defender_rank: 'Vindicator',
    min_level: 200,
  },
];
