export const TL_LEVELS: number[] = [1, 15, 50, 100, 150, 190, 205];

export const LEVEL_ADJUST_PER_TL: number[] = [1, 1, 14, 49, 99, 149, 189, 204];
