import {
  BREED_BASE_HP,
  BREED_BODY_DEV_FACTOR,
  PROF_HP_PER_TL_LVL,
  BREED_HP_LEVEL_FACTOR,
  BREED_NANO_POOL_FACTOR,
  PROF_NANO_PER_TL_LVL,
  BREED_NANO_LEVEL_FACTOR,
  BREED_BASE_NP,
} from './../../constants/character/hp_np';
import { ALL_ABILITY_NAMES, SkillName } from './../../constants/skills/skills';
import { CategoryName, IP_CATEGORY_SKILLS } from './../../constants/skills/categories';
import { LEVEL_ADJUST_PER_TL } from './../../constants/title-levels';
import { BASE_IP_PER_TL, IP_PER_TL } from './../../constants/ip';
import { Skill } from '../skill/Skill';
import { Breed } from '../../constants/breeds';
import { Profession } from '../../constants/professions';
import { validateCharacterOptions } from './validator';
import { getTitleLevel } from '../../helpers/title-level/get-title-level';
import { Category } from '../skill/Category';
import { ALL_SKILL_NAMES } from '../../constants/skills/skills';
import { ALIEN_LEVELS } from '../../constants/levels/ai';
import { ItemSlot } from '../equipment/Equipment';
import { ItemRequirement } from '../../constants/db/types';

export interface NewCharacterOptions {
  breed?: Breed;
  profession?: Profession;
  level?: number;
  name?: string;
}

const defaultCharacterOptions: NewCharacterOptions = {
  level: 220,
  breed: 'Solitus',
  profession: 'Adventurer',
  name: 'New Character',
};

export interface CharacterJSON {
  name: string;
  profession: Profession;
  breed: Breed;
  level: number;
  skills: {
    category: string;
    name: string;
    ipExpenditure: number;
    pointsFromIp: number;
  }[];
}

export class Character {
  public categories: Category[];
  private _breed: Breed;
  private _profession: Profession;
  public name: string;
  public skills: Skill[];
  private _level: number;

  constructor(options?: NewCharacterOptions) {
    options = { ...defaultCharacterOptions, ...options };
    validateCharacterOptions(options);

    this._level = <number>options.level;
    this._breed = <Breed>options.breed;
    this._profession = <Profession>options.profession;
    this.name = <string>options.name;
    this.skills = ALL_SKILL_NAMES.map(skillName => new Skill(skillName, this));
    this.categories = Object.keys(IP_CATEGORY_SKILLS).map(categoryName => {
      const expectedSkills = IP_CATEGORY_SKILLS[categoryName];
      const skillsInCategory = this.skills.filter(s => s.category === categoryName);
      expectedSkills.forEach(skillName => {
        if (!skillsInCategory.some(s => s.name === skillName)) {
          throw new Error('Expected the skill named ' + skillName + ' to be included in category ' + categoryName);
        }
      });
      return new Category(<CategoryName>categoryName, skillsInCategory);
    });
  }

  public static fromJSON(char: CharacterJSON | string): Character {
    if (typeof char === 'string') {
      char = <CharacterJSON>JSON.parse(char);
    }
    const character = new Character({
      name: char.name,
      level: char.level,
      profession: char.profession,
      breed: char.breed,
    });

    ALL_SKILL_NAMES.forEach(skillName => {
      const skillObj = (char as CharacterJSON).skills.find(skill => skill.name === skillName);
      if (!skillObj) {
        // console.debug(`Skill not found from JSON: ${skillName}. Creating fresh skill.`);
        // character.skills.push(new Skill(skillName, character));
        return;
      } else {
        const skill = <Skill>character.skills.find(s => s.name === skillName);
        skill.setIpPoints(skillObj.pointsFromIp);
        if (skill.ipExpenditure !== skillObj.ipExpenditure) {
          console.warn(`New skill IP expenditure for ${skillObj.name} is different to saved.
          New: ${skill.ipExpenditure}, Old: ${skillObj.ipExpenditure}`);
        }
        if (skill.pointsFromIp !== skillObj.pointsFromIp) {
          console.warn(`New skill IP points for ${skillObj.name} different to saved. New: ${skill.pointsFromIp}, Old: ${skillObj.pointsFromIp}`);
        }
      }
    });

    character.categories = Object.keys(IP_CATEGORY_SKILLS).map(categoryName => {
      const expectedSkills = IP_CATEGORY_SKILLS[categoryName];
      const skillsInCategory = character.skills.filter(s => s.category === categoryName);
      expectedSkills.forEach(skillName => {
        if (!skillsInCategory.some(s => s.name === skillName)) {
          throw new Error('Expected the skill named ' + skillName + ' to be included in category ' + categoryName);
        }
      });
      return new Category(<CategoryName>categoryName, skillsInCategory);
    });

    return character;
  }

  public toJSON() {
    return {
      name: this.name,
      level: this._level,
      profession: this._profession,
      breed: this._breed,
      skills: this.skills.filter(s => !!s.pointsFromIp).map(skill => skill.toJSON()), // Ignore skills with no IP spent. See Skill.toJSON()
    };
  }

  public get level(): number {
    return this._level;
  }

  public set level(lvl: number) {
    this._level = lvl;
    this.recalculateSkills();
  }

  public get breed(): Breed {
    return this._breed;
  }

  public set breed(breed: Breed) {
    this._breed = breed;
    this.recalculateSkills();
  }

  public get profession(): Profession {
    return this._profession;
  }

  public set profession(profession: Profession) {
    this._profession = profession;
    this.recalculateSkills();
  }

  /**
   * @returns The number of SL perks available for this character's level
   */
  public get maxSLPerks(): number {
    const post200 = this._level > 200 ? this._level - 200 : 0;
    const pre200 = Math.floor(Math.min(200, this._level) / 10);

    return post200 + pre200;
  }

  /**
   * @returns The number of AI perks available for this character's level
   */
  public get maxAIPerks(): number {
    const result = ALIEN_LEVELS.sort((a, b) => b.min_level - a.min_level).find(ai => {
      return this._level >= ai.min_level;
    });

    return result ? result.alien_level : 0;
  }

  private recalculateSkills(): void {
    this.skills.forEach(skill => {
      skill.recalculateIpPoints();
    });
  }

  /**
   * @returns All of the ability skills (e.g. strength/stamina...) for this character
   */
  public getAbilities(): Skill[] {
    return ALL_ABILITY_NAMES.map(abilityName => {
      const charAbility = this.skills.find(skill => skill.name === abilityName);
      if (!charAbility) {
        throw new Error('Character does not have ability skill: ' + charAbility);
      }

      return charAbility;
    });
  }

  /**
   * @returns The total IP available for this character's level
   */
  public get totalIp(): number {
    const baseIp = BASE_IP_PER_TL[this.titleLevel];
    const levelFactor = this.level - LEVEL_ADJUST_PER_TL[this.titleLevel];

    return baseIp + levelFactor * IP_PER_TL[this.titleLevel];
  }

  /**
   * @returns The amount of unspent IP left for this character
   */
  public get ipRemaining(): number {
    const ipSpent = this.skills.reduce((total: number, skill) => {
      total += skill.ipExpenditure;

      return total;
    }, 0);

    return this.totalIp - ipSpent;
  }

  /**
   * @returns The character's Title Level
   */
  public get titleLevel(): number {
    return getTitleLevel(this.level);
  }

  /**
   * @returns The character's Hit Points (Does not include any Max Health additions from setup)
   */
  public get maxHP(): number {
    const titleLevel = Math.min(6, this.titleLevel) - 1;
    const bodyDev = this.skills.find(s => s.name === 'Body Dev.');
    if (!bodyDev) {
      throw new Error(`Unable to calculate HP as character has no Body Dev. skill`);
    }

    const hpFromBodyDev = bodyDev.getTotalPoints() * BREED_BODY_DEV_FACTOR[this.breed];
    const hpPerLevel = PROF_HP_PER_TL_LVL[this.profession][titleLevel];
    const hpFromLevel = (hpPerLevel + BREED_HP_LEVEL_FACTOR[this.breed]) * this.level;

    return BREED_BASE_HP[this.breed] + hpFromBodyDev + hpFromLevel;
  }

  /**
   * @returns The character's Nano Points (Does not include any Max Nano from setup)
   */
  public get maxNano(): number {
    const titleLevel = Math.min(6, this.titleLevel) - 1;
    const nanoPool = this.skills.find(s => s.name === 'Nano Pool');
    if (!nanoPool) {
      throw new Error(`Unable to calculate HP as character has no Nano Pool skill`);
    }

    const npFromNanoPool = nanoPool.getTotalPoints() * BREED_NANO_POOL_FACTOR[this.breed];
    const npPerLevel = PROF_NANO_PER_TL_LVL[this.profession][titleLevel];
    const npFromLevel = (npPerLevel + BREED_NANO_LEVEL_FACTOR[this.breed]) * this.level;

    return BREED_BASE_NP[this.breed] + npFromNanoPool + npFromLevel;
  }

  public itemOverEquippedFactor(itemReqs: ItemRequirement[]): { buffFactor: number; oeSkills: SkillName[] } {
    let buffFactor = 1;
    const skillNames: SkillName[] = [];

    itemReqs.forEach(req => {
      const skill = this.skills.find(s => s.name === req.stat);
      if (skill) {
        const ratio = skill.getTotalPoints() / req.value;
        if (ratio <= 0.2) {
          buffFactor = 0;
        } else if (ratio <= 0.4) {
          buffFactor = Math.min(buffFactor, 0.25);
        } else if (ratio < 0.6) {
          buffFactor = Math.min(buffFactor, 0.5);
        } else if (ratio < 0.8) {
          buffFactor = Math.min(buffFactor, 0.75);
        }
        if (ratio < 0.8) {
          skillNames.push(skill.name);
        }
      }
    });

    return { buffFactor, oeSkills: skillNames };
  }
}
