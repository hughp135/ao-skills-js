import { NewCharacterOptions } from './Character';
import { PROFESSIONS } from './../../constants/professions';
import { BREEDS } from '../../constants/breeds';

export function validateCharacterOptions(options: NewCharacterOptions) {
  if (options.level && (options.level < 1 || options.level > 220)) {
    throw new Error('Level must be between 1 and 220');
  }

  if (options.profession && !PROFESSIONS.includes(options.profession)) {
    throw new Error('Invalid profession given: ' + options.profession);
  }

  if (options.breed && !BREEDS.includes(options.breed)) {
    throw new Error('Invalid breed given: ' + options.breed);
  }
}
