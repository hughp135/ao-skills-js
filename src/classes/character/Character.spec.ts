import { ALL_CATEGORIES_SKILLS } from './../../constants/skills/categories';
import { Character } from './Character';
import expect from 'expect';
import { Profession } from '../../constants/professions';
import { Breed } from '../../constants/breeds';

describe('Character', () => {
  it('should create a char with default options', () => {
    const char = new Character();
    expect(char.name).toEqual('New Character');
    expect(char.level).toEqual(220);
    expect(char.profession).toEqual('Adventurer');
    expect(char.breed).toEqual('Solitus');
  });
  it('new char should have all skills', () => {
    const char = new Character();
    expect(char.categories).toBeTruthy();
    expect(char.categories.length).toEqual(Object.keys(ALL_CATEGORIES_SKILLS).length);
  });
  describe('maximum ai/sl perk levels', () => {
    it('should return correct available SL levels for level 1', () => {
      const char = new Character({ level: 1 });
      expect(char.maxSLPerks).toEqual(0);
    });
    it('should return correct available SL levels for level 200', () => {
      const char = new Character({ level: 200 });
      expect(char.maxSLPerks).toEqual(20);
    });
    it('should return correct available SL levels for level 201', () => {
      const char = new Character({ level: 201 });
      expect(char.maxSLPerks).toEqual(21);
    });
    it('should return correct available SL levels for level 220', () => {
      const char = new Character({ level: 220 });
      expect(char.maxSLPerks).toEqual(40);
    });
    it('should return correct AI levels 1', () => {
      const char = new Character({ level: 1 });
      expect(char.maxAIPerks).toEqual(0);
    });
    it('should return correct AI levels 135', () => {
      const char = new Character({ level: 135 });
      expect(char.maxAIPerks).toEqual(17);
    });
    it('should return correct AI levels 134', () => {
      const char = new Character({ level: 134 });
      expect(char.maxAIPerks).toEqual(16);
    });
    it('should return correct AI levels 200', () => {
      const char = new Character({ level: 200 });
      expect(char.maxAIPerks).toEqual(30);
    });
  });
  it('should create a char with options', () => {
    const char = new Character({ name: 'jon', level: 220, profession: 'Soldier', breed: 'Atrox' });
    expect(char.name).toEqual('jon');
    expect(char.level).toEqual(220);
    expect(char.profession).toEqual('Soldier');
    expect(char.breed).toEqual('Atrox');
  });
  it('should have the correct IP at lvl 1', () => {
    const char = new Character({ level: 1 });
    expect(char.totalIp).toEqual(1500);
  });
  it('should have the correct IP at lvl 220', () => {
    const char = new Character({ level: 220 });
    expect(char.totalIp).toEqual(18453500);
  });
  it('should have the correct IP at lvl 100', () => {
    const char = new Character({ level: 100 });
    expect(char.totalIp).toEqual(1443500);
  });
  it('should have the correct IP at lvl 99', () => {
    const char = new Character({ level: 99 });
    expect(char.totalIp).toEqual(1403500);
  });
  describe('toJSON', () => {
    it('returns correct JSON representation of char', () => {
      const char = new Character({ level: 99, name: 'Jon', breed: 'Atrox', profession: 'Meta Physicist' });
      const result = JSON.stringify(char);
      const parsed = JSON.parse(result);
      expect(parsed.name).toEqual('Jon');
      expect(parsed.level).toEqual(99);
      expect(parsed.profession).toEqual('Meta Physicist');
      expect(parsed.breed).toEqual('Atrox');
      parsed.skills.forEach(skill => {
        expect(typeof skill.name).toEqual('string');
        expect(typeof skill.category).toEqual('string');
        expect(skill.ipExpenditure).toEqual(0);
        expect(skill.pointsFromIp).toEqual(0);
        expect(skill.character).toBeUndefined();
      });
      expect(parsed.categories).toBeUndefined();
    });
  });
  describe('fromJSON', () => {
    it('creates character from JSON', () => {
      const jsonObj = {
        name: 'jon',
        profession: 'Agent' as Profession,
        breed: 'Atrox' as Breed,
        level: 50,
        skills: [
          {
            name: 'Elec. Engi',
            category: 'Trade & Repair',
            ipExpenditure: 90,
            pointsFromIp: 6,
          },
        ],
      };
      const char = Character.fromJSON(jsonObj);
      expect(char.name).toEqual('jon');
      expect(char.profession).toEqual('Agent');
      expect(char.breed).toEqual('Atrox');
      expect(char.level).toEqual(50);
      const skill = char.skills.find(s => s.name === 'Elec. Engi');
      expect(skill).toBeDefined();
      if (!skill) {
        throw new Error(`Skill not found`);
      }
      expect(skill.pointsFromIp).toEqual(6);
      expect(skill.ipExpenditure).toEqual(90);
    });
    it('sets IP to the correct amounts', () => {
      const jsonObj = {
        name: '',
        profession: 'Soldier' as Profession,
        breed: 'Atrox' as Breed,
        level: 158,
        skills: [
          {
            name: 'Stamina',
            category: 'Abilities',
            ipExpenditure: 116841,
            pointsFromIp: 474,
          },
          {
            name: 'Body Dev.',
            category: 'Body & Defense',
            ipExpenditure: 156880,
            pointsFromIp: 530,
          },
        ],
      };
      const char = Character.fromJSON(jsonObj);
      const stam = char.skills.find(s => s.name === 'Stamina');
      if (!stam) {
        throw new Error(`char didnt have stam`);
      }
      expect(stam.pointsFromIp).toEqual(474);
      expect(stam.ipExpenditure).toEqual(116841);
      const bodyDev = char.skills.find(s => s.name === 'Body Dev.');
      if (!bodyDev) {
        throw new Error(`char didnt have bodyDev`);
      }
      expect(bodyDev.pointsFromIp).toEqual(530);
      expect(bodyDev.ipExpenditure).toEqual(156880);
    });
  });
});
