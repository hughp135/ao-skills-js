import { ALL_ABILITY_NAMES } from './../../constants/skills/skills';
import { Profession } from './../../constants/professions';
import { Breed } from './../../constants/breeds';
import { Skill } from './Skill';
import expect from 'expect';
import { Character } from '../character/Character';

describe('Level 30 NM Doc', () => {
  it('Extra buffs to abilities should be used to calculate trickledown', () => {
    const character = new Character({
      breed: 'Nanomage',
      profession: 'Doctor',
      level: 30,
    });

    const abil = <Skill>character.skills.find(s => s.name === 'Stamina');
    abil.fromBuffs = 51;
    abil.setIpPoints(999);
    const skill = <Skill>character.skills.find(s => s.name === 'Body Dev.');
    skill.setIpPoints(9999);
    skill.fromBuffs = 10;

    expect(skill.getTotalPoints()).toEqual(165);
    expect(skill.getTotalWithoutBuffs()).toEqual(155);
  });
});
describe('Skill', () => {
  let sampleChar: any;
  let maxAbilitySample: any;
  before(() => {
    sampleChar = {
      level: 220,
      breed: 'Solitus',
      profession: 'Adventurer',
      ipRemaining: 99999999,
      skills: [],
    };
    maxAbilitySample = ALL_ABILITY_NAMES.map(name => {
      const skill = new Skill(name, <any>sampleChar);
      skill.setIpPoints(9999);
      return skill;
    });
  });

  it('skill instance should be created with correct initial values', () => {
    const skill = new Skill('Strength', sampleChar);
    expect(skill).toBeTruthy();
    expect(skill.ipExpenditure).toEqual(0);
  });
  describe('IP Expenditure', () => {
    describe('when skill is strength', () => {
      const skillName = 'Strength';

      describe('when breed is Solitus', () => {
        let skill: Skill;
        let character: {
          breed: Breed;
          profession: Profession;
          level: number;
          skills: Skill[];
          ipRemaining: number;
        };

        beforeEach(() => {
          character = {
            breed: 'Solitus',
            profession: 'Adventurer',
            level: 220,
            skills: maxAbilitySample,
            ipRemaining: 99999999,
          };
          skill = new Skill(skillName, character);
        });

        it('setting IP points to 1 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(1)[1]).toEqual(12);
        });
        it('setting IP points to 2 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(2)[1]).toEqual(26);
        });
        it('when not enough IP left, it should only set skill to 2 points', () => {
          skill = new Skill(skillName, { ...character, ipRemaining: 26 });
          const [points, spent] = skill.setIpPoints(3);

          expect(spent).toEqual(26);
          expect(points).toEqual(2);
        });
        it('setting IP points to 766 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(766)[1]).toEqual(595182);
        });
        it('actual points returned should be 766', () => {
          expect(skill.setIpPoints(766)[0]).toEqual(766);
        });
      });
    });

    describe('when skill is Body Dev.', () => {
      const skillName = 'Body Dev.';
      let skill: Skill;
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };

      describe('when profession is Adventurer and breed is Solitus', () => {
        beforeEach(() => {
          character = new Character({
            breed: 'Solitus',
            profession: 'Adventurer',
            level: 220,
          });
          character.skills.forEach(s => {
            s.setIpPoints(9999);
          });

          skill = <Skill>character.skills.find(s => s.name === skillName);
        });
        it('setting IP points to 1 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(1)[1]).toEqual(6);
        });
        it('setting IP points to 2 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(2)[1]).toEqual(13);
        });
        it('setting IP points to 1095 should set correct ip expenditure', () => {
          expect(skill.setIpPoints(1095)[1]).toEqual(724890);
        });
      });
    });
  });

  describe('Skill Level Cap', () => {
    const skillName = '1h Blunt';
    let skill: Skill;
    let character: {
      breed: Breed;
      profession: Profession;
      level: number;
      skills: Skill[];
      ipRemaining: number;
    };

    describe('when profession is Adventurer and breed is Solitus', () => {
      const profession = 'Adventurer';

      beforeEach(() => {
        character = new Character({
          breed: 'Solitus',
          profession: 'Adventurer',
          level: 13,
        });
        character.skills.forEach(s => {
          s.setIpPoints(9999);
        });

        skill = <Skill>character.skills.find(s => s.name === skillName);
      });
      it('should cap ip points correctly at tl1', () => {
        expect(skill.getSkillLvlCap(14, profession)).toEqual(50);
        expect(skill.getSkillLvlCap(13, profession)).toEqual(50);
        expect(skill.getSkillLvlCap(12, profession)).toEqual(48);
        const [points, _, cap] = skill.setIpPoints(10000);
        expect(points).toEqual(50);
        expect(cap).toEqual('level');
      });
      it('should cap ip points correctly at tl2', () => {
        expect(skill.getSkillLvlCap(49, profession)).toEqual(175);
        expect(skill.getSkillLvlCap(48, profession)).toEqual(175);
        expect(skill.getSkillLvlCap(47, profession)).toEqual(175);
        expect(skill.getSkillLvlCap(46, profession)).toEqual(175);
        expect(skill.getSkillLvlCap(45, profession)).toEqual(174);
      });
    });
  });

  describe('Skill Ability Cap', () => {
    describe('when skill is Body Dev', () => {
      let skill: Skill;
      const breed = 'Atrox';
      const profession = 'Adventurer';
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };

      beforeEach(() => {
        character = new Character({
          breed,
          profession,
          level: 220,
        });
        skill = <Skill>character.skills.find(s => s.name === 'Body Dev.');
      });

      it('should be capped at 12', () => {
        expect(skill.getSkillAbilCap()).toEqual(15);
        const [points, _, cap] = skill.setIpPoints(10000);
        expect(points).toEqual(15);
        expect(cap).toEqual('ability');
      });
      it('should be capped at 213', () => {
        const stamina = character.skills.find(s => s.name === 'Stamina');
        (<Skill>stamina).setIpPoints(99);
        expect(skill.getSkillAbilCap()).toEqual(213);
        const [points, _, cap] = skill.setIpPoints(10000);
        expect(points).toEqual(213);
        expect(cap).toEqual('ability');
      });
      it('returns the skill cap without effecting actual total', () => {
        const stamina = character.skills.find(s => s.name === 'Stamina');
        (<Skill>stamina).setIpPoints(99);
        const before = skill.getTotalPoints();
        const [maxVal, capReason] = skill.getCap();

        expect(maxVal).toEqual(213);
        expect(capReason).toEqual('ability');

        const after = skill.getTotalPoints();
        expect(before).toEqual(after);
      });
      it('returns the level cap without effecting actual total', () => {
        const stamina = character.skills.find(s => s.name === 'Stamina');
        (<Skill>stamina).setIpPoints(99);
        const before = skill.getTotalPoints();
        const [maxVal, capReason] = skill.getCap(true);

        expect(maxVal).toEqual(1095);
        expect(capReason).toEqual('level');

        const after = skill.getTotalPoints();
        expect(before).toEqual(after);
      });
    });

    describe('when skill is Swimming', () => {
      let skill: Skill;
      const breed = 'Opifex';
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };

      beforeEach(() => {
        character = new Character({
          breed,
          profession: 'Agent',
          level: 150,
        });
        skill = <Skill>character.skills.find(s => s.name === 'Swimming');
      });

      it('should be capped at 9', () => {
        expect(skill.getSkillAbilCap()).toEqual(9);
      });
      it('should be capped at 207', () => {
        const stamina = character.skills.find(s => s.name === 'Stamina');
        (<Skill>stamina).setIpPoints(99);
        const agi = character.skills.find(s => s.name === 'Agility');
        (<Skill>agi).setIpPoints(99);
        const str = character.skills.find(s => s.name === 'Strength');
        (<Skill>str).setIpPoints(99);

        expect(skill.getSkillAbilCap()).toEqual(207);

        const [points, _, cap] = skill.setIpPoints(10000);
        expect(points).toEqual(207);
        expect(cap).toEqual('ability');
      });
    });
  });

  describe('Ability Caps', () => {
    describe('when breed is atrox', () => {
      const breedName = 'Atrox';
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };
      beforeEach(() => {
        character = {
          breed: breedName,
          profession: 'Trader',
          level: 150,
          ipRemaining: 99999999,
          skills: [],
        };
      });
      it('Intelligence', () => {
        const skill = new Skill('Intelligence', character);
        expect(skill.getAbilityCap(149, breedName)).toEqual(397);
        expect(skill.getAbilityCap(133, breedName)).toEqual(397);
        expect(skill.getAbilityCap(132, breedName)).toEqual(396);
        const [points, _, cap] = skill.setIpPoints(400);
        expect(points).toEqual(397);
        expect(cap).toEqual('level');
      });
      it('Strength', () => {
        // Note: this skill should not be capped
        const skill = new Skill('Strength', character);
        expect(skill.getAbilityCap(149, breedName)).toEqual(447);
        expect(skill.getAbilityCap(148, breedName)).not.toEqual(
          skill.getAbilityCap(149, breedName),
        );
      });
    });
    describe('when breed is solitus', () => {
      const breedName = 'Solitus';
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };
      beforeEach(() => {
        character = {
          breed: breedName,
          profession: 'Trader',
          level: 150,
          ipRemaining: 99999999,
          skills: [],
        };
      });
      it('Strength', () => {
        const skill = new Skill('Strength', character);
        expect(skill.getAbilityCap(199, breedName)).toEqual(466);
        expect(skill.getAbilityCap(156, breedName)).toEqual(466);
        expect(skill.getAbilityCap(155, breedName)).toEqual(465);
      });
      it('Psychic', () => {
        const skill = new Skill('Psychic', character);
        expect(skill.getAbilityCap(199, breedName)).toEqual(474);
        expect(skill.getAbilityCap(158, breedName)).toEqual(474);
        expect(skill.getAbilityCap(157, breedName)).toEqual(471);
      });
    });
    describe('when breed is Opifex', () => {
      const breedName = 'Opifex';
      let character: {
        breed: Breed;
        profession: Profession;
        level: number;
        skills: Skill[];
        ipRemaining: number;
      };
      beforeEach(() => {
        character = {
          breed: breedName,
          profession: 'Trader',
          level: 150,
          ipRemaining: 99999999,
          skills: [],
        };
      });
      it('Agility', () => {
        const skill = new Skill('Agility', character);
        expect(skill.getAbilityCap(200, breedName)).toEqual(529);
        expect(skill.getAbilityCap(177, breedName)).toEqual(529);
        expect(skill.getAbilityCap(176, breedName)).toEqual(528);
      });
    });
  });
  describe('toJSON', () => {
    it('returns a JSON representation of a skill', () => {
      const skill = new Skill('Agility', new Character());
      const result = JSON.stringify(skill);
      expect(JSON.parse(result)).toEqual({ name: 'Agility', category: 'Abilities', ipExpenditure: 0, pointsFromIp: 0 });
    });
  });
});
