import { Character } from './../character/Character';
import {
  COST_TO_RATE,
  BREED_CAPS_PRE_201,
  ABILITY_INDEX,
  BREED_CAPS_POST_201,
} from './../../constants/skills/skill-caps';
import { getTitleLevel } from '../../helpers/title-level/get-title-level';
import { Profession } from './../../constants/professions';
import { Breed } from './../../constants/breeds';
import { BREED_BASE_ABILITIES } from './../../constants/skills/breed-base-abilities';
import { ABILITY_COSTS, SKILL_COSTS } from './../../constants/skills/ip-costs';
import {
  SkillName,
  SKILL_ABILITY_DEP,
  ALL_SKILL_NAMES,
} from './../../constants/skills/skills';
import { CategoryName } from '../../constants/skills/categories';
import { getSkillCategory } from '../../helpers/skills/get-skill-category';
import { getPreviousTlMaxLvl } from '../../helpers/title-level/get-prev-title-level';
import { roundAo } from '../../helpers/misc/round-ao';

interface CharacterBasic {
  breed: Breed;
  profession: Profession;
  level: number;
  skills: Skill[];
  ipRemaining: number;
}
export type SkillCapReason = 'ip' | 'level' | 'ability' | undefined;
type SkillCharacter = Partial<Character> & CharacterBasic;

// Note: most logic is ported from AOSkills source
export class Skill {
  public category: CategoryName;
  public ipExpenditure: number;
  public pointsFromIp: number;
  private _fromBuffs = 0; // Extra points added through from any external source (e.g. armor, imps, nanos, etc)

  constructor(public name: SkillName, private character: SkillCharacter) {
    if (!ALL_SKILL_NAMES.includes(name)) {
      throw new Error(`${name} is not a valid skill name`);
    }
    this.category = getSkillCategory(name);
    this.ipExpenditure = 0;
    this.pointsFromIp = 0;
  }

  public get fromBuffs() {
    return this._fromBuffs;
  }

  public set fromBuffs(amount: number) {
    this._fromBuffs = amount;
  }

  public toJSON() {
    return {
      name: this.name,
      category: this.category,
      ipExpenditure: this.ipExpenditure,
      pointsFromIp: this.pointsFromIp,
    };
  }

  /**
   * Attempts to set points of a skill.
   * If IP runs out or level cap is reached, it will return a capReason of ('ip', 'level' or 'ability')
   * @param points How many IP points to set skill to. NOTE!!: This is the value *minus* base skill.
   * You must first subtract getPointsPreIP() from this value IF your value is the total skill points.
   * @param opts character level/breed/prof/IP remaining
   * @returns An array containing [newPoints, ipSpent, capReason]
   *
   */
  public setIpPoints(
    points: number,
    character: SkillCharacter = this.character,
    actuallySet: boolean = true,
    checkLevelCap: boolean = false,
  ): [number, number, SkillCapReason] {

    const { level, breed, profession } = character;
    const ipAvailable = character.ipRemaining + this.ipExpenditure;
    const isAbility = this.isAbility();
    const ipCostPerPoint = isAbility
      ? ABILITY_COSTS[breed][this.name]
      : SKILL_COSTS[this.name][profession];
    const maxSkillCap = isAbility
      ? this.getAbilityCap(level, breed)
      : this.getSkillLvlCap();
    let totalIp = 0;
    let newPoints = 0;
    let cappedBy: SkillCapReason;
    const maxSkillAbilCap = isAbility ? Infinity : this.getSkillAbilCap();
    const basePoints = this.getBasePoints(breed);

    for (let i = 0; i < points; i++) {
      const nextIpSpent =
        totalIp + Math.floor((i + basePoints) * ipCostPerPoint);

      if (newPoints + 1 > maxSkillCap) {
        cappedBy = 'level';
        break;
      }

      if (!checkLevelCap && (newPoints + 1 > maxSkillAbilCap)) {
        cappedBy = 'ability';
        break;
      }

      if (!checkLevelCap && (ipAvailable !== undefined && nextIpSpent > ipAvailable)) {
        cappedBy = 'ip';
        break;
      }

      totalIp = nextIpSpent;
      newPoints++;
    }

    // This function may be used just to get the skill cap, so only save if this is true
    if (actuallySet) {
      this.pointsFromIp = newPoints;
      this.ipExpenditure = totalIp;
    }

    return [newPoints, totalIp, cappedBy];
  }

  /**
   * Returns the cap of the skill and the reason for the cap
   * @param levelOnly Set to true if you just want the level cap (not ip/ability cap)
   * @returns An array containing [skillCap, capReason]
   */
  public getCap(levelOnly: boolean = false): [number, SkillCapReason] {
    const [skillCap, _, cappedBy] = this.setIpPoints(Infinity, undefined, false, levelOnly);
    return [skillCap, cappedBy];
  }

  /**
   * (Do not use manually)
   * Updates the current skill's IP points (e.g. when a character's level changes). You shouldn't ever need to call this manually.
   */
  public recalculateIpPoints() {
    this.setIpPoints(this.pointsFromIp);
  }

  /**
   * Returns the max IP points you can spend on a skill
   */
  public getMaxValue(): number {
    return this.isAbility() ? this.getAbilityCap(this.character.level, this.character.breed)
      : this.getSkillLvlCap();
  }

  /**
   * Returns maximum value (level cap) of a *skill* based on level and profession
   */
  public getSkillLvlCap(
    level = this.character.level,
    profession = this.character.profession,
  ): number {
    const tl = getTitleLevel(level);
    const costFactor = SKILL_COSTS[this.name][profession];
    const cost = Math.round(costFactor * 10 - 10);
    let maxValCap: number;
    let maxVal: number;

    if (level < 201) {
      maxValCap = Math.round(COST_TO_RATE[cost][tl + 1]);
      if (tl === 1) {
        maxVal = Math.round(COST_TO_RATE[cost][1] * level);
      } else {
        maxVal = Math.round(
          COST_TO_RATE[cost][tl] +
          COST_TO_RATE[cost][1] * (level - getPreviousTlMaxLvl(level)),
        );
      }
      maxValCap = Math.round(maxValCap > maxVal ? maxVal : maxValCap);
    } else {
      maxValCap = Math.round(
        COST_TO_RATE[cost][7] + (level - 200) * COST_TO_RATE[cost][8],
      );
    }

    return maxValCap;
  }

  /**
   * Returns maximum value (ability cap) of a *non-ability* skill (e.g. 1 Handed Blunt) based on abilities
   * @param opts Character skills and breed (skills provided must include all abilities)
   */
  public getSkillAbilCap(breed: Breed = this.character.breed): number {
    let weightedAbility = 0;
    if (!this.character.getAbilities) {
      throw new Error('getAbilities method not defined on character');
    }

    const abilities = this.character.getAbilities();

    abilities.forEach(charAbility => {
      const charAbilityValue = charAbility.getTotalPoints(breed);
      const dependencyFactor = SKILL_ABILITY_DEP[this.name][charAbility.name];

      if (dependencyFactor === undefined) {
        throw new Error(
          'cannot find dependecy for skill: ' +
          this.name +
          ': ' +
          charAbility.name,
        );
      }

      weightedAbility += charAbilityValue * dependencyFactor;
    });

    return roundAo((weightedAbility - 5) * 2 + 5);
  }

  /**
   * Returns maximum value (level cap) of an *ability* (e.g. str/stam) based on level and breed
   * @param level Character level
   * @param breed Character breed
   */
  public getAbilityCap(
    level: number = this.character.level,
    breed: Breed = this.character.breed,
  ): number {
    const basePoints = this.getBasePoints(breed);
    const abilityIdx = ABILITY_INDEX[this.name];
    const breedCapPre201 = BREED_CAPS_PRE_201[breed][abilityIdx];
    const breedCapPost201 = BREED_CAPS_POST_201[breed][abilityIdx];
    let attrMax: number;

    if (level < 201) {
      attrMax = level * 3 + basePoints;
      attrMax = Math.round(attrMax > breedCapPre201 ? breedCapPre201 : attrMax);
    } else {
      attrMax = breedCapPre201 + (level - 200) * breedCapPost201;
    }

    attrMax = attrMax - basePoints;

    return attrMax;
  }

  public isAbility() {
    return this.category === 'Abilities';
  }

  private getBasePoints(breed: Breed = this.character.breed): number {
    return this.isAbility() ? BREED_BASE_ABILITIES[breed][this.name] : 5;
  }

  public getPointsPreIP(breed: Breed = this.character.breed): number {
    const trickleDown = this.isAbility() ? 0 : this.calcTrickleDown();

    return this.getBasePoints(breed) + trickleDown;
  }

  public getTotalWithoutBuffs(breed: Breed = this.character.breed) {
    return this.pointsFromIp + this.getPointsPreIP(breed);
  }

  public getTotalPoints(breed: Breed = this.character.breed): number {
    return this.getTotalWithoutBuffs(breed) + this._fromBuffs;
  }

  private calcTrickleDown(): number {
    if (this.isAbility()) {
      throw new Error(
        'attempted to calc trickledown of ability skill: ' + this.name,
      );
    }

    if (!this.character.getAbilities) {
      throw new Error(`method this.character.getAbilities is undefined`);
    }

    const amount = this.character.getAbilities().reduce((total, ability) => {
      const abilityPoints = ability.getTotalPoints();
      total +=
        abilityPoints * SKILL_ABILITY_DEP[this.name][ability.name];

      return total;
    }, 0);

    return Math.floor(amount / 4);
  }
}
