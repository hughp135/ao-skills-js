import { CategoryName } from './../../constants/skills/categories';
import { Skill } from './Skill';

export class Category {
  constructor(public name: CategoryName, public skills: Skill[]) { }

  public isAbility(): boolean {
    return this.name === 'Abilities';
  }

  public get ipExpenditure(): number {
    return this.skills.reduce((acc, skill) => {
      acc += skill.ipExpenditure;
      return acc;
    }, 0);
  }
}
