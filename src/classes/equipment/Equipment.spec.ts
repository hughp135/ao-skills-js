import { EquipConfigJSON } from './../../../dist/constants/json/json-types.d';
import expect from 'expect';
import {
  ImplantService,
  ImplantConfig,
  Cluster,
} from '../../services/implant-service';
import { AODBService } from '../../services/ao-db-service';
import { Equipment } from './Equipment';
import { getStore } from '../../test/get-store';
import { Perk } from '../../constants/db/types';
import { BenefitsService } from '../../services/benefits-service';

describe('Equipment', () => {
  let service: Equipment;
  let implantService: ImplantService;
  let dbService: AODBService;
  let benefitsService: BenefitsService;

  before(async () => {
    dbService = new AODBService(getStore());
    implantService = new ImplantService(dbService);
    benefitsService = new BenefitsService(dbService);
  });
  beforeEach(async () => {
    service = new Equipment(implantService, dbService, benefitsService);
  });
  it('creates a new instance', () => {
    expect(service).toBeDefined();
  });
  describe('Equip effects 1', () => {
    it('should create an equip', () => {
      const result = service.getEquipmentEffects();
      expect(result).toEqual([]);
    });
  });
  // @Todo fix these tests: been broken for a few years
  describe('Equip effects', () => {
    it.skip('returns effects for implants', () => {
      const implants: ImplantConfig = {
        eye: {
          type: 'implant',
          ql: 200,
          clusters: {
            Shiny: getTestCluster(1),
            Bright: getTestCluster(5),
            Faded: getTestCluster(2),
          },
        },
        head: {
          ql: 170,
          type: 'symbiant',
          symbiant: {
            selectedQl: 170,
            lowid: 1,
            Name: 'Active Brain Symbiant, Control Unit Aban',
            SlotID: 2,
            QL: 170,
            highid: 1,
            icon: 1,
          },
        },
      };
      service.setImplants(implants);
      const result = service.getEquipmentEffects();
      expect(result).toEqual([
        {
          name: 'Abilities',
          skills: [
            {
              skillName: 'Intelligence',
              amount: 43,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 43,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Psychic',
              amount: 43,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 43,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Sense',
              amount: 18,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 18,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Body & Defense',
          skills: [
            {
              skillName: 'Max Nano',
              amount: 341,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 341,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Nano Pool',
              amount: 52,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 52,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Nano Resist',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Nanos & Casting',
          skills: [
            {
              skillName: 'Bio Metamor',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Matter Crea',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Matt. Metam',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'NanoC. Init.',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Psycho Modi',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Sensory Impr',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Time&Space',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Exploring',
          skills: [
            {
              skillName: 'Vehicle Air',
              amount: 35,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 35,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Vehicle Ground',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Vehicle Water',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Combat & Healing',
          skills: [
            {
              skillName: 'Treatment',
              amount: 218,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 218,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'First Aid',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Perception',
              amount: 35,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 35,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Psychology',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Trade & Repair',
          skills: [
            {
              skillName: 'Chemistry',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Comp. Liter',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Elec. Engi',
              amount: 52,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 52,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Mech. Engi',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Nano Progra',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Pharma Tech',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Quantum FT',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Tutoring',
              amount: 35,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 35,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Weapon Smt',
              amount: 52,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 52,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Disabled / Legacy',
          skills: [
            {
              skillName: 'Map Navig.',
              amount: 52,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 52,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
      ]);
    });
    it('returns effects for weapons', () => {
      const weaponsConfig = {
        HUD3: {
          lowid: 268469,
          highid: 268469,
          lowql: 150,
          highql: 150,
          name: 'Alien Augmentation Device - Defensive',
          icon: 218750,
          selectedQl: 150,
        },
      };
      service.setWeapons(weaponsConfig);
      const result = service.getEquipmentEffects();
      expect(result).toEqual([
        {
          name: 'Body & Defense',
          skills: [
            {
              skillName: 'Max Health',
              amount: 500,
              fromWeapons: 500,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Duck-Exp',
              amount: 10,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Dodge-Rng',
              amount: 10,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Evade-ClsC',
              amount: 10,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Nano Resist',
              amount: 25,
              fromWeapons: 25,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
      ]);
    });
    it.skip('combines weapons and implants stats', () => {
      const imps = {
        6: {
          ql: 200,
          type: 'symbiant',
          clusters: {},
          symbiant: {
            ID: 31,
            Name: 'Active Thigh Symbiant, Control Unit Aban',
            SlotID: 6,
            QL: 170,
          },
        },
      };
      const weaponsConfig = {
        HUD3: {
          lowid: 268469,
          highid: 268469,
          lowql: 150,
          highql: 150,
          name: 'Alien Augmentation Device - Defensive',
          icon: 218750,
          selectedQl: 150,
        },
      };
      service.setImplants(<any>imps);
      service.setWeapons(weaponsConfig);
      const result = service.getEquipmentEffects();
      expect(result).toEqual([
        {
          name: 'Abilities',
          skills: [
            {
              skillName: 'Agility',
              amount: 43,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 43,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Stamina',
              amount: 27,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 27,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Body & Defense',
          skills: [
            {
              skillName: 'Dodge-Rng',
              amount: 96,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Duck-Exp',
              amount: 96,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Evade-ClsC',
              amount: 62,
              fromWeapons: 10,
              fromClothing: 0,
              fromImplants: 52,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Max Health',
              amount: 637,
              fromWeapons: 500,
              fromClothing: 0,
              fromImplants: 137,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Nano Resist',
              amount: 25,
              fromWeapons: 25,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Exploring',
          skills: [
            {
              skillName: 'Adventuring',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Run Speed',
              amount: 35,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 35,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Disabled / Legacy',
          skills: [
            {
              skillName: 'Swimming',
              amount: 86,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 86,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Misc',
          skills: [
            {
              skillName: 'HealDelta',
              amount: 18,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 18,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: '% Add. Xp',
              amount: 2,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 2,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Decreased Nano-Interrupt Modifier %',
              amount: -7,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: -7,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Max NCU',
              amount: 10,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 10,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'SkillLockModifier',
              amount: -7,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: -7,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
      ]);
    });
    it('returns effects for clothes', () => {
      const clothes = {
        NECK: {
          lowid: 96358,
          highid: 96358,
          lowql: 1,
          highql: 1,
          name: 'Clan Merits - Basic Board',
          icon: 99655,
          label: 'Clan Merits - Basic Board (QL1)',
          selectedQl: 1,
        },
        BODY: {
          lowid: 27383,
          highid: 27383,
          lowql: 1,
          highql: 1,
          name: 'Omni-Med Suit Shirt',
          icon: 21861,
          label: 'Omni-Med Suit Shirt (QL1)',
          selectedQl: 1,
        },
        SHOULDER_L: null,
        ARM_R: null,
        HANDS: {
          lowid: 152544,
          highid: 152544,
          lowql: 160,
          highql: 160,
          name: 'Silken Legchopper Gloves',
          icon: 37105,
          label: 'Silken Legchopper Gloves (QL160)',
          selectedQl: 160,
        },
      };
      service.setClothes(clothes);
      const result = service.getEquipmentEffects();
      expect(result).toEqual([
        {
          name: 'Body & Defense',
          skills: [
            {
              skillName: 'Max Health',
              amount: 20,
              fromWeapons: 0,
              fromClothing: 20,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'Max Nano',
              amount: 10,
              fromWeapons: 0,
              fromClothing: 10,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Melee Weapons',
          skills: [
            {
              skillName: '2h Edged',
              amount: 40,
              fromWeapons: 0,
              fromClothing: 40,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: '1h Edged',
              amount: 40,
              fromWeapons: 0,
              fromClothing: 40,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
        {
          name: 'Combat & Healing',
          skills: [
            {
              skillName: 'Treatment',
              amount: 20,
              fromWeapons: 0,
              fromClothing: 20,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
            {
              skillName: 'First Aid',
              amount: 20,
              fromWeapons: 0,
              fromClothing: 20,
              fromImplants: 0,
              fromPerks: 0, fromBuffs: 0, fromBenefits: 0,
            },
          ],
        },
      ]);
    });
    it('returns effects for perks', () => {
      const perks: Perk[] = [
        {
          aoid: 253108,
          level: 1,
          name: 'Champion of Heavy Artillery',
          type: 'AI',
          professions: [],
          breeds: [],
          aiTitle: null,
          counter: 1,
        },
        {
          aoid: 253109,
          level: 1,
          name: 'Champion of Heavy Artillery',
          type: 'AI',
          professions: [],
          breeds: [],
          aiTitle: null,
          counter: 1,
        },
        {
          aoid: 212123,
          name: 'Spatial Displacement',
          level: 70,
          type: 'SL',
          professions: ['Adventurer', 'Fixer', 'Martial Artist', 'Keeper', 'Shade'],
          breeds: [],
          aiTitle: null,
          counter: 1,
        },
      ];
      service.setPerks(perks);
      const result = service.getEquipmentEffects();

      expect(result).toEqual([
        {
          name: 'Ranged Weapons',
          skills: [
            {
              skillName: 'Assault Rif',
              amount: 10,
              fromBenefits: 0,
              fromBuffs: 0,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 10,
            },
            {
              skillName: 'Rifle',
              amount: 10,
              fromBenefits: 0,
              fromBuffs: 0,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 10,
            },
            {
              skillName: 'Ranged Ener',
              amount: 10,
              fromBenefits: 0,
              fromBuffs: 0,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 10,
            },
            {
              skillName: 'Heavy Weapons',
              amount: 10,
              fromBenefits: 0,
              fromBuffs: 0,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 10,
            },
            {
              skillName: 'Grenade',
              amount: 10,
              fromBenefits: 0,
              fromBuffs: 0,
              fromWeapons: 0,
              fromClothing: 0,
              fromImplants: 0,
              fromPerks: 10,
            },
          ],
        },
      ]);
    });
  });
  describe('loading from JSON', () => {
    it('loads correct NCU from json', () => {
      const weaponsConfig = [null, null, null, null, null, null, null, null, null,
        { slot: 'NCU1', highid: 95520, selectedQl: 130 }
        , null, null, null, null, null] as EquipConfigJSON['weapons'];
      const loaded = service.itemsFromJson(weaponsConfig, 'weapons');
      expect(loaded).toEqual({
        HUD1: null,
        HUD2: null,
        HUD3: null,
        UTIL1: null,
        UTIL2: null,
        UTIL3: null,
        WEAPON_R: null,
        BELT: null,
        WEAPON_L: null,
        NCU1:
        {
          highid: 95520,
          highql: 199,
          icon: 119136,
          lowid: 36781,
          lowql: 120,
          name: '32 - 63 NCU Memory',
          selectedQl: 130,
        },
        NCU2: null,
        NCU3: null,
        NCU4: null,
        NCU5: null,
        NCU6: null,
      });
    });
  });
});

function getTestCluster(EffectTypeID: number): Cluster {
  return {
    EffectTypeID,
    ClusterID: 1,
    LongName: 'Treatment',
    NPReq: 1,
    AltName: 'Treatment',
  };
}
