import { ImplantService, ImplantConfig, Implant, SymbiantWithID } from '../../services/implant-service';
import { AODBService } from '../../services/ao-db-service';
import { ALL_CATEGORIES_SKILLS, MISC_STATS_CATEGORIES, DISPLAY_CATEGORIES } from '../../constants/skills/categories';
import { DBItem, Perk, Buff, SelectedBenefit, Benefit } from '../../constants/db/types';
import { StatName } from '../../constants/skills/stats';
import { BenefitsService } from '../../services/benefits-service';
import { EquipConfigJSON } from '../../constants/json/json-types';
import { SLOT_NAMES_WEAPONS, SLOT_NAMES_CLOTHES, WeaponSlot } from '../../constants/items/item-slots';
import { AOS4_PROF_ID_TO_NAME } from '../../constants/professions';
import { DEFAULT_IMP_CONFIG } from '../../constants/implants/default-imp-config';
import { Character } from '../character/Character';

export type ItemSlot = DBItem & { selectedQl: number; oeFactor?: number; isOE?: boolean };
export interface ItemConfig {
  [slot: string]: ItemSlot | null;
}
export interface SkillEffect {
  skillName: string;
  amount: number;
  amountWithoutOE?: number;
  isOE?: boolean;
  isLevelScaling?: boolean; // True if effect scales by level
}
export type PerkConfig = Perk[];
export type SkillEffectWithSource = SkillEffect & {
  fromWeapons: number;
  fromClothing: number;
  fromImplants: number;
  fromPerks: number;
  fromBuffs: number;
  fromBenefits: number;
};
export interface EquipEffect {
  name: string;
  skills: SkillEffectWithSource[];
}
export type EffectSource = 'fromWeapons' | 'fromClothing' | 'fromImplants' | 'fromPerks' | 'fromBuffs' | 'fromBenefits';

export class Equipment {
  public implants: ImplantConfig = DEFAULT_IMP_CONFIG;
  public weapons: ItemConfig = {};
  public clothes: ItemConfig = {};
  public perks: PerkConfig = [];
  public buffs: Buff[] = [];
  public benefits: SelectedBenefit[] = [];

  constructor(
    private readonly implantService: ImplantService,
    private readonly dbService: AODBService,
    private readonly benefitsService: BenefitsService,
    savedConfiguration?: EquipConfigJSON
  ) {
    if (savedConfiguration) {
      this.implants = this.implantService.fromJSON(savedConfiguration.implants);
      this.weapons = this.itemsFromJson(savedConfiguration.weapons, 'weapons');
      this.clothes = this.itemsFromJson(savedConfiguration.clothes, 'clothes');
      this.perks = this.perksFromJson(savedConfiguration.perks);
      this.buffs = this.buffsFromJson(savedConfiguration.buffs);
      this.benefits = this.benefitsFromJson(savedConfiguration.benefits);
    }
  }

  /**
   * @returns A minimal representation of an Equipment setup in JSON format. Can be used for saving/loading equip config
   */
  public toJSON() {
    return <EquipConfigJSON>{
      implants: this.implantService.toJSON(this.implants),
      weapons: Object.keys(this.weapons).map(slot => {
        const weapon = this.weapons[slot];
        return (
          weapon && {
            slot,
            highid: weapon.highid,
            selectedQl: weapon.selectedQl,
          }
        );
      }),
      clothes: Object.keys(this.clothes).map(slot => {
        const item = this.clothes[slot];
        return (
          item && {
            slot,
            highid: item.highid,
            selectedQl: item.selectedQl,
          }
        );
      }),
      perks: this.perks.map(perk => {
        return {
          aoid: perk.aoid,
        };
      }),
      buffs: this.buffs.map(buffs => {
        return {
          aoid: buffs.aoid,
        };
      }),
      benefits: this.benefits.map(benefit => {
        return {
          selectedQl: benefit.selectedQl,
          name: benefit.name,
        };
      }),
    };
  }

  /**
   * Returns ItemConfig from minimal saved data
   * @param itemConfig either weapons or clothes in minimal config format
   * @param type 'weapons' or 'clothes'.
   */
  public itemsFromJson(itemConfig: EquipConfigJSON['weapons'] | EquipConfigJSON['clothes'], type: 'weapons' | 'clothes'): ItemConfig {
    const slotNames = type === 'weapons' ? SLOT_NAMES_WEAPONS : SLOT_NAMES_CLOTHES;
    return (slotNames as any).reduce((acc, curr) => {
      const slotConfig = itemConfig.find(i => i && i.slot === curr);
      if (slotConfig) {
        const dbItem = this.dbService.getItemByHighIdAndQl(slotConfig.highid, slotConfig.selectedQl);
        acc[curr] = <ItemSlot>{
          ...dbItem,
          selectedQl: slotConfig.selectedQl,
        };
      } else {
        acc[curr] = null;
      }
      return acc;
    }, {});
  }

  public perksFromJson(perksConfig: EquipConfigJSON['perks']): PerkConfig {
    const perksMap = this.dbService.getAllPerksByID();
    return perksConfig.map(savedPerk => {
      const item = perksMap[savedPerk.aoid];
      if (!item) {
        throw new Error(`Could not find perk with ID ${savedPerk.aoid}`);
      }
      return item;
    });
  }

  public buffsFromJson(buffsConfig: EquipConfigJSON['buffs']): Buff[] {
    const buffsById = this.dbService.getBuffList().reduce((acc, curr) => {
      acc[curr.aoid] = curr;
      return acc;
    }, {});

    return buffsConfig
      .filter(buffConfig => {
        if (!buffsById[buffConfig.aoid]) {
          console.warn(`Failed to load buff item with id ${buffConfig.aoid}`);
        }

        return buffsById[buffConfig.aoid];
      })
      .map(buffConfig => {
        const item = buffsById[buffConfig.aoid];

        return {
          ...item,
          profession: AOS4_PROF_ID_TO_NAME[item.prof],
          effects: this.dbService.getBuffEffects(item),
        };
      });
  }

  public benefitsFromJson(benefitsConfig: EquipConfigJSON['benefits']): SelectedBenefit[] {
    const allBenefits = this.dbService.getAllBenefits();
    const benefitsByName = Object.values(allBenefits).reduce((acc, curr) => {
      curr.forEach(benefit => {
        acc[benefit.name] = benefit;
      });
      return acc;
    }, {} as { [benefitName: string]: Benefit[] });
    return benefitsConfig.map(benefitConfig => {
      const benefit = benefitsByName[benefitConfig.name];
      if (!benefit) {
        throw new Error(`Could not find benefit with name: ${benefitConfig.name}`);
      }
      return {
        ...benefit,
        selectedQl: benefitConfig.selectedQl,
      };
    });
  }

  public setImplants(implants: ImplantConfig): void {
    this.implants = implants;
  }

  public setWeapons(weapons: ItemConfig): void {
    this.weapons = weapons;
  }

  public setClothes(clothes: ItemConfig): void {
    this.clothes = clothes;
  }

  public setPerks(perks: Perk[]): void {
    this.perks = perks;
  }

  public setBuffs(buffs: Buff[]): void {
    this.buffs = buffs;
  }

  public getEquipmentEffects(character?: Character, calcOe?: boolean): EquipEffect[] {
    const categories: EquipEffect[] = Object.keys(DISPLAY_CATEGORIES)
      .concat(['Misc'])
      .map(name => {
        return {
          name,
          skills: <SkillEffectWithSource[]>[],
        };
      });
    const charLevel = character ? character.level : undefined;

    const addBuffToCategories = (skillEffect: SkillEffect, source: EffectSource) => {
      const categoryName =
        Object.keys(ALL_CATEGORIES_SKILLS).find(catName => ALL_CATEGORIES_SKILLS[catName].includes(skillEffect.skillName)) ||
        Object.keys(MISC_STATS_CATEGORIES).find(catName => MISC_STATS_CATEGORIES[catName].includes(<StatName>skillEffect.skillName)) ||
        'Misc';
      const category = categories.find(c => c.name === categoryName);
      if (!category) {
        console.warn(`No category found with name ${categoryName} for buff ${skillEffect.skillName}`);
        return;
      }
      const skill = category.skills.find(e => e.skillName === skillEffect.skillName);
      if (skill) {
        skill.amount += skillEffect.amount;
        if (skill[source]) {
          skill[source] += skillEffect.amount;
        } else {
          skill[source] = skillEffect.amount;
        }
      } else {
        category.skills.push({
          ...skillEffect,
          fromWeapons: source === 'fromWeapons' ? skillEffect.amount : 0,
          fromClothing: source === 'fromClothing' ? skillEffect.amount : 0,
          fromImplants: source === 'fromImplants' ? skillEffect.amount : 0,
          fromPerks: source === 'fromPerks' ? skillEffect.amount : 0,
          fromBuffs: source === 'fromBuffs' ? skillEffect.amount : 0,
          fromBenefits: source === 'fromBenefits' ? skillEffect.amount : 0,
        });
      }
    };

    Object.values(this.implants).forEach(implant => {
      if (implant.type === 'symbiant' && !implant.symbiant) {
        // Ignore null symbiants
        return;
      }
      const effects =
        implant.type === 'symbiant'
          ? this.implantService.getSymbiantEffects(<SymbiantWithID>implant.symbiant)
          : this.implantService.getImplantEffects(<Implant>implant);

      effects.forEach(buff => {
        addBuffToCategories(buff, 'fromImplants');
      });
    });

    Object.entries(this.weapons).forEach(([slotName, slot]) => {
      if (slot) {
        if (calcOe && character && ['WEAPON_R', 'WEAPON_L'].includes(slotName)) {
          const itemReqs = this.dbService.getItemRequirements(slot);
          const { buffFactor } = character.itemOverEquippedFactor(itemReqs);
          if (buffFactor !== 1) {
            slot.isOE = true;
            slot.oeFactor = buffFactor;
          }
        } else {
          slot.isOE = false;
          slot.oeFactor = undefined;
        }
        const buffs = this.dbService.getItemBuffs({ ...slot, slotName: slotName as WeaponSlot }, character ? character.level : undefined, true);
        buffs.forEach(buff => {
          addBuffToCategories(buff, 'fromWeapons');
        });
      }
    });

    Object.values(this.clothes).forEach(slot => {
      if (slot) {
        if (calcOe && character) {
          const itemReqs = this.dbService.getItemRequirements(slot);
          const { buffFactor, oeSkills } = character.itemOverEquippedFactor(itemReqs);
          if (buffFactor !== 1) {
            slot.oeFactor = buffFactor;
            slot.isOE = true;
          }
        } else {
          slot.isOE = false;
          slot.oeFactor = undefined;
        }
        const buffs = this.dbService.getItemBuffs(slot, charLevel, true);
        buffs.forEach(buff => {
          addBuffToCategories(buff, 'fromClothing');
        });
      }
    });

    this.perks.forEach(perk => {
      const buffs = this.dbService.getPerkEffects(perk);
      buffs.forEach(buff => {
        addBuffToCategories(buff, 'fromPerks');
      });
    });

    this.buffs.forEach(buff => {
      const effects = this.dbService.getBuffEffects(buff, charLevel);
      effects.forEach(effect => {
        addBuffToCategories(effect, 'fromBuffs');
      });
    });

    this.benefits.forEach(benefit => {
      const effects = this.benefitsService.getBenefitEffects(benefit, charLevel);
      effects.forEach(effect => {
        addBuffToCategories(effect, 'fromBenefits');
      });
    });

    return categories.filter(cat => cat.skills.length);
  }

  public setBenefits(benefitsConfig: SelectedBenefit[]): void {
    this.benefits = benefitsConfig;
  }
}
