import { Store } from '../services/ao-db-service';
import * as fs from 'fs';
import { AODBService } from '../services/ao-db-service';

export function getStore(): Store {
  const fileNames = [
    'Cluster',
    'EffectTypeMatrix',
    'itemEffects',
    'aodb',
    'ClusterImplantMap',
    'implantHelper',
    'itemSlots',
    'perks',
    'buffList',
    'benefits',
    'itemReqs',
  ];
  const dataSources = fileNames.map(fileName => {
    const data = JSON.parse(fs.readFileSync(__dirname + '/../../data/front-end/' + fileName + '.json').toString());
    return {
      storeName: fileName,
      data,
    };
  });
  const store: Store = AODBService.createStoreFromSourceData(dataSources);

  return store;
}
