import { TL_LEVELS } from './../../constants/title-levels';
import { getTitleLevel } from '../../helpers/title-level/get-title-level';

export function getPreviousTlMaxLvl(level: number): number {
    const currentTl = getTitleLevel(level);
    const tlMinLevel = TL_LEVELS[currentTl - 1];

    return tlMinLevel - 1;
}
