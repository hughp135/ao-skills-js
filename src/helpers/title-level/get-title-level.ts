import { TL_LEVELS } from './../../constants/title-levels';

export function getTitleLevel(level: number): number {
  for (let i = TL_LEVELS.length - 1; i >= 0; i--) {
    if (level >= TL_LEVELS[i]) {
      return i + 1;
    }
  }

  throw new Error('Unable to get title level for level: ' + level);
}
