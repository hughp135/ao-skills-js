// Some values in DB are very large (probably when negative), so bring it back past 32bit int limit
export function getIntValue(val: number) {
  return val > 4000000000 ? val - 4294967295 - 1 : val;
}
