import { IPABLE_SKILLS_MAP, SkillName } from '../../constants/skills/skills';

const exceptionOESkills: Set<SkillName> = new Set(['Nano Resist', 'Melee. Init.', 'Ranged. Init.', 'Physic. Init', 'NanoC. Init.']);

export const canSkillBeOE = (skillName: SkillName) => {
  return !IPABLE_SKILLS_MAP[skillName] || exceptionOESkills.has(skillName);
};
