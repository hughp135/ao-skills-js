import { CategoryName, ALL_CATEGORIES_SKILLS } from './../../constants/skills/categories';
import { SkillName } from '../../constants/skills/skills';

export function getSkillCategory(skill: SkillName): CategoryName {
  const category = Object.keys(ALL_CATEGORIES_SKILLS).find((cat) => {
    return ALL_CATEGORIES_SKILLS[cat].includes(skill);
  });

  return <CategoryName> category;
}
