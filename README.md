JS package to emulate characters and skills in Anarchy Online, and access the AO database in JS.

See `/data/README.md` for instructions on how to compile the source data from the SQL dump of the AO dbs. This is only required when item changes occur to AO for example after a new patch. Otherwise, the json files in `data/front-end` can be used to initiate the data store for the `AODBService` class constructor, using the class methods `createStoreFromSourceData()` and `addSourceDataToStore()`

Can be imported as an npm package in other JS projects using a file import (pointing to the project's root) - after running `npm build`.
